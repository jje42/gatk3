import com.gilcloud.sbt.gitlab.GitlabPlugin

Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / version := "0.1.0"
ThisBuild / organization := "com.gitlab.jje42"
ThisBuild / scalaVersion := "2.12.12"
ThisBuild / scalacOptions := Seq("-deprecation", "-feature")
//ThisBuild / javacOptions := Seq("-Xlint:unchecked", "-Xlint:deprecation")

val htsjdkVersion = "2.10.1"

lazy val util = (project in file("gatk-utils"))
  .settings(
    name := "gatk-utils",
    libraryDependencies ++= Seq(
      "javax.xml.bind"            % "jaxb-api"            % "2.3.1" exclude("javax.activation", "javax.activation-api"),
      "com.github.samtools"       % "htsjdk"              % htsjdkVersion,
      "colt"                      % "colt"                % "1.2.0",
      "it.unimi.dsi"              % "fastutil"            % "6.5.3",
      "org.reflections"           % "reflections"         % "0.9.11",
      "org.slf4j"                 % "slf4j-api"           % "1.6.1",
      "commons-lang"              % "commons-lang"        % "2.5",
      "commons-io"                % "commons-io"          % "2.1",
      "commons-collections"       % "commons-collections" % "3.2.2",
      "org.apache.commons"        % "commons-math"        % "2.2",
      "org.apache.commons"        % "commons-math3"       % "3.5",
      "net.java.dev.jna"          % "jna"                 % "3.2.7",
      "us.levk"                   % "drmaa-gridengine"    % "6.2u5",
      "com.google.code.gson"      % "gson"                % "2.2.2",
      "org.apache.httpcomponents" % "httpclient"          % "4.1.1",
      "org.freemarker"            % "freemarker"          % "2.3.18",
      "org.huoc"                  % "cofoja"              % "1.3.1",
      "log4j"                     % "log4j"               % "1.2.15" exclude("com.sun.jdmk", "jmxtools") exclude("javax.jms", "jms") exclude("com.sun.jmx", "jmxri"),
      "com.google.caliper"        % "caliper"             % "0.5-rc1" % "test" exclude("com.google.guava", "guava"),
      "org.testng"                % "testng"              % "6.8"     % "test",
    ),
    unmanagedJars in Compile ~= { uj =>
      Seq(Attributed.blank(file(System.getProperty("java.home").dropRight(3) + "lib/tools.jar"))) ++ uj
    },
    assemblyExcludedJars in assembly := {
      val cp = (fullClasspath in assembly).value
      cp.filter { _.data.getName == "tools.jar" }
    },
    GitlabPlugin.autoImport.gitlabProjectId := Some(21491706),
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials")
  )


lazy val engine = (project in file("gatk-engine"))
  .dependsOn(util)
  .settings(
    name := "gatk-engine",
    libraryDependencies ++= Seq(
      "com.github.samtools" % "htsjdk" % htsjdkVersion,
      "com.intel.gkl" % "gkl" % "0.6.0",
      "net.java.dev.jets3t" % "jets3t" % "0.8.1",
      "org.simpleframework" % "simple-xml" % "2.0.4",
      "org.apache.commons" % "commons-math" % "2.2",
      "commons-collections" % "commons-collections" % "3.2.2",
      "org.huoc" % "cofoja" % "1.3.1",
      "org.testng" % "testng" % "6.8" % "test",
      "commons-lang" % "commons-lang" % "2.5" % "test",
      "it.unimi.dsi" % "fastutil" % "6.5.3" % "test",
      "com.google.caliper" % "caliper" % "0.5-rc1" % "test" exclude("com.google.guava", "guava"),
    ),
    GitlabPlugin.autoImport.gitlabProjectId := Some(21491706),
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials")
  )

lazy val toolsPublic = (project in file("gatk-tools-public"))
  .dependsOn(util)
  .dependsOn(engine)
  .settings(
    name := "gatk-tools-public",
    libraryDependencies ++= Seq(
      "com.github.samtools" % "htsjdk" % htsjdkVersion,
    ),
    GitlabPlugin.autoImport.gitlabProjectId := Some(21491706),
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials")
  )

lazy val queue = (project in file("gatk-queue"))
  .dependsOn(util)
  .settings(
    name := "gatk-queue",
    libraryDependencies ++= Seq(
      "org.scala-lang" % "scala-library" % s"${scalaVersion.value}",
      "org.scala-lang" % "scala-compiler" % s"${scalaVersion.value}",
      "org.reflections" % "reflections" % "0.9.11",
      "net.sf.jgrapht" % "jgrapht" % "0.8.3",
      "org.apache.commons" % "commons-email" % "1.2",
      "javax.mail" % "mail" % "1.4.4",
      "commons-io" % "commons-io" % "2.1",
      "commons-lang" % "commons-lang" % "2.5",
      "us.levk" % "drmaa-gridengine" % "6.2u5",
      "net.java.dev.jna" % "jna" % "3.2.7",
      "com.github.samtools" % "htsjdk" % htsjdkVersion,
      "log4j" % "log4j" % "1.2.15" exclude("com.sun.jdmk", "jmxtools") exclude("javax.jms", "jms") exclude("com.sun.jmx", "jmxri"),
      "org.testng" % "testng" % "6.8" % "test",
    ),
    GitlabPlugin.autoImport.gitlabProjectId := Some(21491706),
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials")
  )

// GitlabPlugin.autoImport.gitlabProjectId and credentials must be set
// explicitly in each sub-project. Using ThisBuild does not work.

lazy val root = (project in file("."))
  .aggregate(util, queue, engine, toolsPublic)
  .settings(
    GitlabPlugin.autoImport.gitlabProjectId := Some(21491706),
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials"),
    skip in publish := true,
    skip in publishLocal := true,
  )

