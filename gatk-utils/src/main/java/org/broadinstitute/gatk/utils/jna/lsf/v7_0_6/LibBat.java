
package org.broadinstitute.gatk.utils.jna.lsf.v7_0_6;

import com.sun.jna.*;
import com.sun.jna.ptr.*;
import org.broadinstitute.gatk.utils.jna.clibrary.JNAUtils;
import org.broadinstitute.gatk.utils.jna.clibrary.LibC;






@SuppressWarnings("unused")
public class LibBat {

    static {
        // via Platform LSF Configuration Reference, by default quiet the BSUB output.
        if ("Y".equals(System.getProperty("BSUB_QUIET", "Y")))
            LibC.setenv("BSUB_QUIET", "Y", 1);
        String lsfLibDir = System.getenv("LSF_LIBDIR");
        if (lsfLibDir != null) {
            NativeLibrary.addSearchPath("lsf", lsfLibDir);
            NativeLibrary.addSearchPath("bat", lsfLibDir);
        }
        if (Platform.isMac())
            NativeLibrary.getInstance("environhack");
        NativeLibrary liblsf = NativeLibrary.getInstance("lsf");
        Native.register("bat");
        // HACK: Running into a weird error:
        //   java.lang.UnsatisfiedLinkError: Unable to load library 'bat': <$LSF_LIBDIR>/libbat.so: undefined symbol: xdr_resourceInfoReq
        // This function is very clearly unsatisfied by running 'nm $LSF_LIBDIR/libbat.so | grep xdr_resourceInfoReq' but is
        // found in liblsf.so when running 'nm $LSF_LIBDIR/liblsf.so | grep xdr_resourceInfoReq'. For now holding on to a reference
        // to the LSF lib just in case this is a problem with the NativeLibrary's internal WeakReferences and the library being unloaded?
        liblsf.getFunction("xdr_resourceInfoReq").getName();
    }

    // Via support@platform.com:
    //    For equivalent api of bsub -a "xxx aaa qqq", option -a is not in struct submit, we
    //    have to use setOption_ to set it. setOption_ can be used in user program by including
    //    cmd.h or opensource.h of LSF opensource. You can refer to cmd.sub.c in opensource.
    //
    //    Here is a demonstration on the api for bsub -a
    //    =========================================================================
    //    /*define external setOption_ function*/
    //    extern int setOption_(int argc, char **argv, char *template,
    //    struct submit *req, int mask, int mask2, char **errMsg);
    //
    //    int setEsub(char *esub, struct submit *req) {
    //    int x;
    //    char *template, *arg[3];
    //    /*set esub with the following strings and set array length*/
    //    arg[0] = "blah";
    //    arg[1] = "-a";
    //    arg[2] = test;
    //    /* -a "test", You can add additional esubs in here.  Just make sure they're space delimited.  ie. "test mpich lammpi" */
    //    x=3;
    //    /*set template*/
    //    template = "a:"
    //    /*run setOption_()*/
    //    if (setOption_(x, arg, template, req, ~0, ~0, ~0, NULL) == -1) {
    //    return(-1);
    //    }
    //    else {
    //    return(0);
    //    }
    //    }
    //    =========================================================================

    public static native int setOption_(int argc, Pointer argv, String template, submit jobSubReq, int mask, int mask2, int mask3, Pointer errMsg);

    public static final int MAX_JOB_NAME_LEN = 4094;

    public static final String _PATH_NULL = "/dev/null";

    //public static int SKIP_SPACES (int word)  { while (word[0] == ' ' )  word++; }

    //public static void FREEUP_ARRAY(int num, Pointer vector) { FREE_STRING_VECTOR_ENTRIES(num, vector);  FREEUP(vector); }


    public static final float LSB_EVENT_VERSION3_0 = 3.0f;
    public static final float LSB_EVENT_VERSION3_1 = 3.1f;
    public static final float LSB_EVENT_VERSION3_2 = 3.2f;
    public static final float LSB_EVENT_VERSION4_0 = 4.0f;
    public static final float LSB_EVENT_VERSION4_1 = 4.1f;
    public static final float LSB_EVENT_VERSION4_2 = 4.2f;
    public static final float LSB_EVENT_VERSION5_0 = 5.0f;
    public static final float LSB_EVENT_VERSION5_1 = 5.1f;
    public static final float LSB_EVENT_VERSION6_0 = 6.0f;
    public static final float LSB_EVENT_VERSION6_1 = 6.1f;
    public static final float LSB_EVENT_VERSION6_2 = 6.2f;
    public static final float LSB_EVENT_VERSION7_0 = 7.0f;
    public static final float LSB_EVENT_VERSION7_0_1 = 7.01f;
    public static final float LSB_EVENT_VERSION7_0_2 = 7.02f;
    public static final float LSB_EVENT_VERSION7_0_3 = 7.03f;
    public static final float LSB_EVENT_VERSION7_0_4 = 7.04f;
    public static final float LSB_EVENT_VERSION7_0_5 = 7.05f;
    public static final float LSB_EVENT_VERSION7_0_6 = 7.06f;

    public static final String THIS_VERSION = "7.06";

    public static final int MAX_VERSION_LEN = 12;

    public static final int MAX_HPART_USERS = 100;

    public static final int MAX_CHARLEN = 20;

    public static final int MAX_LSB_NAME_LEN = 60;

    public static final int MAX_LSB_UG_NAME_LEN = 512;

    public static final int MAX_LSB_UG_HIERDEPTH = 64;

    public static final int MAX_CMD_DESC_LEN = 512;

    public static final int MAX_CALENDARS = 256;

    public static final int MAX_USER_EQUIVALENT = 128;

    public static final int MAX_USER_MAPPING = 128;

    public static final int MAXDESCLEN = 20 * 512;

    public static final int MAX_GROUPS = 1024;


    public static final int MAXFULLFILENAMELEN = 4096;
    public static final int MAXFULLPATHNAMELEN = 2 * MAXFULLFILENAMELEN;
    public static final int FILENAMEPADDING = 128;

    public static final String DEFAULT_MSG_DESC = "no description";

    public static final int MSGSIZE = 4096;

    public static final int MAXFULLMSGSIZE = 4 * MAXFULLFILENAMELEN;


    public static final int HOST_STAT_OK = 0x0;

    public static final int HOST_STAT_BUSY = 0x01;

    public static final int HOST_STAT_WIND = 0x02;

    public static final int HOST_STAT_DISABLED = 0x04;

    public static final int HOST_STAT_LOCKED = 0x08;

    public static final int HOST_STAT_FULL = 0x10;

    public static final int HOST_STAT_UNREACH = 0x20;

    public static final int HOST_STAT_UNAVAIL = 0x40;

    public static final int HOST_STAT_UNLICENSED = 0x80;

    public static final int HOST_STAT_NO_LIM = 0x100;

    public static final int HOST_STAT_EXCLUSIVE = 0x200;

    public static final int HOST_STAT_LOCKED_MASTER = 0x400;

    public static final int HOST_STAT_REMOTE_DISABLED = 0x800;

    public static final int HOST_STAT_LEASE_INACTIVE = 0x1000;


    public static final int HOST_STAT_DISABLED_RES = 0x4000;


    public static final int HOST_STAT_DISABLED_RMS = 0x8000;


    public static final int HOST_STAT_LOCKED_EGO = 0x10000;

    public static final int HOST_CLOSED_BY_ADMIN = 0x20000;

    public static final int HOST_STAT_CU_EXCLUSIVE = 0x40000;


    public static boolean LSB_HOST_OK(int status) {
        return (status == HOST_STAT_OK);
    }


    public static boolean LSB_HOST_BUSY(int status) {
        return ((status & HOST_STAT_BUSY) != 0);
    }


    public static boolean LSB_HOST_CLOSED(int status) {
        return ((status & (HOST_STAT_WIND | HOST_STAT_DISABLED | HOST_STAT_LOCKED | HOST_STAT_LOCKED_MASTER | HOST_STAT_FULL | HOST_STAT_CU_EXCLUSIVE | HOST_STAT_EXCLUSIVE | HOST_STAT_LEASE_INACTIVE | HOST_STAT_NO_LIM)) != 0);
    }


    public static boolean LSB_HOST_FULL(int status) {
        return ((status & HOST_STAT_FULL) != 0);
    }


    public static boolean LSB_HOST_UNLICENSED(int status) {
        return ((status & HOST_STAT_UNLICENSED) != 0);
    }


    public static boolean LSB_HOST_UNREACH(int status) {
        return ((status & HOST_STAT_UNREACH) != 0);
    }


    public static boolean LSB_HOST_UNAVAIL(int status) {
        return ((status & HOST_STAT_UNAVAIL) != 0);
    }



    public static final int HOST_BUSY_NOT = 0x000;

    public static final int HOST_BUSY_R15S = 0x001;

    public static final int HOST_BUSY_R1M = 0x002;

    public static final int HOST_BUSY_R15M = 0x004;

    public static final int HOST_BUSY_UT = 0x008;

    public static final int HOST_BUSY_PG = 0x010;

    public static final int HOST_BUSY_IO = 0x020;

    public static final int HOST_BUSY_LS = 0x040;

    public static final int HOST_BUSY_IT = 0x080;

    public static final int HOST_BUSY_TMP = 0x100;

    public static final int HOST_BUSY_SWP = 0x200;

    public static final int HOST_BUSY_MEM = 0x400;


    public static boolean LSB_ISBUSYON(int[] status, int index) {
        return (((status[(index) / LibLsf.INTEGER_BITS]) & (1 << (index) % LibLsf.INTEGER_BITS)) != 0);
    }



    public static final int QUEUE_STAT_OPEN = 0x01;

    public static final int QUEUE_STAT_ACTIVE = 0x02;

    public static final int QUEUE_STAT_RUN = 0x04;

    public static final int QUEUE_STAT_NOPERM = 0x08;

    public static final int QUEUE_STAT_DISC = 0x10;

    public static final int QUEUE_STAT_RUNWIN_CLOSE = 0x20;


    public static final int Q_ATTRIB_EXCLUSIVE = 0x01;

    public static final int Q_ATTRIB_DEFAULT = 0x02;

    public static final int Q_ATTRIB_FAIRSHARE = 0x04;

    public static final int Q_ATTRIB_PREEMPTIVE = 0x08;

    public static final int Q_ATTRIB_NQS = 0x10;

    public static final int Q_ATTRIB_RECEIVE = 0x20;

    public static final int Q_ATTRIB_PREEMPTABLE = 0x40;

    public static final int Q_ATTRIB_BACKFILL = 0x80;

    public static final int Q_ATTRIB_HOST_PREFER = 0x100;

    public static final int Q_ATTRIB_NONPREEMPTIVE = 0x200;

    public static final int Q_ATTRIB_NONPREEMPTABLE = 0x400;

    public static final int Q_ATTRIB_NO_INTERACTIVE = 0x800;

    public static final int Q_ATTRIB_ONLY_INTERACTIVE = 0x1000;

    public static final int Q_ATTRIB_NO_HOST_TYPE = 0x2000;

    public static final int Q_ATTRIB_IGNORE_DEADLINE = 0x4000;

    public static final int Q_ATTRIB_CHKPNT = 0x8000;

    public static final int Q_ATTRIB_RERUNNABLE = 0x10000;

    public static final int Q_ATTRIB_EXCL_RMTJOB = 0x20000;

    public static final int Q_ATTRIB_MC_FAST_SCHEDULE = 0x40000;

    public static final int Q_ATTRIB_ENQUE_INTERACTIVE_AHEAD = 0x80000;



    public static final int Q_MC_FLAG = 0xf00000;

    public static final int Q_ATTRIB_LEASE_LOCAL = 0x100000;

    public static final int Q_ATTRIB_LEASE_ONLY = 0x200000;

    public static final int Q_ATTRIB_RMT_BATCH_LOCAL = 0x300000;

    public static final int Q_ATTRIB_RMT_BATCH_ONLY = 0x400000;


    public static final int Q_ATTRIB_RESOURCE_RESERVE = 0x1000000;

    public static final int Q_ATTRIB_FS_DISPATCH_ORDER_QUEUE = 0x2000000;

    public static final int Q_ATTRIB_BATCH = 0x4000000;

    public static final int Q_ATTRIB_ONLINE = 0x8000000;

    public static final int Q_ATTRIB_INTERRUPTIBLE_BACKFILL = 0x10000000;

    public static final int Q_ATTRIB_APS = 0x20000000;

    public static final int Q_ATTRIB_NO_HIGHER_RESERVE = 0x40000000;

    public static final int Q_ATTRIB_NO_HOST_VALID = 0x80000000;



    public static int IS_ONLINE_QUEUE(queueInfoEnt Q) {
        return (Q.qAttrib & Q_ATTRIB_ONLINE);
    }

    public static int IS_BATCH_QUEUE(queueInfoEnt Q) {
        return (Q.qAttrib & Q_ATTRIB_BATCH);
    }


    public static boolean IS_LEASE_LOCAL_QUEUE(queueInfoEnt Q) {
        return ((Q.qAttrib & Q_MC_FLAG) == Q_ATTRIB_LEASE_LOCAL);
    }

    public static boolean IS_LEASE_ONLY_QUEUE(queueInfoEnt Q) {
        return ((Q.qAttrib & Q_MC_FLAG) == Q_ATTRIB_LEASE_ONLY);
    }

    public static boolean IS_RMT_BATCH_LOCAL_QUEUE(queueInfoEnt Q) {
        return ((Q.qAttrib & Q_MC_FLAG) == Q_ATTRIB_RMT_BATCH_LOCAL);
    }

    public static boolean IS_RMT_BATCH_ONLY_QUEUE(queueInfoEnt Q) {
        return ((Q.qAttrib & Q_MC_FLAG) == Q_ATTRIB_RMT_BATCH_ONLY);
    }

    public static boolean IS_LEASE_QUEUE(queueInfoEnt Q) {
        return (IS_LEASE_LOCAL_QUEUE(Q) || IS_LEASE_ONLY_QUEUE(Q));
    }

    public static boolean IS_RMT_BATCH_QUEUE(queueInfoEnt Q) {
        return (IS_RMT_BATCH_LOCAL_QUEUE(Q) || IS_RMT_BATCH_ONLY_QUEUE(Q));
    }

    public static boolean IS_MC_QUEUE(queueInfoEnt Q) {
        return (IS_LEASE_QUEUE(Q) || IS_RMT_BATCH_QUEUE(Q));
    }

    public static int SET_LEASE_LOCAL_QUEUE(queueInfoEnt Q) {
        return (Q.qAttrib |= Q_ATTRIB_LEASE_LOCAL);
    }

    public static int SET_LEASE_ONLY_QUEUE(queueInfoEnt Q) {
        return (Q.qAttrib |= Q_ATTRIB_LEASE_ONLY);
    }

    public static int SET_RMT_BATCH_LOCAL_QUEUE(queueInfoEnt Q) {
        return (Q.qAttrib |= Q_ATTRIB_RMT_BATCH_LOCAL);
    }

    public static int SET_RMT_BATCH_ONLY_QUEUE(queueInfoEnt Q) {
        return (Q.qAttrib |= Q_ATTRIB_RMT_BATCH_ONLY);
    }

    public static int CLR_MC_QUEUE_FLAG(queueInfoEnt Q) {
        return (Q.qAttrib &= ~Q_MC_FLAG);
    }



    public static final int MASTER_NULL = 200;
    public static final int MASTER_RESIGN = 201;
    public static final int MASTER_RECONFIG = 202;
    public static final int MASTER_FATAL = 203;
    public static final int MASTER_MEM = 204;
    public static final int MASTER_CONF = 205;
    public static final int MASTER_EVENT = 206;
    public static final int MASTER_DISABLE = 207;

    public static final int MBD_USER_CMD = 1;
    public static final int MBD_NON_USER_CMD = 2;


    public static final int JOB_STAT_NULL = 0x00;

    public static final int JOB_STAT_PEND = 0x01;

    public static final int JOB_STAT_PSUSP = 0x02;

    public static final int JOB_STAT_RUN = 0x04;

    public static final int JOB_STAT_SSUSP = 0x08;

    public static final int JOB_STAT_USUSP = 0x10;

    public static final int JOB_STAT_EXIT = 0x20;

    public static final int JOB_STAT_DONE = 0x40;

    public static final int JOB_STAT_PDONE = (0x80);

    public static final int JOB_STAT_PERR = (0x100);

    public static final int JOB_STAT_WAIT = (0x200);

    public static final int JOB_STAT_UNKWN = 0x10000;


    public static final int EVENT_JOB_NEW = 1;

    public static final int EVENT_JOB_START = 2;

    public static final int EVENT_JOB_STATUS = 3;

    public static final int EVENT_JOB_SWITCH = 4;

    public static final int EVENT_JOB_MOVE = 5;

    public static final int EVENT_QUEUE_CTRL = 6;

    public static final int EVENT_HOST_CTRL = 7;

    public static final int EVENT_MBD_DIE = 8;

    public static final int EVENT_MBD_UNFULFILL = 9;

    public static final int EVENT_JOB_FINISH = 10;

    public static final int EVENT_LOAD_INDEX = 11;

    public static final int EVENT_CHKPNT = 12;

    public static final int EVENT_MIG = 13;

    public static final int EVENT_PRE_EXEC_START = 14;

    public static final int EVENT_MBD_START = 15;

    public static final int EVENT_JOB_ROUTE = 16;

    public static final int EVENT_JOB_MODIFY = 17;

    public static final int EVENT_JOB_SIGNAL = 18;

    public static final int EVENT_CAL_NEW = 19;

    public static final int EVENT_CAL_MODIFY = 20;

    public static final int EVENT_CAL_DELETE = 21;

    public static final int EVENT_JOB_FORWARD = 22;

    public static final int EVENT_JOB_ACCEPT = 23;

    public static final int EVENT_STATUS_ACK = 24;

    public static final int EVENT_JOB_EXECUTE = 25;

    public static final int EVENT_JOB_MSG = 26;

    public static final int EVENT_JOB_MSG_ACK = 27;

    public static final int EVENT_JOB_REQUEUE = 28;

    public static final int EVENT_JOB_OCCUPY_REQ = 29;

    public static final int EVENT_JOB_VACATED = 30;

    public static final int EVENT_JOB_SIGACT = 32;

    public static final int EVENT_SBD_JOB_STATUS = 34;

    public static final int EVENT_JOB_START_ACCEPT = 35;

    public static final int EVENT_CAL_UNDELETE = 36;

    public static final int EVENT_JOB_CLEAN = 37;

    public static final int EVENT_JOB_EXCEPTION = 38;

    public static final int EVENT_JGRP_ADD = 39;

    public static final int EVENT_JGRP_MOD = 40;

    public static final int EVENT_JGRP_CTRL = 41;

    public static final int EVENT_JOB_FORCE = 42;

    public static final int EVENT_LOG_SWITCH = 43;

    public static final int EVENT_JOB_MODIFY2 = 44;

    public static final int EVENT_JGRP_STATUS = 45;

    public static final int EVENT_JOB_ATTR_SET = 46;

    public static final int EVENT_JOB_EXT_MSG = 47;

    public static final int EVENT_JOB_ATTA_DATA = 48;

    public static final int EVENT_JOB_CHUNK = 49;

    public static final int EVENT_SBD_UNREPORTED_STATUS = 50;

    public static final int EVENT_ADRSV_FINISH = 51;

    public static final int EVENT_HGHOST_CTRL = 52;

    public static final int EVENT_CPUPROFILE_STATUS = 53;

    public static final int EVENT_DATA_LOGGING = 54;

    public static final int EVENT_JOB_RUN_RUSAGE = 55;

    public static final int EVENT_END_OF_STREAM = 56;

    public static final int EVENT_SLA_RECOMPUTE = 57;

    public static final int EVENT_METRIC_LOG = 58;

    public static final int EVENT_TASK_FINISH = 59;

    public static final int EVENT_JOB_RESIZE_NOTIFY_START = 60;

    public static final int EVENT_JOB_RESIZE_NOTIFY_ACCEPT = 61;

    public static final int EVENT_JOB_RESIZE_NOTIFY_DONE = 62;

    public static final int EVENT_JOB_RESIZE_RELEASE = 63;

    public static final int EVENT_JOB_RESIZE_CANCEL = 64;

    public static final int EVENT_JOB_RESIZE = 65;

    public static final int EVENT_JOB_ARRAY_ELEMENT = 66;

    public static final int EVENT_MBD_SIM_STATUS = 67;


    public static final int EVENT_JOB_RELATED = 1;

    public static final int EVENT_NON_JOB_RELATED = 0;



    public static final int PEND_JOB_REASON = 0;

    public static final int PEND_JOB_NEW = 1;

    public static final int PEND_JOB_START_TIME = 2;

    public static final int PEND_JOB_DEPEND = 3;

    public static final int PEND_JOB_DEP_INVALID = 4;

    public static final int PEND_JOB_MIG = 5;

    public static final int PEND_JOB_PRE_EXEC = 6;

    public static final int PEND_JOB_NO_FILE = 7;

    public static final int PEND_JOB_ENV = 8;

    public static final int PEND_JOB_PATHS = 9;

    public static final int PEND_JOB_OPEN_FILES = 10;

    public static final int PEND_JOB_EXEC_INIT = 11;

    public static final int PEND_JOB_RESTART_FILE = 12;

    public static final int PEND_JOB_DELAY_SCHED = 13;

    public static final int PEND_JOB_SWITCH = 14;

    public static final int PEND_JOB_DEP_REJECT = 15;

    public static final int PEND_JOB_JS_DISABLED = 16;

    public static final int PEND_JOB_NO_PASSWD = 17;

    public static final int PEND_JOB_LOGON_FAIL = 18;

    public static final int PEND_JOB_MODIFY = 19;

    public static final int PEND_JOB_TIME_INVALID = 20;

    public static final int PEND_TIME_EXPIRED = 21;

    public static final int PEND_JOB_REQUEUED = 23;

    public static final int PEND_WAIT_NEXT = 24;

    public static final int PEND_JGRP_HOLD = 25;

    public static final int PEND_JGRP_INACT = 26;

    public static final int PEND_JGRP_WAIT = 27;

    public static final int PEND_JOB_RCLUS_UNREACH = 28;

    public static final int PEND_JOB_QUE_REJECT = 29;

    public static final int PEND_JOB_RSCHED_START = 30;

    public static final int PEND_JOB_RSCHED_ALLOC = 31;

    public static final int PEND_JOB_FORWARDED = 32;

    public static final int PEND_JOB_RMT_ZOMBIE = 33;

    public static final int PEND_JOB_ENFUGRP = 34;

    public static final int PEND_SYS_UNABLE = 35;

    public static final int PEND_JGRP_RELEASE = 36;

    public static final int PEND_HAS_RUN = 37;

    public static final int PEND_JOB_ARRAY_JLIMIT = 38;

    public static final int PEND_CHKPNT_DIR = 39;

    public static final int PEND_CHUNK_FAIL = 40;

    public static final int PEND_JOB_SLA_MET = 41;

    public static final int PEND_JOB_APP_NOEXIST = 42;

    public static final int PEND_APP_PROCLIMIT = 43;

    public static final int PEND_EGO_NO_HOSTS = 44;

    public static final int PEND_JGRP_JLIMIT = 45;

    public static final int PEND_PREEXEC_LIMIT = 46;

    public static final int PEND_REQUEUE_LIMIT = 47;

    public static final int PEND_BAD_RESREQ = 48;

    public static final int PEND_RSV_INACTIVE = 49;

    public static final int PEND_WAITING_RESUME = 50;

    public static final int PEND_SLOT_COMPOUND = 51;


    public static final int PEND_QUE_INACT = 301;

    public static final int PEND_QUE_WINDOW = 302;

    public static final int PEND_QUE_JOB_LIMIT = 303;

    public static final int PEND_QUE_USR_JLIMIT = 304;

    public static final int PEND_QUE_USR_PJLIMIT = 305;

    public static final int PEND_QUE_PRE_FAIL = 306;

    public static final int PEND_NQS_RETRY = 307;

    public static final int PEND_NQS_REASONS = 308;

    public static final int PEND_NQS_FUN_OFF = 309;

    public static final int PEND_SYS_NOT_READY = 310;

    public static final int PEND_SBD_JOB_REQUEUE = 311;

    public static final int PEND_JOB_SPREAD_TASK = 312;

    public static final int PEND_QUE_SPREAD_TASK = 313;

    public static final int PEND_QUE_PJOB_LIMIT = 314;

    public static final int PEND_QUE_WINDOW_WILL_CLOSE = 315;

    public static final int PEND_QUE_PROCLIMIT = 316;

    public static final int PEND_SBD_PLUGIN = 317;

    public static final int PEND_WAIT_SIGN_LEASE = 318;

    public static final int PEND_WAIT_SLOT_SHARE = 319;


    public static final int PEND_USER_JOB_LIMIT = 601;

    public static final int PEND_UGRP_JOB_LIMIT = 602;

    public static final int PEND_USER_PJOB_LIMIT = 603;

    public static final int PEND_UGRP_PJOB_LIMIT = 604;

    public static final int PEND_USER_RESUME = 605;

    public static final int PEND_USER_STOP = 607;

    public static final int PEND_NO_MAPPING = 608;

    public static final int PEND_RMT_PERMISSION = 609;

    public static final int PEND_ADMIN_STOP = 610;

    public static final int PEND_MLS_INVALID = 611;

    public static final int PEND_MLS_CLEARANCE = 612;

    public static final int PEND_MLS_RHOST = 613;

    public static final int PEND_MLS_DOMINATE = 614;

    public static final int PEND_MLS_FATAL = 615;

    public static final int PEND_INTERNAL_STOP = 616;



    public static final int PEND_HOST_RES_REQ = 1001;

    public static final int PEND_HOST_NONEXCLUSIVE = 1002;

    public static final int PEND_HOST_JOB_SSUSP = 1003;

    public static final int PEND_HOST_PART_PRIO = 1004;

    public static final int PEND_SBD_GETPID = 1005;

    public static final int PEND_SBD_LOCK = 1006;

    public static final int PEND_SBD_ZOMBIE = 1007;

    public static final int PEND_SBD_ROOT = 1008;

    public static final int PEND_HOST_WIN_WILL_CLOSE = 1009;

    public static final int PEND_HOST_MISS_DEADLINE = 1010;

    public static final int PEND_FIRST_HOST_INELIGIBLE = 1011;

    public static final int PEND_HOST_EXCLUSIVE_RESERVE = 1012;

    public static final int PEND_FIRST_HOST_REUSE = 1013;

    public static final int PEND_HOST_DISABLED = 1301;

    public static final int PEND_HOST_LOCKED = 1302;

    public static final int PEND_HOST_LESS_SLOTS = 1303;

    public static final int PEND_HOST_WINDOW = 1304;

    public static final int PEND_HOST_JOB_LIMIT = 1305;

    public static final int PEND_QUE_PROC_JLIMIT = 1306;

    public static final int PEND_QUE_HOST_JLIMIT = 1307;

    public static final int PEND_USER_PROC_JLIMIT = 1308;

    public static final int PEND_HOST_USR_JLIMIT = 1309;

    public static final int PEND_HOST_QUE_MEMB = 1310;

    public static final int PEND_HOST_USR_SPEC = 1311;

    public static final int PEND_HOST_PART_USER = 1312;

    public static final int PEND_HOST_NO_USER = 1313;

    public static final int PEND_HOST_ACCPT_ONE = 1314;

    public static final int PEND_LOAD_UNAVAIL = 1315;

    public static final int PEND_HOST_NO_LIM = 1316;

    public static final int PEND_HOST_UNLICENSED = 1317;

    public static final int PEND_HOST_QUE_RESREQ = 1318;

    public static final int PEND_HOST_SCHED_TYPE = 1319;

    public static final int PEND_JOB_NO_SPAN = 1320;

    public static final int PEND_QUE_NO_SPAN = 1321;

    public static final int PEND_HOST_EXCLUSIVE = 1322;

    public static final int PEND_HOST_JS_DISABLED = 1323;

    public static final int PEND_UGRP_PROC_JLIMIT = 1324;

    public static final int PEND_BAD_HOST = 1325;

    public static final int PEND_QUEUE_HOST = 1326;

    public static final int PEND_HOST_LOCKED_MASTER = 1327;

    public static final int PEND_HOST_LESS_RSVSLOTS = 1328;

    public static final int PEND_HOST_LESS_DURATION = 1329;

    public static final int PEND_HOST_NO_RSVID = 1330;

    public static final int PEND_HOST_LEASE_INACTIVE = 1331;

    public static final int PEND_HOST_ADRSV_ACTIVE = 1332;

    public static final int PEND_QUE_RSVID_NOMATCH = 1333;

    public static final int PEND_HOST_GENERAL = 1334;

    public static final int PEND_HOST_RSV = 1335;

    public static final int PEND_HOST_NOT_CU = 1336;

    public static final int PEND_HOST_CU_EXCL = 1337;

    public static final int PEND_HOST_CU_OCCUPIED = 1338;

    public static final int PEND_HOST_USABLE_CU = 1339;

    public static final int PEND_JOB_FIRST_CU = 1340;

    public static final int PEND_HOST_CU_EXCL_RSV = 1341;

    public static final int PEND_JOB_CU_MAXCUS = 1342;

    public static final int PEND_JOB_CU_BALANCE = 1343;

    public static final int PEND_CU_TOPLIB_HOST = 1344;


    public static final int PEND_SBD_UNREACH = 1601;

    public static final int PEND_SBD_JOB_QUOTA = 1602;

    public static final int PEND_JOB_START_FAIL = 1603;

    public static final int PEND_JOB_START_UNKNWN = 1604;

    public static final int PEND_SBD_NO_MEM = 1605;

    public static final int PEND_SBD_NO_PROCESS = 1606;

    public static final int PEND_SBD_SOCKETPAIR = 1607;

    public static final int PEND_SBD_JOB_ACCEPT = 1608;

    public static final int PEND_LEASE_JOB_REMOTE_DISPATCH = 1609;

    public static final int PEND_JOB_RESTART_FAIL = 1610;

    public static final int PEND_HOST_LOAD = 2001;


    public static final int PEND_HOST_QUE_RUSAGE = 2301;


    public static final int PEND_HOST_JOB_RUSAGE = 2601;


    public static final int PEND_RMT_JOB_FORGOTTEN = 2901;

    public static final int PEND_RMT_IMPT_JOBBKLG = 2902;

    public static final int PEND_RMT_MAX_RSCHED_TIME = 2903;

    public static final int PEND_RMT_MAX_PREEXEC_RETRY = 2904;

    public static final int PEND_RMT_QUEUE_CLOSED = 2905;

    public static final int PEND_RMT_QUEUE_INACTIVE = 2906;

    public static final int PEND_RMT_QUEUE_CONGESTED = 2907;

    public static final int PEND_RMT_QUEUE_DISCONNECT = 2908;

    public static final int PEND_RMT_QUEUE_NOPERMISSION = 2909;

    public static final int PEND_RMT_BAD_TIME = 2910;

    public static final int PEND_RMT_PERMISSIONS = 2911;

    public static final int PEND_RMT_PROC_NUM = 2912;

    public static final int PEND_RMT_QUEUE_USE = 2913;

    public static final int PEND_RMT_NO_INTERACTIVE = 2914;

    public static final int PEND_RMT_ONLY_INTERACTIVE = 2915;

    public static final int PEND_RMT_PROC_LESS = 2916;

    public static final int PEND_RMT_OVER_LIMIT = 2917;

    public static final int PEND_RMT_BAD_RESREQ = 2918;

    public static final int PEND_RMT_CREATE_JOB = 2919;

    public static final int PEND_RMT_RERUN = 2920;

    public static final int PEND_RMT_EXIT_REQUEUE = 2921;

    public static final int PEND_RMT_REQUEUE = 2922;

    public static final int PEND_RMT_JOB_FORWARDING = 2923;

    public static final int PEND_RMT_QUEUE_INVALID = 2924;

    public static final int PEND_RMT_QUEUE_NO_EXCLUSIVE = 2925;

    public static final int PEND_RMT_UGROUP_MEMBER = 2926;

    public static final int PEND_RMT_INTERACTIVE_RERUN = 2927;

    public static final int PEND_RMT_JOB_START_FAIL = 2928;

    public static final int PEND_RMT_FORWARD_FAIL_UGROUP_MEMBER = 2930;

    public static final int PEND_RMT_HOST_NO_RSVID = 2931;

    public static final int PEND_RMT_APP_NULL = 2932;

    public static final int PEND_RMT_BAD_RUNLIMIT = 2933;

    public static final int PEND_RMT_OVER_QUEUE_LIMIT = 2934;

    public static final int PEND_RMT_WHEN_NO_SLOTS = 2935;


    public static final int PEND_GENERAL_LIMIT_USER = 3201;

    public static final int PEND_GENERAL_LIMIT_QUEUE = 3501;

    public static final int PEND_GENERAL_LIMIT_PROJECT = 3801;

    public static final int PEND_GENERAL_LIMIT_CLUSTER = 4101;

    public static final int PEND_GENERAL_LIMIT_HOST = 4401;

    public static final int PEND_GENERAL_LIMIT_JOBS_USER = 4701;

    public static final int PEND_GENERAL_LIMIT_JOBS_QUEUE = 4702;

    public static final int PEND_GENERAL_LIMIT_JOBS_PROJECT = 4703;

    public static final int PEND_GENERAL_LIMIT_JOBS_CLUSTER = 4704;

    public static final int PEND_GENERAL_LIMIT_JOBS_HOST = 4705;


    public static final int PEND_RMS_PLUGIN_INTERNAL = 4900;

    public static final int PEND_RMS_PLUGIN_RLA_COMM = 4901;

    public static final int PEND_RMS_NOT_AVAILABLE = 4902;

    public static final int PEND_RMS_FAIL_TOPOLOGY = 4903;

    public static final int PEND_RMS_FAIL_ALLOC = 4904;

    public static final int PEND_RMS_SPECIAL_NO_PREEMPT_BACKFILL = 4905;

    public static final int PEND_RMS_SPECIAL_NO_RESERVE = 4906;

    public static final int PEND_RMS_RLA_INTERNAL = 4907;

    public static final int PEND_RMS_NO_SLOTS_SPECIAL = 4908;

    public static final int PEND_RMS_RLA_NO_SUCH_USER = 4909;

    public static final int PEND_RMS_RLA_NO_SUCH_HOST = 4910;

    public static final int PEND_RMS_CHUNKJOB = 4911;

    public static final int PEND_RLA_PROTOMISMATCH = 4912;

    public static final int PEND_RMS_BAD_TOPOLOGY = 4913;

    public static final int PEND_RMS_RESREQ_MCONT = 4914;

    public static final int PEND_RMS_RESREQ_PTILE = 4915;

    public static final int PEND_RMS_RESREQ_NODES = 4916;

    public static final int PEND_RMS_RESREQ_BASE = 4917;

    public static final int PEND_RMS_RESREQ_RAILS = 4918;

    public static final int PEND_RMS_RESREQ_RAILMASK = 4919;



    public static final int PEND_MAUI_UNREACH = 5000;

    public static final int PEND_MAUI_FORWARD = 5001;

    public static final int PEND_MAUI_REASON = 5030;


    public static final int PEND_CPUSET_ATTACH = 5200;

    public static final int PEND_CPUSET_NOT_CPUSETHOST = 5201;

    public static final int PEND_CPUSET_TOPD_INIT = 5202;

    public static final int PEND_CPUSET_TOPD_TIME_OUT = 5203;

    public static final int PEND_CPUSET_TOPD_FAIL_ALLOC = 5204;

    public static final int PEND_CPUSET_TOPD_BAD_REQUEST = 5205;

    public static final int PEND_CPUSET_TOPD_INTERNAL = 5206;

    public static final int PEND_CPUSET_TOPD_SYSAPI_ERR = 5207;

    public static final int PEND_CPUSET_TOPD_NOSUCH_NAME = 5208;

    public static final int PEND_CPUSET_TOPD_JOB_EXIST = 5209;

    public static final int PEND_CPUSET_TOPD_NO_MEMORY = 5210;

    public static final int PEND_CPUSET_TOPD_INVALID_USER = 5211;

    public static final int PEND_CPUSET_TOPD_PERM_DENY = 5212;

    public static final int PEND_CPUSET_TOPD_UNREACH = 5213;

    public static final int PEND_CPUSET_TOPD_COMM_ERR = 5214;


    public static final int PEND_CPUSET_PLUGIN_INTERNAL = 5215;

    public static final int PEND_CPUSET_CHUNKJOB = 5216;

    public static final int PEND_CPUSET_CPULIST = 5217;

    public static final int PEND_CPUSET_MAXRADIUS = 5218;


    public static final int PEND_NODE_ALLOC_FAIL = 5300;


    public static final int PEND_RMSRID_UNAVAIL = 5400;


    public static final int PEND_NO_FREE_CPUS = 5450;

    public static final int PEND_TOPOLOGY_UNKNOWN = 5451;

    public static final int PEND_BAD_TOPOLOGY = 5452;

    public static final int PEND_RLA_COMM = 5453;

    public static final int PEND_RLA_NO_SUCH_USER = 5454;

    public static final int PEND_RLA_INTERNAL = 5455;

    public static final int PEND_RLA_NO_SUCH_HOST = 5456;

    public static final int PEND_RESREQ_TOOFEWSLOTS = 5457;


    public static final int PEND_PSET_PLUGIN_INTERNAL = 5500;

    public static final int PEND_PSET_RESREQ_PTILE = 5501;

    public static final int PEND_PSET_RESREQ_CELLS = 5502;

    public static final int PEND_PSET_CHUNKJOB = 5503;

    public static final int PEND_PSET_NOTSUPPORT = 5504;

    public static final int PEND_PSET_BIND_FAIL = 5505;

    public static final int PEND_PSET_RESREQ_CELLLIST = 5506;



    public static final int PEND_SLURM_PLUGIN_INTERNAL = 5550;

    public static final int PEND_SLURM_RESREQ_NODES = 5551;

    public static final int PEND_SLURM_RESREQ_NODE_ATTR = 5552;

    public static final int PEND_SLURM_RESREQ_EXCLUDE = 5553;

    public static final int PEND_SLURM_RESREQ_NODELIST = 5554;

    public static final int PEND_SLURM_RESREQ_CONTIGUOUS = 5555;

    public static final int PEND_SLURM_ALLOC_UNAVAIL = 5556;

    public static final int PEND_SLURM_RESREQ_BAD_CONSTRAINT = 5557;


    public static final int PEND_CRAYX1_SSP = 5600;

    public static final int PEND_CRAYX1_MSP = 5601;

    public static final int PEND_CRAYX1_PASS_LIMIT = 5602;


    public static final int PEND_CRAYXT3_ASSIGN_FAIL = 5650;


    public static final int PEND_BLUEGENE_PLUGIN_INTERNAL = 5700;

    public static final int PEND_BLUEGENE_ALLOC_UNAVAIL = 5701;

    public static final int PEND_BLUEGENE_NOFREEMIDPLANES = 5702;

    public static final int PEND_BLUEGENE_NOFREEQUARTERS = 5703;

    public static final int PEND_BLUEGENE_NOFREENODECARDS = 5704;


    public static final int PEND_RESIZE_FIRSTHOSTUNAVAIL = 5705;

    public static final int PEND_RESIZE_MASTERSUSP = 5706;

    public static final int PEND_RESIZE_MASTER_SAME = 5707;

    public static final int PEND_RESIZE_SPAN_PTILE = 5708;

    public static final int PEND_RESIZE_SPAN_HOSTS = 5709;

    public static final int PEND_RESIZE_LEASE_HOST = 5710;


    public static final int PEND_COMPOUND_RESREQ_OLD_LEASE_HOST = 5800;

    public static final int PEND_COMPOUND_RESREQ_TOPLIB_HOST = 5801;

    public static final int PEND_MULTIPHASE_RESREQ_OLD_LEASE_HOST = 5900;


    public static final int PEND_PS_PLUGIN_INTERNAL = 5750;

    public static final int PEND_PS_MBD_SYNC = 5751;



    public static final int PEND_CUSTOMER_MIN = 20001;

    public static final int PEND_CUSTOMER_MAX = 25000;


    public static final int PEND_MAX_REASONS = 25001;



    public static final int SUSP_USER_REASON = 0x00000000;

    public static final int SUSP_USER_RESUME = 0x00000001;

    public static final int SUSP_USER_STOP = 0x00000002;


    public static final int SUSP_QUEUE_REASON = 0x00000004;

    public static final int SUSP_QUEUE_WINDOW = 0x00000008;

    public static final int SUSP_RESCHED_PREEMPT = 0x00000010;

    public static final int SUSP_HOST_LOCK = 0x00000020;

    public static final int SUSP_LOAD_REASON = 0x00000040;

    public static final int SUSP_MBD_PREEMPT = 0x00000080;

    public static final int SUSP_SBD_PREEMPT = 0x00000100;

    public static final int SUSP_QUE_STOP_COND = 0x00000200;

    public static final int SUSP_QUE_RESUME_COND = 0x00000400;

    public static final int SUSP_PG_IT = 0x00000800;

    public static final int SUSP_REASON_RESET = 0x00001000;

    public static final int SUSP_LOAD_UNAVAIL = 0x00002000;

    public static final int SUSP_ADMIN_STOP = 0x00004000;

    public static final int SUSP_RES_RESERVE = 0x00008000;

    public static final int SUSP_MBD_LOCK = 0x00010000;

    public static final int SUSP_RES_LIMIT = 0x00020000;

    public static final int SUSP_SBD_STARTUP = 0x00040000;

    public static final int SUSP_HOST_LOCK_MASTER = 0x00080000;

    public static final int SUSP_HOST_RSVACTIVE = 0x00100000;

    public static final int SUSP_DETAILED_SUBREASON = 0x00200000;

    public static final int SUSP_GLB_LICENSE_PREEMPT = 0x00400000;


    public static final int SUSP_CRAYX1_POSTED = 0x00800000;

    public static final int SUSP_ADVRSV_EXPIRED = 0x01000000;


    public static final int SUB_REASON_RUNLIMIT = 0x00000001;

    public static final int SUB_REASON_DEADLINE = 0x00000002;

    public static final int SUB_REASON_PROCESSLIMIT = 0x00000004;

    public static final int SUB_REASON_CPULIMIT = 0x00000008;

    public static final int SUB_REASON_MEMLIMIT = 0x00000010;

    public static final int SUB_REASON_THREADLIMIT = 0x00000020;

    public static final int SUB_REASON_SWAPLIMIT = 0x00000040;

    public static final int SUB_REASON_CRAYX1_ACCOUNTID = 0x00000001;

    public static final int SUB_REASON_CRAYX1_ATTRIBUTE = 0x00000002;

    public static final int SUB_REASON_CRAYX1_BLOCKED = 0x00000004;

    public static final int SUB_REASON_CRAYX1_RESTART = 0x00000008;

    public static final int SUB_REASON_CRAYX1_DEPTH = 0x00000010;

    public static final int SUB_REASON_CRAYX1_GID = 0x00000020;

    public static final int SUB_REASON_CRAYX1_GASID = 0x00000040;

    public static final int SUB_REASON_CRAYX1_HARDLABEL = 0x00000080;

    public static final int SUB_REASON_CRAYX1_LIMIT = 0x00000100;

    public static final int SUB_REASON_CRAYX1_MEMORY = 0x00000200;

    public static final int SUB_REASON_CRAYX1_SOFTLABEL = 0x00000400;

    public static final int SUB_REASON_CRAYX1_SIZE = 0x00000800;

    public static final int SUB_REASON_CRAYX1_TIME = 0x00001000;

    public static final int SUB_REASON_CRAYX1_UID = 0x00002000;

    public static final int SUB_REASON_CRAYX1_WIDTH = 0x00004000;

    public static final int EXIT_NORMAL = 0x00000000;

    public static final int EXIT_RESTART = 0x00000001;

    public static final int EXIT_ZOMBIE = 0x00000002;

    public static final int FINISH_PEND = 0x00000004;

    public static final int EXIT_KILL_ZOMBIE = 0x00000008;

    public static final int EXIT_ZOMBIE_JOB = 0x00000010;

    public static final int EXIT_RERUN = 0x00000020;

    public static final int EXIT_NO_MAPPING = 0x00000040;

    public static final int EXIT_REMOTE_PERMISSION = 0x00000080;

    public static final int EXIT_INIT_ENVIRON = 0x00000100;

    public static final int EXIT_PRE_EXEC = 0x00000200;

    public static final int EXIT_REQUEUE = 0x00000400;

    public static final int EXIT_REMOVE = 0x00000800;

    public static final int EXIT_VALUE_REQUEUE = 0x00001000;

    public static final int EXIT_CANCEL = 0x00002000;

    public static final int EXIT_MED_KILLED = 0x00004000;

    public static final int EXIT_REMOTE_LEASE_JOB = 0x00008000;

    public static final int EXIT_CWD_NOTEXIST = 0x00010000;


    public static final int LSB_MODE_BATCH = 0x1;
    public static final int LSB_MODE_JS = 0x2;
    public static final int LSB_MODE_BATCH_RD = 0x4;

    public static final int RLIMIT_CPU = 0;
    public static final int RLIMIT_FSIZE = 1;
    public static final int RLIMIT_DATA = 2;
    public static final int RLIMIT_STACK = 3;
    public static final int RLIMIT_CORE = 4;
    public static final int RLIMIT_RSS = 5;
    public static final int RLIM_INFINITY = 0x7fffffff;


    public static final int LSBE_NO_ERROR = 0;

    public static final int LSBE_NO_JOB = 1;

    public static final int LSBE_NOT_STARTED = 2;

    public static final int LSBE_JOB_STARTED = 3;

    public static final int LSBE_JOB_FINISH = 4;

    public static final int LSBE_STOP_JOB = 5;

    public static final int LSBE_DEPEND_SYNTAX = 6;

    public static final int LSBE_EXCLUSIVE = 7;

    public static final int LSBE_ROOT = 8;

    public static final int LSBE_MIGRATION = 9;

    public static final int LSBE_J_UNCHKPNTABLE = 10;

    public static final int LSBE_NO_OUTPUT = 11;

    public static final int LSBE_NO_JOBID = 12;

    public static final int LSBE_ONLY_INTERACTIVE = 13;

    public static final int LSBE_NO_INTERACTIVE = 14;


    public static final int LSBE_NO_USER = 15;

    public static final int LSBE_BAD_USER = 16;

    public static final int LSBE_PERMISSION = 17;

    public static final int LSBE_BAD_QUEUE = 18;

    public static final int LSBE_QUEUE_NAME = 19;

    public static final int LSBE_QUEUE_CLOSED = 20;

    public static final int LSBE_QUEUE_WINDOW = 21;

    public static final int LSBE_QUEUE_USE = 22;

    public static final int LSBE_BAD_HOST = 23;

    public static final int LSBE_PROC_NUM = 24;

    public static final int LSBE_NO_HPART = 25;

    public static final int LSBE_BAD_HPART = 26;

    public static final int LSBE_NO_GROUP = 27;

    public static final int LSBE_BAD_GROUP = 28;

    public static final int LSBE_QUEUE_HOST = 29;

    public static final int LSBE_UJOB_LIMIT = 30;

    public static final int LSBE_NO_HOST = 31;


    public static final int LSBE_BAD_CHKLOG = 32;

    public static final int LSBE_PJOB_LIMIT = 33;

    public static final int LSBE_NOLSF_HOST = 34;


    public static final int LSBE_BAD_ARG = 35;

    public static final int LSBE_BAD_TIME = 36;

    public static final int LSBE_START_TIME = 37;

    public static final int LSBE_BAD_LIMIT = 38;

    public static final int LSBE_OVER_LIMIT = 39;

    public static final int LSBE_BAD_CMD = 40;

    public static final int LSBE_BAD_SIGNAL = 41;

    public static final int LSBE_BAD_JOB = 42;

    public static final int LSBE_QJOB_LIMIT = 43;

    public static final int LSBE_BAD_TERM = 44;


    public static final int LSBE_UNKNOWN_EVENT = 45;

    public static final int LSBE_EVENT_FORMAT = 46;

    public static final int LSBE_EOF = 47;


    public static final int LSBE_MBATCHD = 50;

    public static final int LSBE_SBATCHD = 51;

    public static final int LSBE_LSBLIB = 52;

    public static final int LSBE_LSLIB = 53;

    public static final int LSBE_SYS_CALL = 54;

    public static final int LSBE_NO_MEM = 55;

    public static final int LSBE_SERVICE = 56;

    public static final int LSBE_NO_ENV = 57;

    public static final int LSBE_CHKPNT_CALL = 58;

    public static final int LSBE_NO_FORK = 59;


    public static final int LSBE_PROTOCOL = 60;

    public static final int LSBE_XDR = 61;

    public static final int LSBE_PORT = 62;

    public static final int LSBE_TIME_OUT = 63;

    public static final int LSBE_CONN_TIMEOUT = 64;

    public static final int LSBE_CONN_REFUSED = 65;

    public static final int LSBE_CONN_EXIST = 66;

    public static final int LSBE_CONN_NONEXIST = 67;

    public static final int LSBE_SBD_UNREACH = 68;

    // Search for any ; \s+ /** and fix the comments
    public static final int LSBE_OP_RETRY = 69;

    public static final int LSBE_USER_JLIMIT = 70;


    public static final int LSBE_NQS_BAD_PAR = 72;


    public static final int LSBE_NO_LICENSE = 73;


    public static final int LSBE_BAD_CALENDAR = 74;

    public static final int LSBE_NOMATCH_CALENDAR = 75;

    public static final int LSBE_NO_CALENDAR = 76;

    public static final int LSBE_BAD_TIMEEVENT = 77;

    public static final int LSBE_CAL_EXIST = 78;

    public static final int LSBE_CAL_DISABLED = 79;


    public static final int LSBE_JOB_MODIFY = 80;
    public static final int LSBE_JOB_MODIFY_ONCE = 81;


    public static final int LSBE_J_UNREPETITIVE = 82;

    public static final int LSBE_BAD_CLUSTER = 83;


    public static final int LSBE_PEND_CAL_JOB = 84;
    public static final int LSBE_RUN_CAL_JOB = 85;


    public static final int LSBE_JOB_MODIFY_USED = 86;

    public static final int LSBE_AFS_TOKENS = 87;


    public static final int LSBE_BAD_EVENT = 88;

    public static final int LSBE_NOMATCH_EVENT = 89;

    public static final int LSBE_NO_EVENT = 90;


    public static final int LSBE_HJOB_LIMIT = 91;


    public static final int LSBE_MSG_DELIVERED = 92;
    public static final int LSBE_NO_JOBMSG = 93;

    public static final int LSBE_MSG_RETRY = 94;


    public static final int LSBE_BAD_RESREQ = 95;


    public static final int LSBE_NO_ENOUGH_HOST = 96;


    public static final int LSBE_CONF_FATAL = 97;

    public static final int LSBE_CONF_WARNING = 98;


    public static final int LSBE_CAL_MODIFY = 99;

    public static final int LSBE_JOB_CAL_MODIFY = 100;
    public static final int LSBE_HP_FAIRSHARE_DEF = 101;

    public static final int LSBE_NO_RESOURCE = 102;

    public static final int LSBE_BAD_RESOURCE = 103;
    public static final int LSBE_INTERACTIVE_CAL = 104;
    public static final int LSBE_INTERACTIVE_RERUN = 105;

    public static final int LSBE_PTY_INFILE = 106;

    public static final int LSBE_JS_DISABLED = 107;

    public static final int LSBE_BAD_SUBMISSION_HOST = 108;
    public static final int LSBE_LOCK_JOB = 109;

    public static final int LSBE_UGROUP_MEMBER = 110;
    public static final int LSBE_UNSUPPORTED_MC = 111;
    public static final int LSBE_PERMISSION_MC = 112;

    public static final int LSBE_SYSCAL_EXIST = 113;

    public static final int LSBE_OVER_RUSAGE = 114;

    public static final int LSBE_BAD_HOST_SPEC = 115;

    public static final int LSBE_SYNTAX_CALENDAR = 116;

    public static final int LSBE_CAL_USED = 117;

    public static final int LSBE_CAL_CYC = 118;

    public static final int LSBE_BAD_UGROUP = 119;

    public static final int LSBE_ESUB_ABORT = 120;

    public static final int LSBE_EXCEPT_SYNTAX = 121;
    public static final int LSBE_EXCEPT_COND = 122;
    public static final int LSBE_EXCEPT_ACTION = 123;

    public static final int LSBE_JOB_DEP = 124;

    public static final int LSBE_JGRP_EXIST = 125;

    public static final int LSBE_JGRP_NULL = 126;

    public static final int LSBE_JGRP_HASJOB = 127;

    public static final int LSBE_JGRP_CTRL_UNKWN = 128;

    public static final int LSBE_JGRP_BAD = 129;

    public static final int LSBE_JOB_ARRAY = 130;

    public static final int LSBE_JOB_SUSP = 131;

    public static final int LSBE_JOB_FORW = 132;

    public static final int LSBE_JGRP_HOLD = 133;

    public static final int LSBE_BAD_IDX = 134;

    public static final int LSBE_BIG_IDX = 135;

    public static final int LSBE_ARRAY_NULL = 136;

    public static final int LSBE_CAL_VOID = 137;

    public static final int LSBE_JOB_EXIST = 138;

    public static final int LSBE_JOB_ELEMENT = 139;

    public static final int LSBE_BAD_JOBID = 140;

    public static final int LSBE_MOD_JOB_NAME = 141;


    public static final int LSBE_BAD_FRAME = 142;

    public static final int LSBE_FRAME_BIG_IDX = 143;

    public static final int LSBE_FRAME_BAD_IDX = 144;


    public static final int LSBE_PREMATURE = 145;


    public static final int LSBE_BAD_PROJECT_GROUP = 146;


    public static final int LSBE_NO_HOST_GROUP = 147;

    public static final int LSBE_NO_USER_GROUP = 148;

    public static final int LSBE_INDEX_FORMAT = 149;


    public static final int LSBE_SP_SRC_NOT_SEEN = 150;

    public static final int LSBE_SP_FAILED_HOSTS_LIM = 151;

    public static final int LSBE_SP_COPY_FAILED = 152;

    public static final int LSBE_SP_FORK_FAILED = 153;

    public static final int LSBE_SP_CHILD_DIES = 154;

    public static final int LSBE_SP_CHILD_FAILED = 155;

    public static final int LSBE_SP_FIND_HOST_FAILED = 156;

    public static final int LSBE_SP_SPOOLDIR_FAILED = 157;

    public static final int LSBE_SP_DELETE_FAILED = 158;


    public static final int LSBE_BAD_USER_PRIORITY = 159;

    public static final int LSBE_NO_JOB_PRIORITY = 160;

    public static final int LSBE_JOB_REQUEUED = 161;

    public static final int LSBE_JOB_REQUEUE_REMOTE = 162;

    public static final int LSBE_NQS_NO_ARRJOB = 163;


    public static final int LSBE_BAD_EXT_MSGID = 164;

    public static final int LSBE_NO_IFREG = 165;

    public static final int LSBE_BAD_ATTA_DIR = 166;

    public static final int LSBE_COPY_DATA = 167;

    public static final int LSBE_JOB_ATTA_LIMIT = 168;

    public static final int LSBE_CHUNK_JOB = 169;



    public static final int LSBE_DLOGD_ISCONN = 170;


    public static final int LSBE_MULTI_FIRST_HOST = 171;

    public static final int LSBE_HG_FIRST_HOST = 172;

    public static final int LSBE_HP_FIRST_HOST = 173;

    public static final int LSBE_OTHERS_FIRST_HOST = 174;


    public static final int LSBE_MC_HOST = 175;

    public static final int LSBE_MC_REPETITIVE = 176;

    public static final int LSBE_MC_CHKPNT = 177;

    public static final int LSBE_MC_EXCEPTION = 178;

    public static final int LSBE_MC_TIMEEVENT = 179;

    public static final int LSBE_PROC_LESS = 180;
    public static final int LSBE_MOD_MIX_OPTS = 181;

    public static final int LSBE_MOD_REMOTE = 182;
    public static final int LSBE_MOD_CPULIMIT = 183;
    public static final int LSBE_MOD_MEMLIMIT = 184;

    public static final int LSBE_MOD_ERRFILE = 185;

    public static final int LSBE_LOCKED_MASTER = 186;
    public static final int LSBE_WARNING_INVALID_TIME_PERIOD = 187;
    public static final int LSBE_WARNING_MISSING = 188;
    public static final int LSBE_DEP_ARRAY_SIZE = 189;

    public static final int LSBE_FEWER_PROCS = 190;

    public static final int LSBE_BAD_RSVID = 191;

    public static final int LSBE_NO_RSVID = 192;

    public static final int LSBE_NO_EXPORT_HOST = 193;

    public static final int LSBE_REMOTE_HOST_CONTROL = 194;

    public static final int LSBE_REMOTE_CLOSED = 195;

    public static final int LSBE_USER_SUSPENDED = 196;

    public static final int LSBE_ADMIN_SUSPENDED = 197;

    public static final int LSBE_NOT_LOCAL_HOST = 198;

    public static final int LSBE_LEASE_INACTIVE = 199;

    public static final int LSBE_QUEUE_ADRSV = 200;

    public static final int LSBE_HOST_NOT_EXPORTED = 201;

    public static final int LSBE_HOST_ADRSV = 202;

    public static final int LSBE_MC_CONN_NONEXIST = 203;

    public static final int LSBE_RL_BREAK = 204;


    public static final int LSBE_LSF2TP_PREEMPT = 205;

    public static final int LSBE_LSF2TP_RESERVE = 206;
    public static final int LSBE_LSF2TP_BACKFILL = 207;

    public static final int LSBE_RSV_POLICY_NAME_BAD = 208;

    public static final int LSBE_RSV_POLICY_PERMISSION_DENIED = 209;

    public static final int LSBE_RSV_POLICY_USER = 210;

    public static final int LSBE_RSV_POLICY_HOST = 211;

    public static final int LSBE_RSV_POLICY_TIMEWINDOW = 212;

    public static final int LSBE_RSV_POLICY_DISABLED = 213;

    public static final int LSBE_LIM_NO_GENERAL_LIMIT = 214;

    public static final int LSBE_LIM_NO_RSRC_USAGE = 215;

    public static final int LSBE_LIM_CONVERT_ERROR = 216;

    public static final int LSBE_RSV_NO_HOST = 217;

    public static final int LSBE_MOD_JGRP_ARRAY = 218;

    public static final int LSBE_MOD_MIX = 219;

    public static final int LSBE_SLA_NULL = 220;

    public static final int LSBE_MOD_JGRP_SLA = 221;

    public static final int LSBE_SLA_MEMBER = 222;

    public static final int LSBE_NO_EXCEPTIONAL_HOST = 223;

    public static final int LSBE_WARNING_INVALID_ACTION = 224;


    public static final int LSBE_EXTSCHED_SYNTAX = 225;

    public static final int LSBE_SLA_RMT_ONLY_QUEUE = 226;

    public static final int LSBE_MOD_SLA_ARRAY = 227;

    public static final int LSBE_MOD_SLA_JGRP = 228;

    public static final int LSBE_MAX_PEND = 229;

    public static final int LSBE_CONCURRENT = 230;

    public static final int LSBE_FEATURE_NULL = 231;


    public static final int LSBE_DYNGRP_MEMBER = 232;

    public static final int LSBE_BAD_DYN_HOST = 233;

    public static final int LSBE_NO_GRP_MEMBER = 234;

    public static final int LSBE_JOB_INFO_FILE = 235;

    public static final int LSBE_MOD_OR_RUSAGE = 236;

    public static final int LSBE_BAD_GROUP_NAME = 237;

    public static final int LSBE_BAD_HOST_NAME = 238;

    public static final int LSBE_DT_BSUB = 239;


    public static final int LSBE_PARENT_SYM_JOB = 240;

    public static final int LSBE_PARTITION_NO_CPU = 241;

    public static final int LSBE_PARTITION_BATCH = 242;

    public static final int LSBE_PARTITION_ONLINE = 243;

    public static final int LSBE_NOLICENSE_BATCH = 244;

    public static final int LSBE_NOLICENSE_ONLINE = 245;

    public static final int LSBE_SIGNAL_SRVJOB = 246;

    public static final int LSBE_BEGIN_TIME_INVALID = 247;

    public static final int LSBE_END_TIME_INVALID = 248;

    public static final int LSBE_BAD_REG_EXPR = 249;


    public static final int LSBE_GRP_REG_EXPR = 250;

    public static final int LSBE_GRP_HAVE_NO_MEMB = 251;

    public static final int LSBE_APP_NULL = 252;

    public static final int LSBE_PROC_JOB_APP = 253;

    public static final int LSBE_PROC_APP_QUE = 254;

    public static final int LSBE_BAD_APPNAME = 255;

    public static final int LSBE_APP_OVER_LIMIT = 256;

    public static final int LSBE_REMOVE_DEF_APP = 257;

    public static final int LSBE_EGO_DISABLED = 258;

    public static final int LSBE_REMOTE_HOST = 259;

    public static final int LSBE_SLA_EXCLUSIVE = 260;

    public static final int LSBE_SLA_NONEXCLUSIVE = 261;

    public static final int LSBE_PERFMON_STARTED = 262;

    public static final int LSBE_PERFMON_STOPED = 263;

    public static final int LSBE_PERFMON_PERIOD_SET = 264;

    public static final int LSBE_DEFAULT_SPOOL_DIR_DISABLED = 265;

    public static final int LSBE_APS_QUEUE_JOB = 266;

    public static final int LSBE_BAD_APS_JOB = 267;

    public static final int LSBE_BAD_APS_VAL = 268;

    public static final int LSBE_APS_STRING_UNDEF = 269;

    public static final int LSBE_SLA_JOB_APS_QUEUE = 270;

    public static final int LSBE_MOD_MIX_APS = 271;

    public static final int LSBE_APS_RANGE = 272;

    public static final int LSBE_APS_ZERO = 273;


    public static final int LSBE_DJOB_RES_PORT_UNKNOWN = 274;

    public static final int LSBE_DJOB_RES_TIMEOUT = 275;

    public static final int LSBE_DJOB_RES_IOERR = 276;

    public static final int LSBE_DJOB_RES_INTERNAL_FAILURE = 277;


    public static final int LSBE_DJOB_CAN_NOT_RUN = 278;

    public static final int LSBE_DJOB_VALIDATION_BAD_JOBID = 279;

    public static final int LSBE_DJOB_VALIDATION_BAD_HOST = 280;

    public static final int LSBE_DJOB_VALIDATION_BAD_USER = 281;

    public static final int LSBE_DJOB_EXECUTE_TASK = 282;

    public static final int LSBE_DJOB_WAIT_TASK = 283;


    public static final int LSBE_APS_HPC = 284;

    public static final int LSBE_DIGEST_CHECK_BSUB = 285;

    public static final int LSBE_DJOB_DISABLED = 286;


    public static final int LSBE_BAD_RUNTIME = 287;

    public static final int LSBE_BAD_RUNLIMIT = 288;

    public static final int LSBE_OVER_QUEUE_LIMIT = 289;

    public static final int LSBE_SET_BY_RATIO = 290;

    public static final int LSBE_BAD_CWD = 291;


    public static final int LSBE_JGRP_LIMIT_GRTR_THAN_PARENT = 292;

    public static final int LSBE_JGRP_LIMIT_LESS_THAN_CHILDREN = 293;

    public static final int LSBE_NO_ARRAY_END_INDEX = 294;

    public static final int LSBE_MOD_RUNTIME = 295;

    public static final int LSBE_BAD_SUCCESS_EXIT_VALUES = 296;
    public static final int LSBE_DUP_SUCCESS_EXIT_VALUES = 297;
    public static final int LSBE_NO_SUCCESS_EXIT_VALUES = 298;

    public static final int LSBE_JOB_REQUEUE_BADARG = 299;
    public static final int LSBE_JOB_REQUEUE_DUPLICATED = 300;

    public static final int LSBE_JOB_REQUEUE_INVALID_DIGIT = 301;

    public static final int LSBE_JOB_REQUEUE_INVALID_TILDE = 302;
    public static final int LSBE_JOB_REQUEUE_NOVALID = 303;


    public static final int LSBE_NO_JGRP = 304;
    public static final int LSBE_NOT_CONSUMABLE = 305;


    public static final int LSBE_RSV_BAD_EXEC = 306;

    public static final int LSBE_RSV_EVENTTYPE = 307;

    public static final int LSBE_RSV_SHIFT = 308;

    public static final int LSBE_RSV_USHIFT = 309;

    public static final int LSBE_RSV_NUMEVENTS = 310;


    public static final int LSBE_ADRSV_ID_VALID = 311;

    public static final int LSBE_ADRSV_DISABLE_NONRECUR = 312;

    public static final int LSBE_ADRSV_MOD_ACTINSTANCE = 313;

    public static final int LSBE_ADRSV_HOST_NOTAVAIL = 314;

    public static final int LSBE_ADRSV_TIME_MOD_FAIL = 315;

    public static final int LSBE_ADRSV_R_AND_N = 316;

    public static final int LSBE_ADRSV_EMPTY = 317;

    public static final int LSBE_ADRSV_SWITCHTYPE = 318;

    public static final int LSBE_ADRSV_SYS_N = 319;

    public static final int LSBE_ADRSV_DISABLE = 320;

    public static final int LSBE_ADRSV_ID_UNIQUE = 321;

    public static final int LSBE_BAD_RSVNAME = 322;

    public static final int LSBE_ADVRSV_ACTIVESTART = 323;

    public static final int LSBE_ADRSV_ID_USED = 324;

    public static final int LSBE_ADRSV_PREVDISABLED = 325;

    public static final int LSBE_ADRSV_DISABLECURR = 326;

    public static final int LSBE_ADRSV_NOT_RSV_HOST = 327;


    public static final int LSBE_RESREQ_OK = 328;

    public static final int LSBE_RESREQ_ERR = 329;


    public static final int LSBE_ADRSV_HOST_USED = 330;


    public static final int LSBE_BAD_CHKPNTDIR = 331;

    public static final int LSBE_ADRSV_MOD_REMOTE = 332;
    public static final int LSBE_JOB_REQUEUE_BADEXCLUDE = 333;

    public static final int LSBE_ADRSV_DISABLE_DATE = 334;

    public static final int LSBE_ADRSV_DETACH_MIX = 335;

    public static final int LSBE_ADRSV_DETACH_ACTIVE = 336;

    public static final int LSBE_MISSING_START_END_TIME = 337;

    public static final int LSBE_JOB_RUSAGE_EXCEED_LIMIT = 338;

    public static final int LSBE_APP_RUSAGE_EXCEED_LIMIT = 339;

    public static final int LSBE_CANDIDATE_HOST_EMPTY = 340;

    public static final int LSBE_HS_BAD_AFTER_BRACKT = 341;

    public static final int LSBE_HS_NO_END_INDEX = 342;

    public static final int LSBE_HS_BAD_COMMA = 343;

    public static final int LSBE_HS_BAD_FORMAT = 344;

    public static final int LSBE_HS_BAD_ORDER = 345;

    public static final int LSBE_HS_BAD_MANY_DIGITS = 346;

    public static final int LSBE_HS_BAD_NUM_DIGITS = 347;

    public static final int LSBE_HS_BAD_END_INDEX = 348;

    public static final int LSBE_HS_BAD_INDEX = 349;


    public static final int LSBE_COMMENTS = 350;


    public static final int LSBE_FIRST_HOSTS_NOT_IN_QUEUE = 351;


    public static final int LSBE_JOB_NOTSTART = 352;

    public static final int LSBE_RUNTIME_INVAL = 353;

    public static final int LSBE_SSH_NOT_INTERACTIVE = 354;

    public static final int LSBE_LESS_RUNTIME = 355;

    public static final int LSBE_RESIZE_NOTIFY_CMD_LEN = 356;

    public static final int LSBE_JOB_RESIZABLE = 357;

    public static final int LSBE_RESIZE_RELEASE_HOSTSPEC = 358;

    public static final int LSBE_NO_RESIZE_NOTIFY = 359;

    public static final int LSBE_RESIZE_RELEASE_FRISTHOST = 360;

    public static final int LSBE_RESIZE_EVENT_INPROGRESS = 361;

    public static final int LSBE_RESIZE_BAD_SLOTS = 362;

    public static final int LSBE_RESIZE_NO_ACTIVE_REQUEST = 363;

    public static final int LSBE_HOST_NOT_IN_ALLOC = 364;

    public static final int LSBE_RESIZE_RELEASE_NOOP = 365;

    public static final int LSBE_RESIZE_URGENT_JOB = 366;
    public static final int LSBE_RESIZE_EGO_SLA_COEXIST = 367;

    public static final int LSBE_HOST_NOT_SUPPORT_RESIZE = 368;

    public static final int LSBE_APP_RESIZABLE = 369;

    public static final int LSBE_RESIZE_LOST_AND_FOUND = 370;

    public static final int LSBE_RESIZE_FIRSTHOST_LOST_AND_FOUND = 371;

    public static final int LSBE_RESIZE_BAD_HOST = 372;

    public static final int LSBE_AUTORESIZE_APP = 373;

    public static final int LSBE_RESIZE_PENDING_REQUEST = 374;

    public static final int LSBE_ASKED_HOSTS_NUMBER = 375;

    public static final int LSBE_AR_HOST_EMPTY = 376;

    public static final int LSBE_AR_FIRST_HOST_EMPTY = 377;

    public static final int LSBE_JB = 378;

    public static final int LSBE_JB_DBLIB = 379;

    public static final int LSBE_JB_DB_UNREACH = 380;

    public static final int LSBE_JB_MBD_UNREACH = 381;

    public static final int LSBE_JB_BES = 382;

    public static final int LSBE_JB_BES_UNSUPPORTED_OP = 383;

    public static final int LSBE_LS_PROJECT_NAME = 384;

    public static final int LSBE_END_TIME_INVALID_COMPARE_START = 385;

    public static final int LSBE_HP_REDUNDANT_HOST = 386;

    public static final int LSBE_COMPOUND_APP_SLOTS = 387;

    public static final int LSBE_COMPOUND_QUEUE_SLOTS = 388;

    public static final int LSBE_COMPOUND_RESIZE = 389;

    public static final int LSBE_CU_OVERLAPPING_HOST = 390;

    public static final int LSBE_CU_BAD_HOST = 391;

    public static final int LSBE_CU_HOST_NOT_ALLOWED = 392;

    public static final int LSBE_CU_NOT_LOWEST_LEVEL = 393;

    public static final int LSBE_CU_MOD_RESREQ = 394;

    public static final int LSBE_CU_AUTORESIZE = 395;

    public static final int LSBE_NO_COMPUTE_UNIT_TYPES = 396;

    public static final int LSBE_NO_COMPUTE_UNIT = 397;

    public static final int LSBE_BAD_COMPUTE_UNIT = 398;

    public static final int LSBE_CU_EXCLUSIVE = 399;

    public static final int LSBE_CU_EXCLUSIVE_LEVEL = 400;

    public static final int LSBE_CU_SWITCH = 401;

    public static final int LSBE_COMPOUND_JOB_SLOTS = 402;

    public static final int LSBE_COMPOUND_QUEUE_RUSAGE_OR = 403;

    public static final int LSBE_CU_BALANCE_USABLECUSLOTS = 404;

    public static final int LSBE_COMPOUND_TSJOB_APP = 405;

    public static final int LSBE_COMPOUND_TSJOB_QUEUE = 406;
    public static final int LSBE_EXCEED_MAX_JOB_NAME_DEP = 407;

    public static final int LSBE_WAIT_FOR_MC_SYNC = 408;

    public static final int LSBE_RUSAGE_EXCEED_RESRSV_LIMIT = 409;

    public static final int LSBE_JOB_DESCRIPTION_LEN = 410;

    public static final int LSBE_NOT_IN_SIMMODE = 411;

    public static final int LSBE_SIM_OPT_RUNTIME = 412;

    public static final int LSBE_SIM_OPT_CPUTIME = 413;

    public static final int LSBE_SIM_OPT_MAXMEM = 414;

    public static final int LSBE_SIM_OPT_EXITSTATUS = 415;

    public static final int LSBE_SIM_OPT_SYNTAX = 416;

    public static final int LSBE_NUM_ERR = 417;


    public static final int PREPARE_FOR_OP = 1024;
    public static final int READY_FOR_OP = 1023;




    public static final int SUB_JOB_NAME = 0x01;
    public static final int SUB_QUEUE = 0x02;
    public static final int SUB_HOST = 0x04;
    public static final int SUB_IN_FILE = 0x08;
    public static final int SUB_OUT_FILE = 0x10;
    public static final int SUB_ERR_FILE = 0x20;
    public static final int SUB_EXCLUSIVE = 0x40;
    public static final int SUB_NOTIFY_END = 0x80;
    public static final int SUB_NOTIFY_BEGIN = 0x100;
    public static final int SUB_USER_GROUP = 0x200;
    public static final int SUB_CHKPNT_PERIOD = 0x400;
    public static final int SUB_CHKPNT_DIR = 0x800;
    public static final int SUB_CHKPNTABLE = SUB_CHKPNT_DIR;
    public static final int SUB_RESTART_FORCE = 0x1000;
    public static final int SUB_RESTART = 0x2000;
    public static final int SUB_RERUNNABLE = 0x4000;
    public static final int SUB_WINDOW_SIG = 0x8000;
    public static final int SUB_HOST_SPEC = 0x10000;
    public static final int SUB_DEPEND_COND = 0x20000;
    public static final int SUB_RES_REQ = 0x40000;
    public static final int SUB_OTHER_FILES = 0x80000;
    public static final int SUB_PRE_EXEC = 0x100000;
    public static final int SUB_LOGIN_SHELL = 0x200000;
    public static final int SUB_MAIL_USER = 0x400000;
    public static final int SUB_MODIFY = 0x800000;
    public static final int SUB_MODIFY_ONCE = 0x1000000;
    public static final int SUB_PROJECT_NAME = 0x2000000;
    public static final int SUB_INTERACTIVE = 0x4000000;
    public static final int SUB_PTY = 0x8000000;
    public static final int SUB_PTY_SHELL = 0x10000000;

    public static final int SUB_EXCEPT = 0x20000000;

    public static final int SUB_TIME_EVENT = 0x40000000;


    public static final int SUB2_HOLD = 0x01;

    public static final int SUB2_MODIFY_CMD = 0x02;

    //public static final int SUB2_BSUB_BLOCK = 0x04;

    public static final int SUB2_HOST_NT = 0x08;

    public static final int SUB2_HOST_UX = 0x10;

    public static final int SUB2_QUEUE_CHKPNT = 0x20;

    public static final int SUB2_QUEUE_RERUNNABLE = 0x40;

    public static final int SUB2_IN_FILE_SPOOL = 0x80;

    public static final int SUB2_JOB_CMD_SPOOL = 0x100;

    public static final int SUB2_JOB_PRIORITY = 0x200;

    public static final int SUB2_USE_DEF_PROCLIMIT = 0x400;

    public static final int SUB2_MODIFY_RUN_JOB = 0x800;

    public static final int SUB2_MODIFY_PEND_JOB = 0x1000;

    public static final int SUB2_WARNING_TIME_PERIOD = 0x2000;

    public static final int SUB2_WARNING_ACTION = 0x4000;

    public static final int SUB2_USE_RSV = 0x8000;

    public static final int SUB2_TSJOB = 0x10000;


    public static final int SUB2_LSF2TP = 0x20000;

    public static final int SUB2_JOB_GROUP = 0x40000;

    public static final int SUB2_SLA = 0x80000;

    public static final int SUB2_EXTSCHED = 0x100000;

    public static final int SUB2_LICENSE_PROJECT = 0x200000;

    public static final int SUB2_OVERWRITE_OUT_FILE = 0x400000;

    public static final int SUB2_OVERWRITE_ERR_FILE = 0x800000;


    public static final int SUB2_SSM_JOB = 0x1000000;

    public static final int SUB2_SYM_JOB = 0x2000000;

    public static final int SUB2_SRV_JOB = 0x4000000;

    public static final int SUB2_SYM_GRP = 0x8000000;

    public static final int SUB2_SYM_JOB_PARENT = 0x10000000;

    public static final int SUB2_SYM_JOB_REALTIME = 0x20000000;

    public static final int SUB2_SYM_JOB_PERSIST_SRV = 0x40000000;

    public static final int SUB2_SSM_JOB_PERSIST = 0x80000000;


    public static final int SUB3_APP = 0x01;

    public static final int SUB3_APP_RERUNNABLE = 0x02;

    public static final int SUB3_ABSOLUTE_PRIORITY = 0x04;

    public static final int SUB3_DEFAULT_JOBGROUP = 0x08;

    public static final int SUB3_POST_EXEC = 0x10;
    public static final int SUB3_USER_SHELL_LIMITS = 0x20;
    public static final int SUB3_CWD = 0x40;
    public static final int SUB3_RUNTIME_ESTIMATION = 0x80;

    public static final int SUB3_NOT_RERUNNABLE = 0x100;

    public static final int SUB3_JOB_REQUEUE = 0x200;
    public static final int SUB3_INIT_CHKPNT_PERIOD = 0x400;
    public static final int SUB3_MIG_THRESHOLD = 0x800;

    public static final int SUB3_APP_CHKPNT_DIR = 0x1000;
    public static final int SUB3_BSUB_CHK_RESREQ = 0x2000;
    public static final int SUB3_RUNTIME_ESTIMATION_ACC = 0x4000;
    public static final int SUB3_RUNTIME_ESTIMATION_PERC = 0x8000;

    public static final int SUB3_INTERACTIVE_SSH = 0x10000;
    public static final int SUB3_XJOB_SSH = 0x20000;

    public static final int SUB3_AUTO_RESIZE = 0x40000;

    public static final int SUB3_RESIZE_NOTIFY_CMD = 0x80000;


    public static final int SUB3_BULK_SUBMIT = 0x100000;

    public static final int SUB3_INTERACTIVE_TTY = 0x200000;

    public static final int SUB3_FLOATING_CLIENT = 0x400000;

    public static final int SUB3_XFJOB = 0x800000;

    public static final int SUB3_XFJOB_EXCLUSIVE = 0x1000000;

    public static final int SUB3_JOB_DESCRIPTION = 0x2000000;

    public static final int SUB3_SIMULATION = 0x4000000;


    public static boolean IS_SSM_JOB(int option) {
        return JNAUtils.toBoolean((option) & SUB2_SSM_JOB);
    }

    public static boolean IS_SSM_JOB_PERSIST(int option) {
        return JNAUtils.toBoolean((option) & SUB2_SSM_JOB_PERSIST);
    }

    public static boolean IS_SYM_JOB(int option) {
        return JNAUtils.toBoolean((option) & SUB2_SYM_JOB);
    }

    public static boolean IS_SYM_JOB_PARENT(int option) {
        return JNAUtils.toBoolean((option) & SUB2_SYM_JOB_PARENT);
    }

    public static boolean IS_SYM_JOB_REALTIME(int option) {
        return JNAUtils.toBoolean((option) & SUB2_SYM_JOB_REALTIME);
    }

    public static boolean IS_SYM_JOB_PERSIST_SRV(int option) {
        return JNAUtils.toBoolean((option) & SUB2_SYM_JOB_PERSIST_SRV);
    }

    public static boolean IS_SRV_JOB(int option) {
        return JNAUtils.toBoolean((option) & SUB2_SRV_JOB);
    }

    public static boolean IS_SYM_GRP(int option) {
        return JNAUtils.toBoolean((option) & SUB2_SYM_GRP);
    }

    public static boolean IS_SYM_JOB_OR_SYM_GRP (int option)  { return (IS_SYM_JOB(option) || IS_SYM_GRP(option)); }
    public static boolean IS_REAL_SYM_JOB (int option)  { return (IS_SYM_JOB(option) && !IS_SYM_JOB_PERSIST_SRV(option)); }

    public static boolean IS_WLM_JOB (int option)  { return (IS_SSM_JOB(option) || IS_SYM_JOB(option) || IS_SRV_JOB(option) || IS_SYM_GRP(option)); }
    public static boolean IS_BATCH_JOB (int option)  { return (!IS_WLM_JOB(option)); }
    public static boolean IS_JOB_FOR_ACCT (int option)  { return (IS_REAL_SYM_JOB(option) || IS_BATCH_JOB(option)); }

    public static boolean IS_JOB_FOR_SYM (int option)  { return (IS_SYM_JOB(option) || IS_SRV_JOB(option) || IS_SYM_GRP(option)); }

    // NOTE: Don't know what this jp struct is.
    //public static boolean IS_SYM_JOB_OR_GRP (int jp)   { return (   (jp) != null && (jp)->shared != null && (  IS_SYM_JOB((jp)->shared->jobBill.options2) ||IS_SYM_GRP((jp)->shared->jobBill.options2))); }

    public static final String LOST_AND_FOUND = "lost_and_found";

    public static final int DELETE_NUMBER = -2;
    public static final int DEL_NUMPRO = LibLsf.INFINIT_INT;
    public static final int DEFAULT_NUMPRO = LibLsf.INFINIT_INT - 1;

    public static final int CALADD = 1;

    public static final int CALMOD = 2;

    public static final int CALDEL = 3;

    public static final int CALUNDEL = 4;

    public static final int CALOCCS = 5;

    public static final int EVEADD = 1;
    public static final int EVEMOD = 2;
    public static final int EVEDEL = 3;

    public static final int PLUGIN_REQUEUE = 126;
    public static final int PLUGIN_EXIT = 125;

    public static class xFile extends Structure {
        public static class ByReference extends xFile implements Structure.ByReference {}
        public static class ByValue extends xFile implements Structure.ByValue {}
        public xFile() {}
        public xFile(Pointer p) { super(p); read(); }


        public String subFn;

        public String execFn;

        public static final int XF_OP_SUB2EXEC = 0x1;

        public static final int XF_OP_EXEC2SUB = 0x2;

        public static final int XF_OP_SUB2EXEC_APPEND = 0x4;

        public static final int XF_OP_EXEC2SUB_APPEND = 0x8;
        public static final int XF_OP_URL_SOURCE = 0x10;

        public int options;
    }



    public static final int NQS_ROUTE = 0x1;
    public static final int NQS_SIG = 0x2;
    public static final int NQS_SERVER = 0x4;


    public static final int MAXNFA = 1024;
    public static final int MAXTAG = 10;

    public static final int OKP = 1;
    public static final int NOP = 0;

    public static final int CHR = 1;
    public static final int ANY = 2;
    public static final int CCL = 3;
    public static final int BOL = 4;
    public static final int EOL = 5;
    public static final int BOT = 6;
    public static final int EOT = 7;
    public static final int BOW = 8;
    public static final int EOW = 9;
    public static final int REF = 10;
    public static final int CLO = 11;

    public static final int END = 0;


    public static final int MAXCHR = 128;
    public static final int CHRBIT = 8;
    public static final int BITBLK = MAXCHR / CHRBIT;
    public static final int BLKIND = 0xAA;
    public static final int BITIND = 0x7;

    public static final int ASCIIB = 0x7F;


    public static final byte[] chrtyp = {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
            0, 0, 0, 0, 0, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 0, 0, 0, 0, 1, 0, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 0, 0, 0, 0, 0
    };

    public static int inascii(int x) {
        return (0x7F & (x));
    }

    public static int iswordc(int x) {
        return chrtyp[inascii(x)];
    }



    public static final int ANYSKIP = 2;

    public static final int CHRSKIP = 3;

    public static final int CCLSKIP = 18;


    public static final int JDATA_EXT_TEST = 1001;

    public static final int JDATA_EXT_SIMREQ = 1002;


    public static class submit_ext extends Structure {
        public static class ByReference extends submit_ext implements Structure.ByReference {}
        public static class ByValue extends submit_ext implements Structure.ByValue {}
        public submit_ext() {}
        public submit_ext(Pointer p) { super(p); read(); }


        public int num;

        public Pointer keys;

        public Pointer values;
    }




    public static class submit extends Structure {
        public static class ByReference extends submit implements Structure.ByReference {}
        public static class ByValue extends submit implements Structure.ByValue {}
        public submit() {}
        public submit(Pointer p) { super(p); read(); }


        public int options;


        public int options2;


        public String jobName;

        public String queue;

        public int numAskedHosts;

        public Pointer askedHosts;

        public String resReq;

        public int[] rLimits = new int[LibLsf.LSF_RLIM_NLIMITS];

        public String hostSpec;

        public int numProcessors;

        public String dependCond;

        public String timeEvent;

        public NativeLong beginTime;

        public NativeLong termTime;

        public int sigValue;

        public String inFile;

        public String outFile;

        public String errFile;

        public String command;

        public String newCommand;

        public NativeLong chkpntPeriod;

        public String chkpntDir;

        public int nxf;

        public Pointer /* xFile.ByReference */ xf;

        public String preExecCmd;

        public String mailUser;

        public int delOptions;

        public int delOptions2;

        public String projectName;

        public int maxNumProcessors;

        public String loginShell;

        public String userGroup;

        public String exceptList;


        public int userPriority;

        public String rsvId;

        public String jobGroup;

        public String sla;

        public String extsched;

        public int warningTimePeriod;

        public String warningAction;

        public String licenseProject;

        public int options3;

        public int delOptions3;

        public String app;

        public int jsdlFlag;

        public String jsdlDoc;

        public Pointer correlator;

        public String apsString;

        public String postExecCmd;

        public String cwd;

        public int runtimeEstimation;

        public String requeueEValues;

        public int initChkpntPeriod;

        public int migThreshold;

        public String notifyCmd;

        public String jobDescription;


        public submit_ext.ByReference submitExt;
    }




    public static class submitReply extends Structure {
        public static class ByReference extends submitReply implements Structure.ByReference {}
        public static class ByValue extends submitReply implements Structure.ByValue {}
        public submitReply() {}
        public submitReply(Pointer p) { super(p); read(); }


        public String queue;

        public long badJobId;

        public String badJobName;

        public int badReqIndx;
    }



    public static class submig extends Structure {
        public static class ByReference extends submig implements Structure.ByReference {}
        public static class ByValue extends submig implements Structure.ByValue {}
        public submig() {}
        public submig(Pointer p) { super(p); read(); }


        public long jobId;

        public int options;

        public int numAskedHosts;

        public Pointer askedHosts;
    }




    public static class jgrpAdd extends Structure {
        public static class ByReference extends jgrpAdd implements Structure.ByReference {}
        public static class ByValue extends jgrpAdd implements Structure.ByValue {}
        public jgrpAdd() {}
        public jgrpAdd(Pointer p) { super(p); read(); }

        public String groupSpec;
        public String timeEvent;
        public String depCond;
        public String sla;
        public int maxJLimit;
    }




    public static class jgrpMod extends Structure {
        public static class ByReference extends jgrpMod implements Structure.ByReference {}
        public static class ByValue extends jgrpMod implements Structure.ByValue {}
        public jgrpMod() {}
        public jgrpMod(Pointer p) { super(p); read(); }

        public String destSpec;
        public jgrpAdd jgrp;
    }




    public static class jgrpReply extends Structure {
        public static class ByReference extends jgrpReply implements Structure.ByReference {}
        public static class ByValue extends jgrpReply implements Structure.ByValue {}
        public jgrpReply() {}
        public jgrpReply(Pointer p) { super(p); read(); }

        public String badJgrpName;
        public int num;
        public Pointer delJgrpList;
    }



    public static class signalBulkJobs extends Structure {
        public static class ByReference extends signalBulkJobs implements Structure.ByReference {}
        public static class ByValue extends signalBulkJobs implements Structure.ByValue {}
        public signalBulkJobs() {}
        public signalBulkJobs(Pointer p) { super(p); read(); }


        public int signal;

        public int njobs;

        public Pointer jobs;

        public int flags;
    }




    public static class jgrpCtrl extends Structure {
        public static class ByReference extends jgrpCtrl implements Structure.ByReference {}
        public static class ByValue extends jgrpCtrl implements Structure.ByValue {}
        public jgrpCtrl() {}
        public jgrpCtrl(Pointer p) { super(p); read(); }

        public String groupSpec;
        public String userSpec;
        public int options;

        public int ctrlOp;
    }




    public static final int LSB_CHKPERIOD_NOCHNG = -1;


    public static final int LSB_CHKPNT_KILL = 0x1;

    public static final int LSB_CHKPNT_FORCE = 0x2;

    public static final int LSB_CHKPNT_COPY = 0x3;

    public static final int LSB_CHKPNT_MIG = 0x4;

    public static final int LSB_CHKPNT_STOP = 0x8;


    public static final int LSB_KILL_REQUEUE = 0x10;


    public static final String ALL_USERS = "all";

    public static final int ALL_JOB = 0x0001;

    public static final int DONE_JOB = 0x0002;

    public static final int PEND_JOB = 0x0004;

    public static final int SUSP_JOB = 0x0008;

    public static final int CUR_JOB = 0x0010;

    public static final int LAST_JOB = 0x0020;

    public static final int RUN_JOB = 0x0040;

    public static final int JOBID_ONLY = 0x0080;

    public static final int HOST_NAME = 0x0100;

    public static final int NO_PEND_REASONS = 0x0200;

    public static final int JGRP_INFO = 0x0400;

    public static final int JGRP_RECURSIVE = 0x0800;

    public static final int JGRP_ARRAY_INFO = 0x1000;

    public static final int JOBID_ONLY_ALL = 0x02000;

    public static final int ZOMBIE_JOB = 0x04000;

    public static final int TRANSPARENT_MC = 0x08000;

    public static final int EXCEPT_JOB = 0x10000;

    public static final int MUREX_JOB = 0x20000;


    public static final int TO_SYM_UA = 0x40000;

    public static final int SYM_TOP_LEVEL_ONLY = 0x80000;

    public static final int JGRP_NAME = 0x100000;

    public static final int COND_HOSTNAME = 0x200000;

    public static final int FROM_BJOBSCMD = 0x400000;

    public static final int WITH_LOPTION = 0x800000;

    public static final int APS_JOB = 0x1000000;

    public static final int UGRP_INFO = 0x2000000;

    public static final int TIME_LEFT = 0x4000000;

    public static final int FINISH_TIME = 0x8000000;

    public static final int COM_PERCENTAGE = 0x10000000;

    public static final int SSCHED_JOB = 0x20000000;

    public static final int KILL_JGRP_RECURSIVE = 0x40000000;


    public static final int JGRP_NODE_JOB = 1;

    public static final int JGRP_NODE_GROUP = 2;

    public static final int JGRP_NODE_ARRAY = 3;

    public static final int JGRP_NODE_SLA = 4;

    public static final long LSB_MAX_ARRAY_JOBID = 0x0FFFFFFFFL;
    public static final long LSB_MAX_ARRAY_IDX = 0x07FFFFFFFL;
    public static final int LSB_MAX_SEDJOB_RUNID = (0x0F);
    public static long LSB_JOBID (int array_jobId, int array_idx)    { return (((long)array_idx << 32) | array_jobId); }
    public static int LSB_ARRAY_IDX (long jobId)   { return (((jobId) == -1) ? (0) : (int)(((long)jobId >> 32)  & LSB_MAX_ARRAY_IDX)); }
    public static int LSB_ARRAY_JOBID (long jobId)  { return (((jobId) == -1) ? (-1) : (int)(jobId)); }
    //public static int LSB_ARRAY_JOBID (long jobId)  { return (((jobId) == -1) ? (-1) : (int)(jobId & LSB_MAX_ARRAY_JOBID)); }


    public static final int JGRP_INACTIVE = 0;
    public static final int JGRP_ACTIVE = 1;
    public static final int JGRP_UNDEFINED = -1;



    public static final int JGRP_RELEASE = 1;

    public static final int JGRP_HOLD = 2;

    public static final int JGRP_DEL = 3;


    public static final int JGRP_COUNT_NJOBS = 0;

    public static final int JGRP_COUNT_PEND = 1;

    public static final int JGRP_COUNT_NPSUSP = 2;

    public static final int JGRP_COUNT_NRUN = 3;

    public static final int JGRP_COUNT_NSSUSP = 4;

    public static final int JGRP_COUNT_NUSUSP = 5;

    public static final int JGRP_COUNT_NEXIT = 6;

    public static final int JGRP_COUNT_NDONE = 7;

    public static final int JGRP_COUNT_NJOBS_SLOTS = 8;

    public static final int JGRP_COUNT_PEND_SLOTS = 9;

    public static final int JGRP_COUNT_RUN_SLOTS = 10;

    public static final int JGRP_COUNT_SSUSP_SLOTS = 11;

    public static final int JGRP_COUNT_USUSP_SLOTS = 12;

    public static final int JGRP_COUNT_RESV_SLOTS = 13;

    public static final int JGRP_MOD_LIMIT = 0x1;

    public static final int NUM_JGRP_JOB_COUNTERS = 8;
    public static final int NUM_JGRP_COUNTERS = 14;

    public static final int JGRP_CREATE_EXP = 0x01;

    public static final int JGRP_CREATE_IMP = 0x02;

    public static class jgrp extends Structure {
        public static class ByReference extends jgrp implements Structure.ByReference {}
        public static class ByValue extends jgrp implements Structure.ByValue {}
        public jgrp() {}
        public jgrp(Pointer p) { super(p); read(); }

        public String name;
        public String path;
        public String user;
        public String sla;
        public int[] counters = new int[NUM_JGRP_COUNTERS];
        public int maxJLimit;
    }




    public static class jobAttrInfoEnt extends Structure {
        public static class ByReference extends jobAttrInfoEnt implements Structure.ByReference {}
        public static class ByValue extends jobAttrInfoEnt implements Structure.ByValue {}
        public jobAttrInfoEnt() {}
        public jobAttrInfoEnt(Pointer p) { super(p); read(); }


        public long jobId;

        public short port;

        public byte[] hostname = new byte[LibLsf.MAXHOSTNAMELEN];
    }



    public static class jobAttrSetLog extends Structure {
        public static class ByReference extends jobAttrSetLog implements Structure.ByReference {}
        public static class ByValue extends jobAttrSetLog implements Structure.ByValue {}
        public jobAttrSetLog() {}
        public jobAttrSetLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int idx;

        public int uid;

        public int port;

        public String hostname;
    }



    public static class jobInfoHead extends Structure {
        public static class ByReference extends jobInfoHead implements Structure.ByReference {}
        public static class ByValue extends jobInfoHead implements Structure.ByValue {}
        public jobInfoHead() {}
        public jobInfoHead(Pointer p) { super(p); read(); }


        public int numJobs;

        public NativeLongByReference jobIds;

        public int numHosts;

        public Pointer hostNames;

        public int numClusters;

        public Pointer clusterNames;

        public IntByReference numRemoteHosts;

        public PointerByReference remoteHosts;
    }



    public static class jobInfoHeadExt extends Structure {
        public static class ByReference extends jobInfoHeadExt implements Structure.ByReference {}
        public static class ByValue extends jobInfoHeadExt implements Structure.ByValue {}
        public jobInfoHeadExt() {}
        public jobInfoHeadExt(Pointer p) { super(p); read(); }


        public jobInfoHead.ByReference jobInfoHead;

        public Pointer groupInfo;
    }



    public static class reserveItem extends Structure {
        public static class ByReference extends reserveItem implements Structure.ByReference {}
        public static class ByValue extends reserveItem implements Structure.ByValue {}
        public reserveItem() {}
        public reserveItem(Pointer p) { super(p); read(); }


        public String resName;

        public int nHost;

        public FloatByReference value;

        public int shared;
    }



    public static class jobInfoEnt extends Structure {
        public static class ByReference extends jobInfoEnt implements Structure.ByReference {}
        public static class ByValue extends jobInfoEnt implements Structure.ByValue {}
        public jobInfoEnt() {}
        public jobInfoEnt(Pointer p) { super(p); read(); }


        public long jobId;

        public String user;

        public int status;

        public IntByReference reasonTb;

        public int numReasons;

        public int reasons;

        public int subreasons;

        public int jobPid;

        public NativeLong submitTime;

        public NativeLong reserveTime;

        public NativeLong startTime;

        public NativeLong predictedStartTime;

        public NativeLong endTime;

        public NativeLong lastEvent;

        public NativeLong nextEvent;

        public int duration;

        public float cpuTime;

        public int umask;

        public String cwd;

        public String subHomeDir;

        public String fromHost;

        public Pointer exHosts;

        public int numExHosts;

        public float cpuFactor;

        public int nIdx;

        public FloatByReference loadSched;

        public FloatByReference loadStop;

        public submit submit;

        public int exitStatus;

        public int execUid;

        public String execHome;

        public String execCwd;

        public String execUsername;

        public NativeLong jRusageUpdateTime;

        public LibLsf.jRusage runRusage;

        public int jType;

        public String parentGroup;

        public String jName;

        public int[] counter = new int[NUM_JGRP_COUNTERS];

        public short port;

        public int jobPriority;

        public int numExternalMsg;

        public Pointer externalMsg;

        public int clusterId;

        public String detailReason;

        public float idleFactor;

        public int exceptMask;


        public String additionalInfo;

        public int exitInfo;

        public int warningTimePeriod;

        public String warningAction;

        public String chargedSAAP;

        public String execRusage;

        public NativeLong rsvInActive;

        public int numLicense;

        public Pointer licenseNames;

        public float aps;

        public float adminAps;

        public int runTime;

        public int reserveCnt;

        public Pointer /* reserveItem.ByReference */ items;

        public float adminFactorVal;

        public int resizeMin;

        public int resizeMax;

        public NativeLong resizeReqTime;

        public int jStartNumExHosts;

        public Pointer jStartExHosts;

        public NativeLong lastResizeTime;
    }


    public static final int J_EXCEPT_OVERRUN = 0x02;
    public static final int J_EXCEPT_UNDERUN = 0x04;
    public static final int J_EXCEPT_IDLE = 0x80;
    public static final int J_EXCEPT_RUNTIME_EST_EXCEEDED = 0x100;

    public static final String OVERRUN = "overrun";
    public static final String UNDERRUN = "underrun";
    public static final String IDLE = "idle";
    public static final String SPACE = "  ";
    public static final String RUNTIME_EST_EXCEEDED = "runtime_est_exceeded";


    public static class jobInfoReq extends Structure {
        public static class ByReference extends jobInfoReq implements Structure.ByReference {}
        public static class ByValue extends jobInfoReq implements Structure.ByValue {}
        public jobInfoReq() {}
        public jobInfoReq(Pointer p) { super(p); read(); }


        public int options;

        public String userName;

        public long jobId;

        public String jobName;

        public String queue;

        public String host;

        public String app;

        public String jobDescription;

        public submit_ext.ByReference submitExt;
    }



    public static class userInfoEnt extends Structure {
        public static class ByReference extends userInfoEnt implements Structure.ByReference {}
        public static class ByValue extends userInfoEnt implements Structure.ByValue {}
        public userInfoEnt() {}
        public userInfoEnt(Pointer p) { super(p); read(); }


        public String user;

        public float procJobLimit;

        public int maxJobs;

        public int numStartJobs;

        public int numJobs;

        public int numPEND;

        public int numRUN;

        public int numSSUSP;

        public int numUSUSP;

        public int numRESERVE;

        public int maxPendJobs;
    }




    public static class userEquivalentInfoEnt extends Structure {
        public static class ByReference extends userEquivalentInfoEnt implements Structure.ByReference {}
        public static class ByValue extends userEquivalentInfoEnt implements Structure.ByValue {}
        public userEquivalentInfoEnt() {}
        public userEquivalentInfoEnt(Pointer p) { super(p); read(); }

        public String equivalentUsers;
    }




    public static class userMappingInfoEnt extends Structure {
        public static class ByReference extends userMappingInfoEnt implements Structure.ByReference {}
        public static class ByValue extends userMappingInfoEnt implements Structure.ByValue {}
        public userMappingInfoEnt() {}
        public userMappingInfoEnt(Pointer p) { super(p); read(); }


        public String localUsers;

        public String remoteUsers;

        public String direction;
    }





    public static class apsFactorMap extends Structure {
        public static class ByReference extends apsFactorMap implements Structure.ByReference {}
        public static class ByValue extends apsFactorMap implements Structure.ByValue {}
        public apsFactorMap() {}
        public apsFactorMap(Pointer p) { super(p); read(); }


        public String factorName;

        public String subFactorNames;
    }



    public static class apsLongNameMap extends Structure {
        public static class ByReference extends apsLongNameMap implements Structure.ByReference {}
        public static class ByValue extends apsLongNameMap implements Structure.ByValue {}
        public apsLongNameMap() {}
        public apsLongNameMap(Pointer p) { super(p); read(); }


        public String shortName;

        public String longName;
    }





    public static final int ALL_QUEUE = 0x01;

    public static final int DFT_QUEUE = 0x02;
    public static final int CHECK_HOST = 0x80;
    public static final int CHECK_USER = 0x100;
    public static final int SORT_HOST = 0x200;

    public static final int QUEUE_SHORT_FORMAT = 0x400;
    public static final int EXPAND_HOSTNAME = 0x800;

    public static final int RETRIEVE_BATCH = 0x1000;

    public static final int LSB_SIG_NUM_40 = 25;
    public static final int LSB_SIG_NUM_41 = 26;

    public static final int LSB_SIG_NUM_51 = 30;
    public static final int LSB_SIG_NUM_60 = 30;
    public static final int LSB_SIG_NUM = 30;

    public static final int DCP_LEND_HOSTS = 0x0001;
    public static final int DCP_BORROW_HOSTS = 0x0002;

    public static final int DCP_ALLOC_CPU_OK = 0x0;
    public static final int DCP_UNDER_ALLOC_CPU = 0x0001;
    public static final int DCP_JOB_WAIT_FOR_CPU = 0x0002;
    public static final int DCP_ALLOC_CPU_BUSY = 0x0004;


    public static class queueInfoEnt extends Structure {
        public static class ByReference extends queueInfoEnt implements Structure.ByReference {}
        public static class ByValue extends queueInfoEnt implements Structure.ByValue {}
        public queueInfoEnt() {}
        public queueInfoEnt(Pointer p) { super(p); read(); }


        public String queue;

        public String description;

        public int priority;

        public short nice;

        public String userList;

        public String hostList;

        public String hostStr;

        public int nIdx;

        public FloatByReference loadSched;

        public FloatByReference loadStop;

        public int userJobLimit;

        public float procJobLimit;

        public String windows;

        public int[] rLimits = new int[LibLsf.LSF_RLIM_NLIMITS];

        public String hostSpec;

        public int qAttrib;

        public int qStatus;

        public int maxJobs;

        public int numJobs;

        public int numPEND;

        public int numRUN;

        public int numSSUSP;

        public int numUSUSP;

        public int mig;

        public int schedDelay;

        public int acceptIntvl;

        public String windowsD;

        public String nqsQueues;

        public String userShares;

        public String defaultHostSpec;

        public int procLimit;

        public String admins;

        public String preCmd;

        public String postCmd;

        public String requeueEValues;

        public int hostJobLimit;

        public String resReq;

        public int numRESERVE;

        public int slotHoldTime;

        public String sndJobsTo;

        public String rcvJobsFrom;

        public String resumeCond;

        public String stopCond;

        public String jobStarter;

        public String suspendActCmd;

        public String resumeActCmd;

        public String terminateActCmd;

        public int[] sigMap = new int[LSB_SIG_NUM];

        public String preemption;

        public int maxRschedTime;


        public int numOfSAccts;

        public Pointer /* shareAcctInfoEnt.ByReference */ shareAccts;

        public String chkpntDir;

        public int chkpntPeriod;

        public int imptJobBklg;

        public int[] defLimits = new int[LibLsf.LSF_RLIM_NLIMITS];

        public int chunkJobSize;

        public int minProcLimit;

        public int defProcLimit;

        public String fairshareQueues;

        public String defExtSched;

        public String mandExtSched;

        public int slotShare;

        public String slotPool;

        public int underRCond;

        public int overRCond;

        public float idleCond;

        public int underRJobs;

        public int overRJobs;

        public int idleJobs;

        public int warningTimePeriod;

        public String warningAction;

        public String qCtrlMsg;

        public String acResReq;

        public int symJobLimit;

        public String cpuReq;

        public int proAttr;

        public int lendLimit;

        public int hostReallocInterval;

        public int numCPURequired;

        public int numCPUAllocated;

        public int numCPUBorrowed;

        public int numCPULent;



        public int schGranularity;

        public int symTaskGracePeriod;

        public int minOfSsm;

        public int maxOfSsm;

        public int numOfAllocSlots;

        public String servicePreemption;


        public int provisionStatus;

        public int minTimeSlice;

        public String queueGroup;

        public int numApsFactors;

        public Pointer /* apsFactorInfo.ByReference */ apsFactorInfoList;

        public Pointer /* apsFactorMap.ByReference */ apsFactorMaps;

        public Pointer /* apsLongNameMap.ByReference */ apsLongNames;

        public int maxJobPreempt;

        public int maxPreExecRetry;

        public int localMaxPreExecRetry;

        public int maxJobRequeue;

        public int usePam;

        public int cu_type_exclusive;

        public String cu_str_exclusive;

        public String resRsvLimit;

    }




    public static final int ACT_NO = 0;

    public static final int ACT_START = 1;

    public static final int ACT_PREEMPT = 2;

    public static final int ACT_DONE = 3;

    public static final int ACT_FAIL = 4;

    public static class hostInfoEnt extends Structure {
        public static class ByReference extends hostInfoEnt implements Structure.ByReference {}
        public static class ByValue extends hostInfoEnt implements Structure.ByValue {}
        public hostInfoEnt() {}
        public hostInfoEnt(Pointer p) { super(p); read(); }


        public String host;

        public int hStatus;

        public IntByReference busySched;

        public IntByReference busyStop;

        public float cpuFactor;

        public int nIdx;

        public FloatByReference load;

        public FloatByReference loadSched;

        public FloatByReference loadStop;

        public String windows;

        public int userJobLimit;

        public int maxJobs;

        public int numJobs;

        public int numRUN;

        public int numSSUSP;

        public int numUSUSP;

        public int mig;


        public int attr;

        public static final int H_ATTR_CHKPNTABLE = 0x1;

        public static final int H_ATTR_CHKPNT_COPY = 0x2;

        public FloatByReference realLoad;

        public int numRESERVE;

        public int chkSig;


        public float cnsmrUsage;

        public float prvdrUsage;

        public float cnsmrAvail;

        public float prvdrAvail;

        public float maxAvail;

        public float maxExitRate;

        public float numExitRate;

        public String hCtrlMsg;

    }



    public static class condHostInfoEnt extends Structure {
        public static class ByReference extends condHostInfoEnt implements Structure.ByReference {}
        public static class ByValue extends condHostInfoEnt implements Structure.ByValue {}
        public condHostInfoEnt() {}
        public condHostInfoEnt(Pointer p) { super(p); read(); }


        public String name;


        public int howManyOk;

        public int howManyBusy;

        public int howManyClosed;

        public int howManyFull;

        public int howManyUnreach;

        public int howManyUnavail;


        public Pointer /* hostInfoEnt.ByReference */ hostInfo;

    }



    public static class adjustParam extends Structure {
        public static class ByReference extends adjustParam implements Structure.ByReference {}
        public static class ByValue extends adjustParam implements Structure.ByValue {}
        public adjustParam() {}
        public adjustParam(Pointer p) { super(p); read(); }


        public String key;

        public float value;
    }




    public static final int FAIR_ADJUST_CPU_TIME_FACTOR = 0;

    public static final int FAIR_ADJUST_RUN_TIME_FACTOR = 1;

    public static final int FAIR_ADJUST_RUN_JOB_FACTOR = 2;

    public static final int FAIR_ADJUST_COMMITTED_RUN_TIME_FACTOR = 3;

    public static final int FAIR_ADJUST_ENABLE_HIST_RUN_TIME = 4;

    public static final int FAIR_ADJUST_HIST_CPU_TIME = 5;

    public static final int FAIR_ADJUST_NEW_USED_CPU_TIME = 6;

    public static final int FAIR_ADJUST_RUN_TIME = 7;

    public static final int FAIR_ADJUST_HIST_RUN_TIME = 8;

    public static final int FAIR_ADJUST_COMMITTED_RUN_TIME = 9;

    public static final int FAIR_ADJUST_NUM_START_JOBS = 10;

    public static final int FAIR_ADJUST_NUM_RESERVE_JOBS = 11;

    public static final int FAIR_ADJUST_MEM_USED = 12;

    public static final int FAIR_ADJUST_MEM_ALLOCATED = 13;

    public static final int FAIR_ADJUST_KVPS_SUM = 14;

    //public String[] FairAdjustPairArrayName = new String[FAIR_ADJUST_KVPS_SUM];

    public static class shareAdjustPair extends Structure {
        public static class ByReference extends shareAdjustPair implements Structure.ByReference {}
        public static class ByValue extends shareAdjustPair implements Structure.ByValue {}
        public shareAdjustPair() {}
        public shareAdjustPair(Pointer p) { super(p); read(); }


        public static int SHAREACCTTYPEQUEUE = 0x01;

        public static final int SHAREACCTTYPEHP = 0x02;

        public static final int SHAREACCTTYPESLA = 0x04;

        public int shareAcctType;

        public String holderName;

        public String providerName;

        public int numPair;

        public Pointer /* adjustParam.ByReference */ adjustParam;
    }



    // NOTE: Not in libbat
    //public static native float fairshare_adjustment(shareAdjustPair shareAdjustPair1);


    public static class hostPartUserInfo extends Structure {
        public static class ByReference extends hostPartUserInfo implements Structure.ByReference {}
        public static class ByValue extends hostPartUserInfo implements Structure.ByValue {}
        public hostPartUserInfo() {}
        public hostPartUserInfo(Pointer p) { super(p); read(); }


        public String user;

        public int shares;

        public float priority;

        public int numStartJobs;

        public float histCpuTime;

        public int numReserveJobs;

        public int runTime;

        public float shareAdjustment;
    }




    public static class hostPartInfoEnt extends Structure {
        public static class ByReference extends hostPartInfoEnt implements Structure.ByReference {}
        public static class ByValue extends hostPartInfoEnt implements Structure.ByValue {}
        public hostPartInfoEnt() {}
        public hostPartInfoEnt(Pointer p) { super(p); read(); }


        public byte[] hostPart = new byte[MAX_LSB_NAME_LEN];

        public String hostList;

        public int numUsers;

        public Pointer /* hostPartUserInfo.ByReference */ users;
    }




    public static class shareAcctInfoEnt extends Structure {
        public static class ByReference extends shareAcctInfoEnt implements Structure.ByReference {}
        public static class ByValue extends shareAcctInfoEnt implements Structure.ByValue {}
        public shareAcctInfoEnt() {}
        public shareAcctInfoEnt(Pointer p) { super(p); read(); }


        public String shareAcctPath;

        public int shares;

        public float priority;

        public int numStartJobs;

        public float histCpuTime;

        public int numReserveJobs;

        public int runTime;

        public float shareAdjustment;
    }



    public static final int DEF_MAX_JOBID = 999999;
    public static final int MAX_JOBID_LOW = 999999;
    public static final int MAX_JOBID_HIGH = (LibLsf.INFINIT_INT - 1);


    public static final int DEF_PREEMPTION_WAIT_TIME = 300;

    public static final int DEF_MAX_ASKED_HOSTS = 512;


    public static class parameterInfo extends Structure {
        public static class ByReference extends parameterInfo implements Structure.ByReference {}
        public static class ByValue extends parameterInfo implements Structure.ByValue {}
        public parameterInfo() {}
        public parameterInfo(Pointer p) { super(p); read(); }


        public String defaultQueues;

        public String defaultHostSpec;

        public int mbatchdInterval;

        public int sbatchdInterval;

        public int jobAcceptInterval;

        public int maxDispRetries;

        public int maxSbdRetries;

        public int preemptPeriod;

        public int cleanPeriod;

        public int maxNumJobs;

        public float historyHours;

        public int pgSuspendIt;

        public String defaultProject;

        public int retryIntvl;

        public int nqsQueuesFlags;

        public int nqsRequestsFlags;

        public int maxPreExecRetry;

        public int localMaxPreExecRetry;

        public int eventWatchTime;

        public float runTimeFactor;

        public float waitTimeFactor;

        public float runJobFactor;

        public int eEventCheckIntvl;

        public int rusageUpdateRate;

        public int rusageUpdatePercent;

        public int condCheckTime;

        public int maxSbdConnections;

        public int rschedInterval;

        public int maxSchedStay;

        public int freshPeriod;

        public int preemptFor;

        public int adminSuspend;

        public int userReservation;

        public float cpuTimeFactor;

        public int fyStart;

        public int maxJobArraySize;

        public NativeLong exceptReplayPeriod;

        public int jobTerminateInterval;

        public int disableUAcctMap;

        public int enforceFSProj;

        public int enforceProjCheck;

        public int jobRunTimes;

        public int dbDefaultIntval;

        public int dbHjobCountIntval;

        public int dbQjobCountIntval;

        public int dbUjobCountIntval;

        public int dbJobResUsageIntval;

        public int dbLoadIntval;

        public int dbJobInfoIntval;

        public int jobDepLastSub;

        public int maxJobNameDep;

        public String dbSelectLoad;

        public int jobSynJgrp;

        public String pjobSpoolDir;


        public int maxUserPriority;

        public int jobPriorityValue;

        public int jobPriorityTime;

        public int enableAutoAdjust;

        public int autoAdjustAtNumPend;

        public float autoAdjustAtPercent;

        public int sharedResourceUpdFactor;

        public int scheRawLoad;

        public String jobAttaDir;

        public int maxJobMsgNum;

        public int maxJobAttaSize;

        public int mbdRefreshTime;

        public int updJobRusageInterval;

        public String sysMapAcct;

        public int preExecDelay;

        public int updEventUpdateInterval;

        public int resourceReservePerSlot;

        public int maxJobId;

        public String preemptResourceList;

        public int preemptionWaitTime;

        public int maxAcctArchiveNum;

        public int acctArchiveInDays;

        public int acctArchiveInSize;

        public float committedRunTimeFactor;

        public int enableHistRunTime;


        public int mcbOlmReclaimTimeDelay;

        public int chunkJobDuration;

        public int sessionInterval;

        public int publishReasonJobNum;

        public int publishReasonInterval;

        public int publishReason4AllJobInterval;

        public int mcUpdPendingReasonInterval;

        public int mcUpdPendingReasonPkgSize;

        public int noPreemptRunTime;

        public int noPreemptFinishTime;

        public String acctArchiveAt;

        public int absoluteRunLimit;

        public int lsbExitRateDuration;

        public int lsbTriggerDuration;

        public int maxJobinfoQueryPeriod;

        public int jobSubRetryInterval;

        public int pendingJobThreshold;


        public int maxConcurrentJobQuery;

        public int minSwitchPeriod;


        public int condensePendingReasons;

        public int slotBasedParallelSched;

        public int disableUserJobMovement;

        public int detectIdleJobAfter;
        public int useSymbolPriority;
        public int JobPriorityRound;

        public String priorityMapping;

        public int maxInfoDirs;

        public int minMbdRefreshTime;

        public int enableStopAskingLicenses2LS;

        public int expiredTime;

        public String mbdQueryCPUs;

        public String defaultApp;

        public int enableStream;

        public String streamFile;

        public int streamSize;

        public int syncUpHostStatusWithLIM;

        public String defaultSLA;

        public int slaTimer;

        public int mbdEgoTtl;

        public int mbdEgoConnTimeout;

        public int mbdEgoReadTimeout;

        public int mbdUseEgoMXJ;

        public int mbdEgoReclaimByQueue;

        public int defaultSLAvelocity;

        public String exitRateTypes;

        public float globalJobExitRate;

        public int enableJobExitRatePerSlot;

        public int enableMetric;

        public int schMetricsSample;

        public float maxApsValue;

        public int newjobRefresh;

        public int preemptJobType;

        public String defaultJgrp;

        public int jobRunlimitRatio;

        public int jobIncludePostproc;

        public int jobPostprocTimeout;

        public int sschedUpdateSummaryInterval;

        public int sschedUpdateSummaryByTask;

        public int sschedRequeueLimit;

        public int sschedRetryLimit;

        public int sschedMaxTasks;

        public int sschedMaxRuntime;

        public String sschedAcctDir;

        public int jgrpAutoDel;

        public int maxJobPreempt;

        public int maxJobRequeue;

        public int noPreemptRunTimePercent;

        public int noPreemptFinishTimePercent;


        public int slotReserveQueueLimit;

        public int maxJobPercentagePerSession;

        public int useSuspSlots;


        public int maxStreamFileNum;

        public int privilegedUserForceBkill;

        public int mcSchedulingEnhance;

        public int mcUpdateInterval;

        public int intersectCandidateHosts;

        public int enforceOneUGLimit;

        public int logRuntimeESTExceeded;

        public String computeUnitTypes;

        public float fairAdjustFactor;

        public int simAbsoluteTime;

        public int extendJobException;
    }



    public static final int GROUP_MAX = 0x0001;
    public static final int GROUP_JLP = 0x0002;
    public static final int USER_JLP = 0x0004;
    public static final int HOST_JLU = 0x0008;

    public static final int MINI_JOB = 0x0010;

    public static final int LEAST_RUN_TIME = 0x0020;

    public static final int OPTIMAL_MINI_JOB = 0x0040;

    public static final int RESOURCE_ONLY = 0x0001;
    public static final int COUNT_PREEMPTABLE = 0x0002;
    public static final int HIGH_QUEUE_PRIORITY = 0x0004;
    public static final int PREEMPTABLE_QUEUE_PRIORITY = 0x0008;
    public static final int PENDING_WHEN_NOSLOTS = 0x0010;

    public static final int CAL_FORCE = 0x0001;

    public static final int PREEMPT_JOBTYPE_EXCLUSIVE = 0x0001;
    public static final int PREEMPT_JOBTYPE_BACKFILL = 0x0002;


    public static class calendarInfoEnt extends Structure {
        public static class ByReference extends calendarInfoEnt implements Structure.ByReference {}
        public static class ByValue extends calendarInfoEnt implements Structure.ByValue {}
        public calendarInfoEnt() {}
        public calendarInfoEnt(Pointer p) { super(p); read(); }


        public String name;

        public String desc;

        public String calExpr;

        public String userName;

        public int status;

        public int options;

        public int lastDay;

        public int nextDay;

        public NativeLong creatTime;

        public NativeLong lastModifyTime;

        public int flags;
    }



    public static final int ALL_CALENDARS = 0x1;

    public static final int EVE_HIST = 0x1;
    public static final int EVENT_ACTIVE = 1;
    public static final int EVENT_INACTIVE = 2;
    public static final int EVENT_REJECT = 3;

    public static final int EVENT_TYPE_UNKNOWN = 0;
    public static final int EVENT_TYPE_LATCHED = 1;
    public static final int EVENT_TYPE_PULSEALL = 2;
    public static final int EVENT_TYPE_PULSE = 3;
    public static final int EVENT_TYPE_EXCLUSIVE = 4;

    public static final int EV_UNDEF = 0;
    public static final int EV_FILE = 1;
    public static final int EV_EXCEPT = 2;
    public static final int EV_USER = 3;

    public static class loadInfoEnt extends Structure {
        public static class ByReference extends loadInfoEnt implements Structure.ByReference {}
        public static class ByValue extends loadInfoEnt implements Structure.ByValue {}
        public loadInfoEnt() {}
        public loadInfoEnt(Pointer p) { super(p); read(); }

        public String hostName;
        public int status;
        public FloatByReference load;
    }



    public static class queuePairEnt extends Structure {
        public static class ByReference extends queuePairEnt implements Structure.ByReference {}
        public static class ByValue extends queuePairEnt implements Structure.ByValue {}
        public queuePairEnt() {}
        public queuePairEnt(Pointer p) { super(p); read(); }

        public String local;
        public String remote;
        public int send;
        public int status;
    }



    public static class rmbCluAppEnt extends Structure {
        public static class ByReference extends rmbCluAppEnt implements Structure.ByReference {}
        public static class ByValue extends rmbCluAppEnt implements Structure.ByValue {}
        public rmbCluAppEnt() {}
        public rmbCluAppEnt(Pointer p) { super(p); read(); }

        public String name;
        public String description;
    }





    public static final int LEASE_CLU_STAT_DISC = 1;

    public static final int LEASE_CLU_STAT_CONN = 2;

    public static final int LEASE_CLU_STAT_OK = 3;
    public static final int LEASE_CLU_STAT_NUMBER = 3;

    public static class consumerCluEnt extends Structure {
        public static class ByReference extends consumerCluEnt implements Structure.ByReference {}
        public static class ByValue extends consumerCluEnt implements Structure.ByValue {}
        public consumerCluEnt() {}
        public consumerCluEnt(Pointer p) { super(p); read(); }


        public String cluName;

        public int status;
    }



    public static class providerCluEnt extends Structure {
        public static class ByReference extends providerCluEnt implements Structure.ByReference {}
        public static class ByValue extends providerCluEnt implements Structure.ByValue {}
        public providerCluEnt() {}
        public providerCluEnt(Pointer p) { super(p); read(); }


        public String cluName;

        public int status;
    }



    public static class rmbCluInfoEnt extends Structure {
        public static class ByReference extends rmbCluInfoEnt implements Structure.ByReference {}
        public static class ByValue extends rmbCluInfoEnt implements Structure.ByValue {}
        public rmbCluInfoEnt() {}
        public rmbCluInfoEnt(Pointer p) { super(p); read(); }

        public String cluster;
        public int numPairs;
        public Pointer /* queuePairEnt.ByReference */ queues;
        public int numApps;
        public Pointer /* rmbCluAppEnt.ByReference */ apps;
    }




    public static class leaseCluInfoEnt extends Structure {
        public static class ByReference extends leaseCluInfoEnt implements Structure.ByReference {}
        public static class ByValue extends leaseCluInfoEnt implements Structure.ByValue {}
        public leaseCluInfoEnt() {}
        public leaseCluInfoEnt(Pointer p) { super(p); read(); }


        public int flags;

        public int numConsumer;

        public Pointer /* consumerCluEnt.ByReference */ consumerClus;

        public int numProvider;

        public Pointer /* providerCluEnt.ByReference */ providerClus;
    }




    public static class clusterInfoEnt extends Structure {
        public static class ByReference extends clusterInfoEnt implements Structure.ByReference {}
        public static class ByValue extends clusterInfoEnt implements Structure.ByValue {}
        public clusterInfoEnt() {}
        public clusterInfoEnt(Pointer p) { super(p); read(); }

        public String cluster;
        public int numPairs;
        public Pointer /* queuePairEnt.ByReference */ queues;
        public int numApps;
        public Pointer /* rmbCluAppEnt.ByReference */ apps;
    }



    public static class clusterInfoEntEx extends Structure {
        public static class ByReference extends clusterInfoEntEx implements Structure.ByReference {}
        public static class ByValue extends clusterInfoEntEx implements Structure.ByValue {}
        public clusterInfoEntEx() {}
        public clusterInfoEntEx(Pointer p) { super(p); read(); }


        public rmbCluInfoEnt.ByReference rmbCluInfo;

        public leaseCluInfoEnt leaseCluInfo;
    }



    public static class eventInfoEnt extends Structure {
        public static class ByReference extends eventInfoEnt implements Structure.ByReference {}
        public static class ByValue extends eventInfoEnt implements Structure.ByValue {}
        public eventInfoEnt() {}
        public eventInfoEnt(Pointer p) { super(p); read(); }


        public String name;

        public int status;

        public int type;

        public int eType;

        public String userName;

        public String attributes;

        public int numDependents;

        public NativeLong updateTime;

        public long lastDisJob;

        public NativeLong lastDisTime;
    }


    public static final int ALL_EVENTS = 0x01;


    public static final int USER_GRP = 0x1;

    public static final int HOST_GRP = 0x2;

    public static final int HPART_HGRP = 0x4;

    public static final int GRP_RECURSIVE = 0x8;

    public static final int GRP_ALL = 0x10;

    public static final int NQSQ_GRP = 0x20;

    public static final int GRP_SHARES = 0x40;

    public static final int DYNAMIC_GRP = 0x800;

    public static final int GRP_CU = 0x1000;

    public static class userShares extends Structure {
        public static class ByReference extends userShares implements Structure.ByReference {}
        public static class ByValue extends userShares implements Structure.ByValue {}
        public userShares() {}
        public userShares(Pointer p) { super(p); read(); }


        public String user;

        public int shares;
    }




    public static class groupInfoEnt extends Structure {
        public static class ByReference extends groupInfoEnt implements Structure.ByReference {}
        public static class ByValue extends groupInfoEnt implements Structure.ByValue {}
        public groupInfoEnt() {}
        public groupInfoEnt(Pointer p) { super(p); read(); }


        public String group;

        public String memberList;

        public String adminMemberList;

        public int numUserShares;

        public Pointer /* userShares.ByReference */ userShares;


        public static final int GRP_NO_CONDENSE_OUTPUT = 0x01;

        public static final int GRP_CONDENSE_OUTPUT = 0x02;

        public static final int GRP_HAVE_REG_EXP = 0x04;

        public static final int GRP_SERVICE_CLASS = 0x08;

        public static final int GRP_IS_CU = 0x10;

        public int options;

        public String pattern;

        public String neg_pattern;

        public int cu_type;
    }



    public static class runJobRequest extends Structure {
        public static class ByReference extends runJobRequest implements Structure.ByReference {}
        public static class ByValue extends runJobRequest implements Structure.ByValue {}
        public runJobRequest() {}
        public runJobRequest(Pointer p) { super(p); read(); }


        public long jobId;

        public int numHosts;

        public Pointer hostname;


        public static final int RUNJOB_OPT_NORMAL = 0x01;

        public static final int RUNJOB_OPT_NOSTOP = 0x02;

        public static final int RUNJOB_OPT_PENDONLY = 0x04;

        public static final int RUNJOB_OPT_FROM_BEGIN = 0x08;

        public static final int RUNJOB_OPT_FREE = 0x10;

        public static final int RUNJOB_OPT_IGNORE_RUSAGE = 0x20;

        public int options;

        public IntByReference slots;
    }





    public static final int EXT_MSG_POST = 0x01;

    public static final int EXT_ATTA_POST = 0x02;

    public static final int EXT_MSG_READ = 0x04;

    public static final int EXT_ATTA_READ = 0x08;

    public static final int EXT_MSG_REPLAY = 0x10;

    public static final int EXT_MSG_POST_NOEVENT = 0x20;


    public static class jobExternalMsgReq extends Structure {
        public static class ByReference extends jobExternalMsgReq implements Structure.ByReference {}
        public static class ByValue extends jobExternalMsgReq implements Structure.ByValue {}
        public jobExternalMsgReq() {}
        public jobExternalMsgReq(Pointer p) { super(p); read(); }


        public int options;

        public long jobId;

        public String jobName;

        public int msgIdx;

        public String desc;

        public int userId;

        public NativeLong dataSize;

        public NativeLong postTime;

        public String userName;
    }




    public static final int EXT_DATA_UNKNOWN = 0;

    public static final int EXT_DATA_NOEXIST = 1;

    public static final int EXT_DATA_AVAIL = 2;

    public static final int EXT_DATA_UNAVAIL = 3;

    public static class jobExternalMsgReply extends Structure {
        public static class ByReference extends jobExternalMsgReply implements Structure.ByReference {}
        public static class ByValue extends jobExternalMsgReply implements Structure.ByValue {}
        public jobExternalMsgReply() {}
        public jobExternalMsgReply(Pointer p) { super(p); read(); }


        public long jobId;

        public int msgIdx;

        public String desc;

        public int userId;

        public NativeLong dataSize;

        public NativeLong postTime;

        public int dataStatus;

        public String userName;
    }




    public static class symJobInfo extends Structure {
        public static class ByReference extends symJobInfo implements Structure.ByReference {}
        public static class ByValue extends symJobInfo implements Structure.ByValue {}
        public symJobInfo() {}
        public symJobInfo(Pointer p) { super(p); read(); }


        public String partition;

        public int priority;

        public String jobFullName;

        public String auxCmdDesc;

        public String auxJobDesc;
    }



    public static class symJobStatus extends Structure {
        public static class ByReference extends symJobStatus implements Structure.ByReference {}
        public static class ByValue extends symJobStatus implements Structure.ByValue {}
        public symJobStatus() {}
        public symJobStatus(Pointer p) { super(p); read(); }


        public String desc;
    }



    public static class symJobProgress extends Structure {
        public static class ByReference extends symJobProgress implements Structure.ByReference {}
        public static class ByValue extends symJobProgress implements Structure.ByValue {}
        public symJobProgress() {}
        public symJobProgress(Pointer p) { super(p); read(); }


        public String desc;
    }




    public static class symJobStatusUpdateReq extends Structure {
        public static class ByReference extends symJobStatusUpdateReq implements Structure.ByReference {}
        public static class ByValue extends symJobStatusUpdateReq implements Structure.ByValue {}
        public symJobStatusUpdateReq() {}
        public symJobStatusUpdateReq(Pointer p) { super(p); read(); }


        public long jobId;

        public static final int SYM_JOB_UPDATE_NONE = 0x0;
        public static final int SYM_JOB_UPDATE_INFO = 0x1;
        public static final int SYM_JOB_UPDATE_STATUS = 0x2;
        public static final int SYM_JOB_UPDATE_PROGRESS = 0x4;

        public int bitOption;
        public symJobInfo info;
        public int numOfJobStatus;
        public Pointer /* symJobStatus.ByReference */ status;
        public symJobProgress progress;
    }



    public static class symJobStatusUpdateReqArray extends Structure {
        public static class ByReference extends symJobStatusUpdateReqArray implements Structure.ByReference {}
        public static class ByValue extends symJobStatusUpdateReqArray implements Structure.ByValue {}
        public symJobStatusUpdateReqArray() {}
        public symJobStatusUpdateReqArray(Pointer p) { super(p); read(); }

        public int numOfJobReq;
        public Pointer /* symJobStatusUpdateReq.ByReference */ symJobReqs;
    }





    public static class symJobUpdateAck extends Structure {
        public static class ByReference extends symJobUpdateAck implements Structure.ByReference {}
        public static class ByValue extends symJobUpdateAck implements Structure.ByValue {}
        public symJobUpdateAck() {}
        public symJobUpdateAck(Pointer p) { super(p); read(); }

        public static int SYM_UPDATE_ACK_OK = 0;
        public static final int SYM_UPDATE_ACK_ERR = 1;
        public int ackCode;

        public String desc;
    }



    public static class symJobStatusUpdateReply extends Structure {
        public static class ByReference extends symJobStatusUpdateReply implements Structure.ByReference {}
        public static class ByValue extends symJobStatusUpdateReply implements Structure.ByValue {}
        public symJobStatusUpdateReply() {}
        public symJobStatusUpdateReply(Pointer p) { super(p); read(); }


        public long jobId;
        public static final int SYM_UPDATE_INFO_IDX = 0;
        public static final int SYM_UPDATE_STATUS_IDX = 1;
        public static final int SYM_UPDATE_PROGRESS_IDX = 2;
        public static final int NUM_SYM_UPDATE_ACK = 3;
        public symJobUpdateAck[] acks = new symJobUpdateAck[NUM_SYM_UPDATE_ACK];
    }



    public static class symJobStatusUpdateReplyArray extends Structure {
        public static class ByReference extends symJobStatusUpdateReplyArray implements Structure.ByReference {}
        public static class ByValue extends symJobStatusUpdateReplyArray implements Structure.ByValue {}
        public symJobStatusUpdateReplyArray() {}
        public symJobStatusUpdateReplyArray(Pointer p) { super(p); read(); }

        public int numOfJobReply;
        public Pointer /* symJobStatusUpdateReply.ByReference */ symJobReplys;
    }






    public static final int REQUEUE_DONE = 0x1;

    public static final int REQUEUE_EXIT = 0x2;

    public static final int REQUEUE_RUN = 0x4;

    public static class jobrequeue extends Structure {
        public static class ByReference extends jobrequeue implements Structure.ByReference {}
        public static class ByValue extends jobrequeue implements Structure.ByValue {}
        public jobrequeue() {}
        public jobrequeue(Pointer p) { super(p); read(); }


        public long jobId;

        public int status;

        public int options;
    }



    public static class requeueEStruct extends Structure {
        public static class ByReference extends requeueEStruct implements Structure.ByReference {}
        public static class ByValue extends requeueEStruct implements Structure.ByValue {}
        public requeueEStruct() {}
        public requeueEStruct(Pointer p) { super(p); read(); }


        public int type;

        public static final int RQE_NORMAL = 0;

        public static final int RQE_EXCLUDE = 1;

        public static final int RQE_END = 255;

        public int value;

        public int interval;
    }



    public static class requeue extends Structure {
        public static class ByReference extends requeue implements Structure.ByReference {}
        public static class ByValue extends requeue implements Structure.ByValue {}
        public requeue() {}
        public requeue(Pointer p) { super(p); read(); }

        public int numReqValues;
        public Pointer /* requeueEStruct.ByReference */ reqValues;
    }






    public static class serviceClass extends Structure {
        public static class ByReference extends serviceClass implements Structure.ByReference {}
        public static class ByValue extends serviceClass implements Structure.ByValue {}
        public serviceClass() {}
        public serviceClass(Pointer p) { super(p); read(); }


        public String name;

        public float priority;

        public int ngoals;

        public Pointer /* objective.ByReference */ goals;

        public String userGroups;

        public String description;

        public String controlAction;

        public float throughput;

        public int[] counters = new int[NUM_JGRP_COUNTERS + 1];

        public String consumer;

        public slaControl.ByReference ctrl;

        public slaControlExt.ByReference ctrlExt;
    }




    public static final int GOAL_WINDOW_OPEN = 0x1;
    public static final int GOAL_WINDOW_CLOSED = 0x2;
    public static final int GOAL_ONTIME = 0x4;
    public static final int GOAL_DELAYED = 0x8;
    public static final int GOAL_DISABLED = 0x10;


    public static interface objectives {
        public static int GOAL_DEADLINE = 0;
        public static int GOAL_VELOCITY = 1;
        public static int GOAL_THROUGHPUT = 2;
    }




    public static class objective extends Structure {
        public static class ByReference extends objective implements Structure.ByReference {}
        public static class ByValue extends objective implements Structure.ByValue {}
        public objective() {}
        public objective(Pointer p) { super(p); read(); }


        public String spec;

        public int type;

        public int state;

        public int goal;

        public int actual;

        public int optimum;

        public int minimum;
    }




    public static class slaControl extends Structure {
        public static class ByReference extends slaControl implements Structure.ByReference {}
        public static class ByValue extends slaControl implements Structure.ByValue {}
        public slaControl() {}
        public slaControl(Pointer p) { super(p); read(); }


        public String sla;

        public String consumer;

        public int maxHostIdleTime;

        public int recallTimeout;

        public int numHostRecalled;

        public String egoResReq;
    }



    public static class slaControlExt extends Structure {
        public static class ByReference extends slaControlExt implements Structure.ByReference {}
        public static class ByValue extends slaControlExt implements Structure.ByValue {}
        public slaControlExt() {}
        public slaControlExt(Pointer p) { super(p); read(); }


        public int allocflags;

        public int tile;
    }




    public static class appInfoEnt extends Structure {
        public static class ByReference extends appInfoEnt implements Structure.ByReference {}
        public static class ByValue extends appInfoEnt implements Structure.ByValue {}
        public appInfoEnt() {}
        public appInfoEnt(Pointer p) { super(p); read(); }


        public String name;

        public String description;

        public int numJobs;

        public int numPEND;

        public int numRUN;

        public int numSSUSP;

        public int numUSUSP;

        public int numRESERVE;

        public int aAttrib;

        public int chunkJobSize;

        public String requeueEValues;

        public String successEValues;

        public String preCmd;

        public String postCmd;

        public String jobStarter;

        public String suspendActCmd;

        public String resumeActCmd;

        public String terminateActCmd;

        public int memLimitType;

        public int[] defLimits = new int[LibLsf.LSF_RLIM_NLIMITS];

        public String hostSpec;

        public String resReq;

        public int maxProcLimit;

        public int defProcLimit;

        public int minProcLimit;

        public int runTime;

        public int jobIncludePostProc;

        public int jobPostProcTimeOut;

        public String rTaskGoneAction;

        public String djobEnvScript;

        public int djobRuInterval;

        public int djobHbInterval;

        public String djobCommfailAction;

        public int djobDisabled;

        public int djobResizeGracePeriod;

        public String chkpntDir;

        public String chkpntMethod;

        public int chkpntPeriod;

        public int initChkpntPeriod;

        public int migThreshold;

        public int maxJobPreempt;

        public int maxPreExecRetry;

        public int localMaxPreExecRetry;

        public int maxJobRequeue;

        public int noPreemptRunTime;

        public int noPreemptFinishTime;

        public int noPreemptRunTimePercent;

        public int noPreemptFinishTimePercent;

        public int usePam;

        public int bindingOption;

        public int persistHostOrder;

        public String resizeNotifyCmd;
    }




    public static final int A_ATTRIB_RERUNNABLE = 0x01;

    public static final int A_ATTRIB_NONRERUNNABLE = 0x02;

    public static final int A_ATTRIB_DEFAULT = 0x04;

    public static final int A_ATTRIB_ABS_RUNLIMIT = 0x08;

    public static final int A_ATTRIB_JOBBINDING = 0x10;

    public static final int A_ATTRIB_NONJOBBINDING = 0x20;

    public static final int A_ATTRIB_CHKPNT = 0x40;

    public static final int A_ATTRIB_RESIZABLE = 0x80;

    public static final int A_ATTRIB_AUTO_RESIZABLE = 0x100;


    public static final int BINDING_OPTION_BALANCE = 0x1;
    public static final int BINDING_OPTION_PACK = 0x2;
    public static final int BINDING_OPTION_ANY = 0x4;
    public static final int BINDING_OPTION_USER = 0x8;
    public static final int BINDING_OPTION_USER_CPU_LIST = 0x10;
    public static final int BINDING_OPTION_NONE = 0x20;


    public static final int TO_TOP = 1;

    public static final int TO_BOTTOM = 2;


    public static final int QUEUE_OPEN = 1;

    public static final int QUEUE_CLOSED = 2;

    public static final int QUEUE_ACTIVATE = 3;

    public static final int QUEUE_INACTIVATE = 4;

    public static final int QUEUE_CLEAN = 5;

    public static class queueCtrlReq extends Structure {
        public static class ByReference extends queueCtrlReq implements Structure.ByReference {}
        public static class ByValue extends queueCtrlReq implements Structure.ByValue {}
        public queueCtrlReq() {}
        public queueCtrlReq(Pointer p) { super(p); read(); }


        public String queue;

        public int opCode;

        public String message;
    }




    public static final int HOST_OPEN = 1;

    public static final int HOST_CLOSE = 2;

    public static final int HOST_REBOOT = 3;

    public static final int HOST_SHUTDOWN = 4;

    public static final int HOST_CLOSE_REMOTE = 5;

    public static class hostCtrlReq extends Structure {
        public static class ByReference extends hostCtrlReq implements Structure.ByReference {}
        public static class ByValue extends hostCtrlReq implements Structure.ByValue {}
        public hostCtrlReq() {}
        public hostCtrlReq(Pointer p) { super(p); read(); }


        public String host;

        public int opCode;

        public String message;
    }



    public static final int HGHOST_ADD = 1;
    public static final int HGHOST_DEL = 2;

    public static class hgCtrlReq extends Structure {
        public static class ByReference extends hgCtrlReq implements Structure.ByReference {}
        public static class ByValue extends hgCtrlReq implements Structure.ByValue {}
        public hgCtrlReq() {}
        public hgCtrlReq(Pointer p) { super(p); read(); }

        public int opCode;
        public String grpname;
        public int numhosts;
        public Pointer hosts;
        public String message;
    }



    public static class hgCtrlReply extends Structure {
        public static class ByReference extends hgCtrlReply implements Structure.ByReference {}
        public static class ByValue extends hgCtrlReply implements Structure.ByValue {}
        public hgCtrlReply() {}
        public hgCtrlReply(Pointer p) { super(p); read(); }

        public int numsucc;
        public int numfail;
        public Pointer succHosts;
        public Pointer failHosts;
        public IntByReference failReasons;
    }




    public static final int MBD_RESTART = 0;

    public static final int MBD_RECONFIG = 1;

    public static final int MBD_CKCONFIG = 2;

    public static class mbdCtrlReq extends Structure {
        public static class ByReference extends mbdCtrlReq implements Structure.ByReference {}
        public static class ByValue extends mbdCtrlReq implements Structure.ByValue {}
        public mbdCtrlReq() {}
        public mbdCtrlReq(Pointer p) { super(p); read(); }


        public int opCode;

        public String name;

        public String message;
    }



    public static final int PERFMON_START = 1;
    public static final int PERFMON_STOP = 2;
    public static final int PERFMON_SET_PERIOD = 3;


    public static final int DEF_PERFMON_PERIOD = 60;


    public static class perfmonMetricsEnt extends Structure {
        public static class ByReference extends perfmonMetricsEnt implements Structure.ByReference {}
        public static class ByValue extends perfmonMetricsEnt implements Structure.ByValue {}
        public perfmonMetricsEnt() {}
        public perfmonMetricsEnt(Pointer p) { super(p); read(); }

        public String name;

        public NativeLong current;

        public NativeLong max;

        public NativeLong min;

        public NativeLong avg;

        public String total;
    }




    public static class perfmonInfo extends Structure {
        public static class ByReference extends perfmonInfo implements Structure.ByReference {}
        public static class ByValue extends perfmonInfo implements Structure.ByValue {}
        public perfmonInfo() {}
        public perfmonInfo(Pointer p) { super(p); read(); }

        public int num;

        public Pointer /* perfmonMetricsEnt.ByReference */ record;

        public int period;

        public NativeLong start;

        public NativeLong end;
    }



    public static final int JGRP_RELEASE_PARENTONLY = 0x01;


    public static class logSwitchLog extends Structure {
        public static class ByReference extends logSwitchLog implements Structure.ByReference {}
        public static class ByValue extends logSwitchLog implements Structure.ByValue {}
        public logSwitchLog() {}
        public logSwitchLog(Pointer p) { super(p); read(); }


        public int lastJobId;



    }



    public static class dataLoggingLog extends Structure {
        public static class ByReference extends dataLoggingLog implements Structure.ByReference {}
        public static class ByValue extends dataLoggingLog implements Structure.ByValue {}
        public dataLoggingLog() {}
        public dataLoggingLog(Pointer p) { super(p); read(); }


        public NativeLong loggingTime;
    }



    public static class jgrpNewLog extends Structure {
        public static class ByReference extends jgrpNewLog implements Structure.ByReference {}
        public static class ByValue extends jgrpNewLog implements Structure.ByValue {}
        public jgrpNewLog() {}
        public jgrpNewLog(Pointer p) { super(p); read(); }


        public int userId;

        public NativeLong submitTime;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];

        public String depCond;

        public String timeEvent;

        public String groupSpec;

        public String destSpec;

        public int delOptions;

        public int delOptions2;

        public int fromPlatform;

        public String sla;

        public int maxJLimit;

        public int options;
    }



    public static class jgrpCtrlLog extends Structure {
        public static class ByReference extends jgrpCtrlLog implements Structure.ByReference {}
        public static class ByValue extends jgrpCtrlLog implements Structure.ByValue {}
        public jgrpCtrlLog() {}
        public jgrpCtrlLog(Pointer p) { super(p); read(); }


        public int userId;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];

        public String groupSpec;

        public int options;

        public int ctrlOp;
    }



    public static class jgrpStatusLog extends Structure {
        public static class ByReference extends jgrpStatusLog implements Structure.ByReference {}
        public static class ByValue extends jgrpStatusLog implements Structure.ByValue {}
        public jgrpStatusLog() {}
        public jgrpStatusLog(Pointer p) { super(p); read(); }


        public String groupSpec;

        public int status;

        public int oldStatus;
    }



    public static class jobNewLog extends Structure {
        public static class ByReference extends jobNewLog implements Structure.ByReference {}
        public static class ByValue extends jobNewLog implements Structure.ByValue {}
        public jobNewLog() {}
        public jobNewLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int userId;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];

        public int options;

        public int options2;

        public int numProcessors;

        public NativeLong submitTime;

        public NativeLong beginTime;

        public NativeLong termTime;

        public int sigValue;

        public int chkpntPeriod;

        public int restartPid;

        public int[] rLimits = new int[LibLsf.LSF_RLIM_NLIMITS];

        public byte[] hostSpec = new byte[LibLsf.MAXHOSTNAMELEN];

        public float hostFactor;

        public int umask;

        public byte[] queue = new byte[MAX_LSB_NAME_LEN];

        public String resReq;

        public byte[] fromHost = new byte[LibLsf.MAXHOSTNAMELEN];

        public String cwd;

        public String chkpntDir;

        public String inFile;

        public String outFile;

        public String errFile;

        public String inFileSpool;

        public String commandSpool;

        public String jobSpoolDir;

        public String subHomeDir;

        public String jobFile;

        public int numAskedHosts;

        public Pointer askedHosts;

        public String dependCond;

        public String timeEvent;

        public String jobName;

        public String command;

        public int nxf;

        public Pointer /* xFile.ByReference */ xf;

        public String preExecCmd;

        public String mailUser;

        public String projectName;

        public int niosPort;

        public int maxNumProcessors;

        public String schedHostType;

        public String loginShell;

        public String userGroup;

        public String exceptList;

        public int idx;

        public int userPriority;

        public String rsvId;

        public String jobGroup;

        public String extsched;

        public int warningTimePeriod;

        public String warningAction;

        public String sla;

        public int SLArunLimit;

        public String licenseProject;

        public int options3;

        public String app;

        public String postExecCmd;

        public int runtimeEstimation;

        public String requeueEValues;

        public int initChkpntPeriod;

        public int migThreshold;

        public String notifyCmd;

        public String jobDescription;

        public submit_ext.ByReference submitExt;








    }




    public static class jobModLog extends Structure {
        public static class ByReference extends jobModLog implements Structure.ByReference {}
        public static class ByValue extends jobModLog implements Structure.ByValue {}
        public jobModLog() {}
        public jobModLog(Pointer p) { super(p); read(); }


        public int options;

        public int options2;

        public int delOptions;

        public int delOptions2;

        public int userId;

        public String userName;

        public int submitTime;

        public int umask;

        public int numProcessors;

        public NativeLong beginTime;

        public NativeLong termTime;

        public int sigValue;

        public int restartPid;


        public String jobName;

        public String queue;


        public int numAskedHosts;

        public Pointer askedHosts;


        public String resReq;

        public int[] rLimits = new int[LibLsf.LSF_RLIM_NLIMITS];

        public String hostSpec;


        public String dependCond;

        public String timeEvent;


        public String subHomeDir;

        public String inFile;

        public String outFile;

        public String errFile;

        public String command;

        public String inFileSpool;

        public String commandSpool;

        public int chkpntPeriod;

        public String chkpntDir;

        public int nxf;

        public Pointer /* xFile.ByReference */ xf;


        public String jobFile;

        public String fromHost;

        public String cwd;


        public String preExecCmd;

        public String mailUser;

        public String projectName;


        public int niosPort;

        public int maxNumProcessors;


        public String loginShell;

        public String schedHostType;

        public String userGroup;

        public String exceptList;

        public int userPriority;

        public String rsvId;

        public String extsched;

        public int warningTimePeriod;

        public String warningAction;

        public String jobGroup;

        public String sla;

        public String licenseProject;

        public int options3;

        public int delOptions3;

        public String app;

        public String apsString;

        public String postExecCmd;

        public int runtimeEstimation;

        public String requeueEValues;

        public int initChkpntPeriod;

        public int migThreshold;

        public String notifyCmd;

        public String jobDescription;

        public submit_ext.ByReference submitExt;
    }



    public static class jobStartLog extends Structure {
        public static class ByReference extends jobStartLog implements Structure.ByReference {}
        public static class ByValue extends jobStartLog implements Structure.ByValue {}
        public jobStartLog() {}
        public jobStartLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int jStatus;

        public int jobPid;

        public int jobPGid;

        public float hostFactor;

        public int numExHosts;

        public Pointer execHosts;

        public String queuePreCmd;

        public String queuePostCmd;

        public int jFlags;

        public String userGroup;

        public int idx;

        public String additionalInfo;

        public int duration4PreemptBackfill;

        public int jFlags2;
    }



    public static class jobStartAcceptLog extends Structure {
        public static class ByReference extends jobStartAcceptLog implements Structure.ByReference {}
        public static class ByValue extends jobStartAcceptLog implements Structure.ByValue {}
        public jobStartAcceptLog() {}
        public jobStartAcceptLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int jobPid;

        public int jobPGid;

        public int idx;
    }



    public static class jobExecuteLog extends Structure {
        public static class ByReference extends jobExecuteLog implements Structure.ByReference {}
        public static class ByValue extends jobExecuteLog implements Structure.ByValue {}
        public jobExecuteLog() {}
        public jobExecuteLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int execUid;

        public String execHome;

        public String execCwd;

        public int jobPGid;

        public String execUsername;

        public int jobPid;

        public int idx;

        public String additionalInfo;

        public int SLAscaledRunLimit;

        public int position;

        public String execRusage;

        public int duration4PreemptBackfill;
    }




    public static class jobStatusLog extends Structure {
        public static class ByReference extends jobStatusLog implements Structure.ByReference {}
        public static class ByValue extends jobStatusLog implements Structure.ByValue {}
        public jobStatusLog() {}
        public jobStatusLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int jStatus;

        public int reason;

        public int subreasons;

        public float cpuTime;

        public NativeLong endTime;

        public int ru;

        public LibLsf.lsfRusage lsfRusage;

        public int jFlags;

        public int exitStatus;

        public int idx;

        public int exitInfo;
    }




    public static class sbdJobStatusLog extends Structure {
        public static class ByReference extends sbdJobStatusLog implements Structure.ByReference {}
        public static class ByValue extends sbdJobStatusLog implements Structure.ByValue {}
        public sbdJobStatusLog() {}
        public sbdJobStatusLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int jStatus;

        public int reasons;

        public int subreasons;

        public int actPid;

        public int actValue;

        public NativeLong actPeriod;

        public int actFlags;

        public int actStatus;

        public int actReasons;

        public int actSubReasons;

        public int idx;

        public int sigValue;

        public int exitInfo;
    }



    public static class sbdUnreportedStatusLog extends Structure {
        public static class ByReference extends sbdUnreportedStatusLog implements Structure.ByReference {}
        public static class ByValue extends sbdUnreportedStatusLog implements Structure.ByValue {}
        public sbdUnreportedStatusLog() {}
        public sbdUnreportedStatusLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int actPid;

        public int jobPid;

        public int jobPGid;

        public int newStatus;

        public int reason;

        public int subreasons;

        public LibLsf.lsfRusage lsfRusage;

        public int execUid;

        public int exitStatus;

        public String execCwd;

        public String execHome;

        public String execUsername;

        public int msgId;

        public LibLsf.jRusage runRusage;

        public int sigValue;

        public int actStatus;

        public int seq;

        public int idx;

        public int exitInfo;
    }



    public static class jobSwitchLog extends Structure {
        public static class ByReference extends jobSwitchLog implements Structure.ByReference {}
        public static class ByValue extends jobSwitchLog implements Structure.ByValue {}
        public jobSwitchLog() {}
        public jobSwitchLog(Pointer p) { super(p); read(); }


        public int userId;

        public int jobId;

        public byte[] queue = new byte[MAX_LSB_NAME_LEN];

        public int idx;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];
    }



    public static class jobMoveLog extends Structure {
        public static class ByReference extends jobMoveLog implements Structure.ByReference {}
        public static class ByValue extends jobMoveLog implements Structure.ByValue {}
        public jobMoveLog() {}
        public jobMoveLog(Pointer p) { super(p); read(); }


        public int userId;

        public int jobId;

        public int position;

        public int base;

        public int idx;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];
    }



    public static class chkpntLog extends Structure {
        public static class ByReference extends chkpntLog implements Structure.ByReference {}
        public static class ByValue extends chkpntLog implements Structure.ByValue {}
        public chkpntLog() {}
        public chkpntLog(Pointer p) { super(p); read(); }


        public int jobId;

        public NativeLong period;

        public int pid;

        public int ok;

        public int flags;

        public int idx;
    }



    public static class jobRequeueLog extends Structure {
        public static class ByReference extends jobRequeueLog implements Structure.ByReference {}
        public static class ByValue extends jobRequeueLog implements Structure.ByValue {}
        public jobRequeueLog() {}
        public jobRequeueLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int idx;
    }



    public static class jobCleanLog extends Structure {
        public static class ByReference extends jobCleanLog implements Structure.ByReference {}
        public static class ByValue extends jobCleanLog implements Structure.ByValue {}
        public jobCleanLog() {}
        public jobCleanLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int idx;
    }



    public static class jobExceptionLog extends Structure {
        public static class ByReference extends jobExceptionLog implements Structure.ByReference {}
        public static class ByValue extends jobExceptionLog implements Structure.ByValue {}
        public jobExceptionLog() {}
        public jobExceptionLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int exceptMask;

        public int actMask;

        public NativeLong timeEvent;

        public int exceptInfo;

        public int idx;
    }



    public static class sigactLog extends Structure {
        public static class ByReference extends sigactLog implements Structure.ByReference {}
        public static class ByValue extends sigactLog implements Structure.ByValue {}
        public sigactLog() {}
        public sigactLog(Pointer p) { super(p); read(); }


        public int jobId;

        public NativeLong period;

        public int pid;

        public int jStatus;

        public int reasons;

        public int flags;

        public String signalSymbol;

        public int actStatus;

        public int idx;
    }



    public static class migLog extends Structure {
        public static class ByReference extends migLog implements Structure.ByReference {}
        public static class ByValue extends migLog implements Structure.ByValue {}
        public migLog() {}
        public migLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int numAskedHosts;

        public Pointer askedHosts;

        public int userId;

        public int idx;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];
    }



    public static class signalLog extends Structure {
        public static class ByReference extends signalLog implements Structure.ByReference {}
        public static class ByValue extends signalLog implements Structure.ByValue {}
        public signalLog() {}
        public signalLog(Pointer p) { super(p); read(); }


        public int userId;

        public int jobId;

        public String signalSymbol;

        public int runCount;

        public int idx;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];
    }



    public static class queueCtrlLog extends Structure {
        public static class ByReference extends queueCtrlLog implements Structure.ByReference {}
        public static class ByValue extends queueCtrlLog implements Structure.ByValue {}
        public queueCtrlLog() {}
        public queueCtrlLog(Pointer p) { super(p); read(); }


        public int opCode;

        public byte[] queue = new byte[MAX_LSB_NAME_LEN];

        public int userId;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];

        public byte[] message = new byte[LibLsf.MAXLINELEN];
    }




    public static class newDebugLog extends Structure {
        public static class ByReference extends newDebugLog implements Structure.ByReference {}
        public static class ByValue extends newDebugLog implements Structure.ByValue {}
        public newDebugLog() {}
        public newDebugLog(Pointer p) { super(p); read(); }


        public int opCode;

        public int level;

        public int _logclass;

        public int turnOff;

        public byte[] logFileName = new byte[LibLsf.MAXLSFNAMELEN];

        public int userId;
    }



    public static class hostCtrlLog extends Structure {
        public static class ByReference extends hostCtrlLog implements Structure.ByReference {}
        public static class ByValue extends hostCtrlLog implements Structure.ByValue {}
        public hostCtrlLog() {}
        public hostCtrlLog(Pointer p) { super(p); read(); }


        public int opCode;

        public byte[] host = new byte[LibLsf.MAXHOSTNAMELEN];

        public int userId;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];

        public byte[] message = new byte[LibLsf.MAXLINELEN];
    }



    public static class hgCtrlLog extends Structure {
        public static class ByReference extends hgCtrlLog implements Structure.ByReference {}
        public static class ByValue extends hgCtrlLog implements Structure.ByValue {}
        public hgCtrlLog() {}
        public hgCtrlLog(Pointer p) { super(p); read(); }


        public int opCode;

        public byte[] host = new byte[LibLsf.MAXHOSTNAMELEN];

        public byte[] grpname = new byte[LibLsf.MAXHOSTNAMELEN];

        public int userId;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];

        public byte[] message = new byte[LibLsf.MAXLINELEN];
    }




    public static final int SIMU_STATUS_READYSCHEDULE = 0x01;

    public static class mbdStartLog extends Structure {
        public static class ByReference extends mbdStartLog implements Structure.ByReference {}
        public static class ByValue extends mbdStartLog implements Structure.ByValue {}
        public mbdStartLog() {}
        public mbdStartLog(Pointer p) { super(p); read(); }


        public byte[] master = new byte[LibLsf.MAXHOSTNAMELEN];

        public byte[] cluster = new byte[LibLsf.MAXLSFNAMELEN];

        public int numHosts;

        public int numQueues;
    public int    simDiffTime;
    public int    pendJobsThreshold;
    public int    simStatus;
    }



    public static class mbdSimStatusLog extends Structure {
        public static class ByReference extends mbdSimStatusLog implements Structure.ByReference {}
        public static class ByValue extends mbdSimStatusLog implements Structure.ByValue {}
        public mbdSimStatusLog() {}
        public mbdSimStatusLog(Pointer p) { super(p); read(); }


        public int simStatus;
    }



    public static class mbdDieLog extends Structure {
        public static class ByReference extends mbdDieLog implements Structure.ByReference {}
        public static class ByValue extends mbdDieLog implements Structure.ByValue {}
        public mbdDieLog() {}
        public mbdDieLog(Pointer p) { super(p); read(); }


        public byte[] master = new byte[LibLsf.MAXHOSTNAMELEN];

        public int numRemoveJobs;

        public int exitCode;

        public byte[] message = new byte[LibLsf.MAXLINELEN];
    }



    public static class unfulfillLog extends Structure {
        public static class ByReference extends unfulfillLog implements Structure.ByReference {}
        public static class ByValue extends unfulfillLog implements Structure.ByValue {}
        public unfulfillLog() {}
        public unfulfillLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int notSwitched;

        public int sig;

        public int sig1;

        public int sig1Flags;

        public NativeLong chkPeriod;

        public int notModified;

        public int idx;

        public int miscOpts4PendSig;
    }



    public static final int TERM_UNKNOWN = 0;
    public static final int TERM_PREEMPT = 1;
    public static final int TERM_WINDOW = 2;
    public static final int TERM_LOAD = 3;
    public static final int TERM_OTHER = 4;
    public static final int TERM_RUNLIMIT = 5;
    public static final int TERM_DEADLINE = 6;
    public static final int TERM_PROCESSLIMIT = 7;
    public static final int TERM_FORCE_OWNER = 8;
    public static final int TERM_FORCE_ADMIN = 9;
    public static final int TERM_REQUEUE_OWNER = 10;
    public static final int TERM_REQUEUE_ADMIN = 11;
    public static final int TERM_CPULIMIT = 12;
    public static final int TERM_CHKPNT = 13;
    public static final int TERM_OWNER = 14;
    public static final int TERM_ADMIN = 15;
    public static final int TERM_MEMLIMIT = 16;
    public static final int TERM_EXTERNAL_SIGNAL = 17;
    public static final int TERM_RMS = 18;
    public static final int TERM_ZOMBIE = 19;
    public static final int TERM_SWAP = 20;
    public static final int TERM_THREADLIMIT = 21;
    public static final int TERM_SLURM = 22;
    public static final int TERM_BUCKET_KILL = 23;
    public static final int TERM_CTRL_PID = 24;
    public static final int TERM_CWD_NOTEXIST = 25;

    public static class jobFinishLog extends Structure {
        public static class ByReference extends jobFinishLog implements Structure.ByReference {}
        public static class ByValue extends jobFinishLog implements Structure.ByValue {}
        public jobFinishLog() {}
        public jobFinishLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int userId;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];

        public int options;

        public int numProcessors;

        public int jStatus;

        public NativeLong submitTime;

        public NativeLong beginTime;

        public NativeLong termTime;

        public NativeLong startTime;

        public NativeLong endTime;

        public byte[] queue = new byte[MAX_LSB_NAME_LEN];

        public String resReq;

        public byte[] fromHost = new byte[LibLsf.MAXHOSTNAMELEN];

        public String cwd;

        public String inFile;

        public String outFile;

        public String errFile;

        public String inFileSpool;

        public String commandSpool;

        public String jobFile;

        public int numAskedHosts;

        public Pointer askedHosts;

        public float hostFactor;

        public int numExHosts;

        public Pointer execHosts;

        public float cpuTime;

        public String jobName;

        public String command;

        public LibLsf.lsfRusage lsfRusage;

        public String dependCond;

        public String timeEvent;

        public String preExecCmd;

        public String mailUser;

        public String projectName;

        public int exitStatus;

        public int maxNumProcessors;

        public String loginShell;

        public int idx;

        public int maxRMem;

        public int maxRSwap;

        public String rsvId;

        public String sla;

        public int exceptMask;

        public String additionalInfo;

        public int exitInfo;

        public int warningTimePeriod;

        public String warningAction;

        public String chargedSAAP;

        public String licenseProject;

        public String app;

        public String postExecCmd;

        public int runtimeEstimation;

        public String jgroup;

        public int options2;

        public String requeueEValues;

        public String notifyCmd;

        public NativeLong lastResizeTime;

        public String jobDescription;

        public submit_ext.ByReference submitExt;
    }





    public static class loadIndexLog extends Structure {
        public static class ByReference extends loadIndexLog implements Structure.ByReference {}
        public static class ByValue extends loadIndexLog implements Structure.ByValue {}
        public loadIndexLog() {}
        public loadIndexLog(Pointer p) { super(p); read(); }


        public int nIdx;

        public Pointer name;
    }



    public static class calendarLog extends Structure {
        public static class ByReference extends calendarLog implements Structure.ByReference {}
        public static class ByValue extends calendarLog implements Structure.ByValue {}
        public calendarLog() {}
        public calendarLog(Pointer p) { super(p); read(); }


        public int options;

        public int userId;

        public String name;

        public String desc;

        public String calExpr;
    }



    public static class jobForwardLog extends Structure {
        public static class ByReference extends jobForwardLog implements Structure.ByReference {}
        public static class ByValue extends jobForwardLog implements Structure.ByValue {}
        public jobForwardLog() {}
        public jobForwardLog(Pointer p) { super(p); read(); }


        public int jobId;

        public String cluster;

        public int numReserHosts;

        public Pointer reserHosts;

        public int idx;

        public int jobRmtAttr;
    }



    public static class jobAcceptLog extends Structure {
        public static class ByReference extends jobAcceptLog implements Structure.ByReference {}
        public static class ByValue extends jobAcceptLog implements Structure.ByValue {}
        public jobAcceptLog() {}
        public jobAcceptLog(Pointer p) { super(p); read(); }


        public int jobId;

        public long remoteJid;

        public String cluster;

        public int idx;

        public int jobRmtAttr;
    }



    public static class statusAckLog extends Structure {
        public static class ByReference extends statusAckLog implements Structure.ByReference {}
        public static class ByValue extends statusAckLog implements Structure.ByValue {}
        public statusAckLog() {}
        public statusAckLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int statusNum;

        public int idx;
    }



    public static class jobMsgLog extends Structure {
        public static class ByReference extends jobMsgLog implements Structure.ByReference {}
        public static class ByValue extends jobMsgLog implements Structure.ByValue {}
        public jobMsgLog() {}
        public jobMsgLog(Pointer p) { super(p); read(); }


        public int usrId;

        public int jobId;

        public int msgId;

        public int type;

        public String src;

        public String dest;

        public String msg;

        public int idx;
    }



    public static class jobMsgAckLog extends Structure {
        public static class ByReference extends jobMsgAckLog implements Structure.ByReference {}
        public static class ByValue extends jobMsgAckLog implements Structure.ByValue {}
        public jobMsgAckLog() {}
        public jobMsgAckLog(Pointer p) { super(p); read(); }


        public int usrId;

        public int jobId;

        public int msgId;

        public int type;

        public String src;

        public String dest;

        public String msg;

        public int idx;
    }



    public static class jobOccupyReqLog extends Structure {
        public static class ByReference extends jobOccupyReqLog implements Structure.ByReference {}
        public static class ByValue extends jobOccupyReqLog implements Structure.ByValue {}
        public jobOccupyReqLog() {}
        public jobOccupyReqLog(Pointer p) { super(p); read(); }


        public int userId;

        public int jobId;

        public int numOccupyRequests;

        public Pointer occupyReqList;

        public int idx;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];
    }



    public static class jobVacatedLog extends Structure {
        public static class ByReference extends jobVacatedLog implements Structure.ByReference {}
        public static class ByValue extends jobVacatedLog implements Structure.ByValue {}
        public jobVacatedLog() {}
        public jobVacatedLog(Pointer p) { super(p); read(); }


        public int userId;

        public int jobId;

        public int idx;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];
    }



    public static class jobForceRequestLog extends Structure {
        public static class ByReference extends jobForceRequestLog implements Structure.ByReference {}
        public static class ByValue extends jobForceRequestLog implements Structure.ByValue {}
        public jobForceRequestLog() {}
        public jobForceRequestLog(Pointer p) { super(p); read(); }


        public int userId;

        public int numExecHosts;

        public Pointer execHosts;

        public int jobId;

        public int idx;

        public int options;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];

        public String queue;
    }



    public static class jobChunkLog extends Structure {
        public static class ByReference extends jobChunkLog implements Structure.ByReference {}
        public static class ByValue extends jobChunkLog implements Structure.ByValue {}
        public jobChunkLog() {}
        public jobChunkLog(Pointer p) { super(p); read(); }


        public NativeLong membSize;

        public LongByReference membJobId;

        public NativeLong numExHosts;

        public Pointer execHosts;
    }



    public static class jobExternalMsgLog extends Structure {
        public static class ByReference extends jobExternalMsgLog implements Structure.ByReference {}
        public static class ByValue extends jobExternalMsgLog implements Structure.ByValue {}
        public jobExternalMsgLog() {}
        public jobExternalMsgLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int idx;

        public int msgIdx;

        public String desc;

        public int userId;

        public NativeLong dataSize;

        public NativeLong postTime;

        public int dataStatus;

        public String fileName;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];
    }



    public static class rsvRes extends Structure {
        public static class ByReference extends rsvRes implements Structure.ByReference {}
        public static class ByValue extends rsvRes implements Structure.ByValue {}
        public rsvRes() {}
        public rsvRes(Pointer p) { super(p); read(); }


        public String resName;

        public int count;

        public int usedAmt;
    }



    public static class rsvFinishLog extends Structure {
        public static class ByReference extends rsvFinishLog implements Structure.ByReference {}
        public static class ByValue extends rsvFinishLog implements Structure.ByValue {}
        public rsvFinishLog() {}
        public rsvFinishLog(Pointer p) { super(p); read(); }


        public NativeLong rsvReqTime;

        public int options;

        public int uid;

        public String rsvId;

        public String name;

        public int numReses;

        public Pointer /* rsvRes.ByReference */ alloc;

        public String timeWindow;

        public NativeLong duration;

        public String creator;
    }



    public static class cpuProfileLog extends Structure {
        public static class ByReference extends cpuProfileLog implements Structure.ByReference {}
        public static class ByValue extends cpuProfileLog implements Structure.ByValue {}
        public cpuProfileLog() {}
        public cpuProfileLog(Pointer p) { super(p); read(); }


        public byte[] servicePartition = new byte[MAX_LSB_NAME_LEN];

        public int slotsRequired;

        public int slotsAllocated;

        public int slotsBorrowed;

        public int slotsLent;
    }



    public static class jobResizeNotifyStartLog extends Structure {
        public static class ByReference extends jobResizeNotifyStartLog implements Structure.ByReference {}
        public static class ByValue extends jobResizeNotifyStartLog implements Structure.ByValue {}
        public jobResizeNotifyStartLog() {}
        public jobResizeNotifyStartLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int idx;

        public int notifyId;

        public int numResizeHosts;

        public Pointer resizeHosts;
    }



    public static class jobResizeNotifyAcceptLog extends Structure {
        public static class ByReference extends jobResizeNotifyAcceptLog implements Structure.ByReference {}
        public static class ByValue extends jobResizeNotifyAcceptLog implements Structure.ByValue {}
        public jobResizeNotifyAcceptLog() {}
        public jobResizeNotifyAcceptLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int idx;

        public int notifyId;

        public int resizeNotifyCmdPid;

        public int resizeNotifyCmdPGid;

        public int status;
    }



    public static class jobResizeNotifyDoneLog extends Structure {
        public static class ByReference extends jobResizeNotifyDoneLog implements Structure.ByReference {}
        public static class ByValue extends jobResizeNotifyDoneLog implements Structure.ByValue {}
        public jobResizeNotifyDoneLog() {}
        public jobResizeNotifyDoneLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int idx;

        public int notifyId;

        public int status;
    }



    public static class jobResizeReleaseLog extends Structure {
        public static class ByReference extends jobResizeReleaseLog implements Structure.ByReference {}
        public static class ByValue extends jobResizeReleaseLog implements Structure.ByValue {}
        public jobResizeReleaseLog() {}
        public jobResizeReleaseLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int idx;

        public int reqId;

        public int options;

        public int userId;

        public String userName;

        public String resizeNotifyCmd;

        public int numResizeHosts;

        public Pointer resizeHosts;
    }



    public static class jobResizeCancelLog extends Structure {
        public static class ByReference extends jobResizeCancelLog implements Structure.ByReference {}
        public static class ByValue extends jobResizeCancelLog implements Structure.ByValue {}
        public jobResizeCancelLog() {}
        public jobResizeCancelLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int idx;

        public int userId;

        public String userName;
    }



    public static class jobRunRusageLog extends Structure {
        public static class ByReference extends jobRunRusageLog implements Structure.ByReference {}
        public static class ByValue extends jobRunRusageLog implements Structure.ByValue {}
        public jobRunRusageLog() {}
        public jobRunRusageLog(Pointer p) { super(p); read(); }


        public int jobid;

        public int idx;

        public LibLsf.jRusage jrusage;
    }



    public static class slaLog extends Structure {
        public static class ByReference extends slaLog implements Structure.ByReference {}
        public static class ByValue extends slaLog implements Structure.ByValue {}
        public slaLog() {}
        public slaLog(Pointer p) { super(p); read(); }


        public String name;

        public String consumer;

        public int goaltype;

        public int state;

        public int optimum;

        public int[] counters = new int[NUM_JGRP_COUNTERS];
    }



    public static class perfmonLogInfo extends Structure {
        public static class ByReference extends perfmonLogInfo implements Structure.ByReference {}
        public static class ByValue extends perfmonLogInfo implements Structure.ByValue {}
        public perfmonLogInfo() {}
        public perfmonLogInfo(Pointer p) { super(p); read(); }


        public int samplePeriod;

        public IntByReference metrics;

        public NativeLong startTime;

        public NativeLong logTime;
    }



    public static class perfmonLog extends Structure {
        public static class ByReference extends perfmonLog implements Structure.ByReference {}
        public static class ByValue extends perfmonLog implements Structure.ByValue {}
        public perfmonLog() {}
        public perfmonLog(Pointer p) { super(p); read(); }


        public int samplePeriod;

        public int totalQueries;

        public int jobQuries;

        public int queueQuries;

        public int hostQuries;

        public int submissionRequest;

        public int jobSubmitted;

        public int dispatchedjobs;

        public int jobcompleted;

        public int jobMCSend;

        public int jobMCReceive;

        public NativeLong startTime;
    }



    public static class taskFinishLog extends Structure {
        public static class ByReference extends taskFinishLog implements Structure.ByReference {}
        public static class ByValue extends taskFinishLog implements Structure.ByValue {}
        public taskFinishLog() {}
        public taskFinishLog(Pointer p) { super(p); read(); }


        public jobFinishLog jobFinishLog;

        public int taskId;

        public int taskIdx;

        public String taskName;

        public int taskOptions;

        public int taskExitReason;
    }



    public static class eventEOSLog extends Structure {
        public static class ByReference extends eventEOSLog implements Structure.ByReference {}
        public static class ByValue extends eventEOSLog implements Structure.ByValue {}
        public eventEOSLog() {}
        public eventEOSLog(Pointer p) { super(p); read(); }


        public int eos;
    }



    public static class jobResizeLog extends Structure {
        public static class ByReference extends jobResizeLog implements Structure.ByReference {}
        public static class ByValue extends jobResizeLog implements Structure.ByValue {}
        public jobResizeLog() {}
        public jobResizeLog(Pointer p) { super(p); read(); }


        public int jobId;

        public int idx;

        public NativeLong startTime;

        public int userId;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];

        public int resizeType;

        public NativeLong lastResizeStartTime;

        public NativeLong lastResizeFinishTime;

        public int numExecHosts;

        public Pointer execHosts;

        public int numResizeHosts;

        public Pointer resizeHosts;
    }



    public static class eventLog extends Union {
        public jobNewLog jobNewLog;

        public jobStartLog jobStartLog;

        public jobStatusLog jobStatusLog;

        public sbdJobStatusLog sbdJobStatusLog;

        public jobSwitchLog jobSwitchLog;

        public jobMoveLog jobMoveLog;

        public queueCtrlLog queueCtrlLog;

        public newDebugLog newDebugLog;

        public hostCtrlLog hostCtrlLog;

        public mbdStartLog mbdStartLog;

        public mbdDieLog mbdDieLog;

        public unfulfillLog unfulfillLog;

        public jobFinishLog jobFinishLog;

        public loadIndexLog loadIndexLog;

        public migLog migLog;

        public calendarLog calendarLog;

        public jobForwardLog jobForwardLog;

        public jobAcceptLog jobAcceptLog;

        public statusAckLog statusAckLog;

        public signalLog signalLog;

        public jobExecuteLog jobExecuteLog;

        public jobMsgLog jobMsgLog;

        public jobMsgAckLog jobMsgAckLog;

        public jobRequeueLog jobRequeueLog;

        public chkpntLog chkpntLog;

        public sigactLog sigactLog;

        public jobOccupyReqLog jobOccupyReqLog;

        public jobVacatedLog jobVacatedLog;

        public jobStartAcceptLog jobStartAcceptLog;

        public jobCleanLog jobCleanLog;

        public jobExceptionLog jobExceptionLog;

        public jgrpNewLog jgrpNewLog;

        public jgrpCtrlLog jgrpCtrlLog;

        public jobForceRequestLog jobForceRequestLog;

        public logSwitchLog logSwitchLog;

        public jobModLog jobModLog;

        public jgrpStatusLog jgrpStatusLog;

        public jobAttrSetLog jobAttrSetLog;

        public jobExternalMsgLog jobExternalMsgLog;

        public jobChunkLog jobChunkLog;

        public sbdUnreportedStatusLog sbdUnreportedStatusLog;

        public rsvFinishLog rsvFinishLog;

        public hgCtrlLog hgCtrlLog;

        public cpuProfileLog cpuProfileLog;

        public dataLoggingLog dataLoggingLog;

        public jobRunRusageLog jobRunRusageLog;

        public eventEOSLog eventEOSLog;

        public slaLog slaLog;

        public perfmonLog perfmonLog;

        public taskFinishLog taskFinishLog;

        public jobResizeNotifyStartLog jobResizeNotifyStartLog;

        public jobResizeNotifyAcceptLog jobResizeNotifyAcceptLog;

        public jobResizeNotifyDoneLog jobResizeNotifyDoneLog;

        public jobResizeReleaseLog jobResizeReleaseLog;

        public jobResizeCancelLog jobResizeCancelLog;

        public jobResizeLog jobResizeLog;


    }




    public static class eventRec extends Structure {
        public static class ByReference extends eventRec implements Structure.ByReference {}
        public static class ByValue extends eventRec implements Structure.ByValue {}
        public eventRec() {}
        public eventRec(Pointer p) { super(p); read(); }


        public byte[] version = new byte[MAX_VERSION_LEN];

        public int type;

        public NativeLong eventTime;

        public eventLog eventLog;
    }



    public static class eventLogFile extends Structure {
        public static class ByReference extends eventLogFile implements Structure.ByReference {}
        public static class ByValue extends eventLogFile implements Structure.ByValue {}
        public eventLogFile() {}
        public eventLogFile(Pointer p) { super(p); read(); }


        public byte[] eventDir = new byte[LibLsf.MAXFILENAMELEN];

        public NativeLong beginTime, endTime;
    }



    public static class eventLogHandle extends Structure {
        public static class ByReference extends eventLogHandle implements Structure.ByReference {}
        public static class ByValue extends eventLogHandle implements Structure.ByValue {}
        public eventLogHandle() {}
        public eventLogHandle(Pointer p) { super(p); read(); }


        public Pointer fp;

        public byte[] openEventFile = new byte[LibLsf.MAXFILENAMELEN];

        public int curOpenFile;
        public int lastOpenFile;                   /* last open event file number, 0
                  means lsb.events */
    }




    public static final String LSF_JOBIDINDEX_FILENAME = "lsb.events.index";
    public static final String LSF_JOBIDINDEX_FILETAG = "#LSF_JOBID_INDEX_FILE";


    public static class jobIdIndexS extends Structure {
        public static class ByReference extends jobIdIndexS implements Structure.ByReference {}
        public static class ByValue extends jobIdIndexS implements Structure.ByValue {}
        public jobIdIndexS() {}
        public jobIdIndexS(Pointer p) { super(p); read(); }


        public byte[] fileName = new byte[LibLsf.MAXFILENAMELEN];

        public Pointer fp;

        public float version;

        public int totalRows;

        public NativeLong lastUpdate;

        public int curRow;

        public NativeLong timeStamp;

        public long minJobId;

        public long maxJobId;

        public int totalJobIds;

        public IntByReference jobIds;
    }




    public static class sortIntList extends Structure {
        public static class ByReference extends sortIntList implements Structure.ByReference {}
        public static class ByValue extends sortIntList implements Structure.ByValue {}
        public sortIntList() {}
        public sortIntList(Pointer p) { super(p); read(); }

        public int value;

        public sortIntList.ByReference forw;

        public sortIntList.ByReference back;
    }



    public static class nqsStatusReq extends Structure {
        public static class ByReference extends nqsStatusReq implements Structure.ByReference {}
        public static class ByValue extends nqsStatusReq implements Structure.ByValue {}
        public nqsStatusReq() {}
        public nqsStatusReq(Pointer p) { super(p); read(); }

        public long jobId;
        public int opCode;
        public int reportCode;
        public String nqsQueue;
        public int fromUid;
        public String fromUserName;
        public String fromHostName;
        public int idx;
    }



    public static class nqsStatusReply extends Structure {
        public static class ByReference extends nqsStatusReply implements Structure.ByReference {}
        public static class ByValue extends nqsStatusReply implements Structure.ByValue {}
        public nqsStatusReply() {}
        public nqsStatusReply(Pointer p) { super(p); read(); }

        public String orgHost;
        public String orgUser;
        public NativeLong startTime;
        public String jobName;
        public String nqsQueue;
        public String lsbManager;
        public int options;
        public String outFile;
        public String errFile;
    }



    public static final int LSB_MAX_SD_LENGTH = 128;

    public static class lsbMsgHdr extends Structure {
        public static class ByReference extends lsbMsgHdr implements Structure.ByReference {}
        public static class ByValue extends lsbMsgHdr implements Structure.ByValue {}
        public lsbMsgHdr() {}
        public lsbMsgHdr(Pointer p) { super(p); read(); }

        public int usrId;
        public long jobId;
        public int msgId;
        public int type;
        public String src;
        public String dest;
    }



    public static class lsbMsg extends Structure {
        public static class ByReference extends lsbMsg implements Structure.ByReference {}
        public static class ByValue extends lsbMsg implements Structure.ByValue {}
        public lsbMsg() {}
        public lsbMsg(Pointer p) { super(p); read(); }

        public lsbMsgHdr.ByReference header;
        public String msg;
    }




    public static final int CONF_NO_CHECK = 0x00;
    public static final int CONF_CHECK = 0x01;
    public static final int CONF_EXPAND = 0X02;
    public static final int CONF_RETURN_HOSTSPEC = 0X04;
    public static final int CONF_NO_EXPAND = 0X08;
    public static final int CONF_HAS_CU = 0X10;

    public static class paramConf extends Structure {
        public static class ByReference extends paramConf implements Structure.ByReference {}
        public static class ByValue extends paramConf implements Structure.ByValue {}
        public paramConf() {}
        public paramConf(Pointer p) { super(p); read(); }

        public parameterInfo.ByReference param;
    }



    public static class userConf extends Structure {
        public static class ByReference extends userConf implements Structure.ByReference {}
        public static class ByValue extends userConf implements Structure.ByValue {}
        public userConf() {}
        public userConf(Pointer p) { super(p); read(); }

        public int numUgroups;
        public Pointer /* groupInfoEnt.ByReference */ ugroups;
        public int numUsers;
        public Pointer /* userInfoEnt.ByReference */ users;
        public int numUserEquivalent;
        public Pointer /* userEquivalentInfoEnt.ByReference */ userEquivalent;
        public int numUserMapping;
        public Pointer /* userMappingInfoEnt.ByReference */ userMapping;
    }



    public static class hostConf extends Structure {
        public static class ByReference extends hostConf implements Structure.ByReference {}
        public static class ByValue extends hostConf implements Structure.ByValue {}
        public hostConf() {}
        public hostConf(Pointer p) { super(p); read(); }

        public int numHosts;
        public Pointer /* hostInfoEnt.ByReference */ hosts;
        public int numHparts;
        public Pointer /* hostPartInfoEnt.ByReference */ hparts;
        public int numHgroups;
        public Pointer /* groupInfoEnt.ByReference */ hgroups;
    }



    public static class lsbSharedResourceInstance extends Structure {
        public static class ByReference extends lsbSharedResourceInstance implements Structure.ByReference {}
        public static class ByValue extends lsbSharedResourceInstance implements Structure.ByValue {}
        public lsbSharedResourceInstance() {}
        public lsbSharedResourceInstance(Pointer p) { super(p); read(); }


        public String totalValue;

        public String rsvValue;

        public int nHosts;

        public Pointer hostList;
    }



    public static class lsbSharedResourceInfo extends Structure {
        public static class ByReference extends lsbSharedResourceInfo implements Structure.ByReference {}
        public static class ByValue extends lsbSharedResourceInfo implements Structure.ByValue {}
        public lsbSharedResourceInfo() {}
        public lsbSharedResourceInfo(Pointer p) { super(p); read(); }


        public String resourceName;

        public int nInstances;

        public Pointer /* lsbSharedResourceInstance.ByReference */ instances;
    }



    public static class queueConf extends Structure {
        public static class ByReference extends queueConf implements Structure.ByReference {}
        public static class ByValue extends queueConf implements Structure.ByValue {}
        public queueConf() {}
        public queueConf(Pointer p) { super(p); read(); }

        public int numQueues;
        public Pointer /* queueInfoEnt.ByReference */ queues;
    }



    public static class frameElementInfo extends Structure {
        public static class ByReference extends frameElementInfo implements Structure.ByReference {}
        public static class ByValue extends frameElementInfo implements Structure.ByValue {}
        public frameElementInfo() {}
        public frameElementInfo(Pointer p) { super(p); read(); }


        public int jobindex;

        public int jobState;

        public int start;

        public int end;

        public int step;

        public int chunk;
    }



    public static class frameJobInfo extends Structure {
        public static class ByReference extends frameJobInfo implements Structure.ByReference {}
        public static class ByValue extends frameJobInfo implements Structure.ByValue {}
        public frameJobInfo() {}
        public frameJobInfo(Pointer p) { super(p); read(); }


        public long jobGid;

        public int maxJob;

        public byte[] userName = new byte[MAX_LSB_NAME_LEN];

        public byte[] jobName = new byte[LibLsf.MAXLINELEN];

        public frameElementInfo.ByReference frameElementPtr;
    }



    public static class nqsRusageReq extends Structure {
        public static class ByReference extends nqsRusageReq implements Structure.ByReference {}
        public static class ByValue extends nqsRusageReq implements Structure.ByValue {}
        public nqsRusageReq() {}
        public nqsRusageReq(Pointer p) { super(p); read(); }

        public long jobId;
        public int mem;
        public float cpuTime;
    }



    public static class nqsRusageReply extends Structure {
        public static class ByReference extends nqsRusageReply implements Structure.ByReference {}
        public static class ByValue extends nqsRusageReply implements Structure.ByValue {}
        public nqsRusageReply() {}
        public nqsRusageReply(Pointer p) { super(p); read(); }

        public int status;
    }





    public static class _rsvEventInfo_prePost_t extends Structure {
        public static class ByReference extends _rsvEventInfo_prePost_t implements Structure.ByReference {}
        public static class ByValue extends _rsvEventInfo_prePost_t implements Structure.ByValue {}
        public _rsvEventInfo_prePost_t() {}
        public _rsvEventInfo_prePost_t(Pointer p) { super(p); read(); }

        public int shift;
    }



    public static final int RSV_EXECEVENTTYPE_PRE = 1;
    public static final int RSV_EXECEVENTTYPE_POST = 2;

    public static final String RSV_EXECEVENTNAME_PRE = "pre";
    public static final String RSV_EXECEVENTNAME_POST = "post";

    public static class _rsvExecEvent_t extends Structure {
        public static class ByReference extends _rsvExecEvent_t implements Structure.ByReference {}
        public static class ByValue extends _rsvExecEvent_t implements Structure.ByValue {}
        public _rsvExecEvent_t() {}
        public _rsvExecEvent_t(Pointer p) { super(p); read(); }


        public int type;

        public int infoAttached;

        public Pointer info;
    }



    public static class _rsvExecCmd_t extends Structure {
        public static class ByReference extends _rsvExecCmd_t implements Structure.ByReference {}
        public static class ByValue extends _rsvExecCmd_t implements Structure.ByValue {}
        public _rsvExecCmd_t() {}
        public _rsvExecCmd_t(Pointer p) { super(p); read(); }


        public String path;

        public int numEvents;

        public Pointer /* _rsvExecEvent_t.ByReference */ events;
    }




    public static final int RSV_OPTION_USER = 0x0001;

    public static final int RSV_OPTION_GROUP = 0x0002;

    public static final int RSV_OPTION_SYSTEM = 0x0004;

    public static final int RSV_OPTION_RECUR = 0x0008;

    public static final int RSV_OPTION_RESREQ = 0x0010;

    public static final int RSV_OPTION_HOST = 0x0020;

    public static final int RSV_OPTION_OPEN = 0x0040;

    public static final int RSV_OPTION_DELETE = 0x0080;

    public static final int RSV_OPTION_CLOSED = 0x0100;

    public static final int RSV_OPTION_EXEC = 0x0200;

    public static final int RSV_OPTION_RMEXEC = 0x0400;

    public static final int RSV_OPTION_NEXTINSTANCE = 0x0800;

    public static final int RSV_OPTION_DISABLE = 0x1000;

    public static final int RSV_OPTION_ADDHOST = 0x2000;

    public static final int RSV_OPTION_RMHOST = 0x4000;

    public static final int RSV_OPTION_DESCRIPTION = 0x8000;

    public static final int RSV_OPTION_TWMOD = 0x10000;

    public static final int RSV_OPTION_SWITCHOPENCLOSE = 0x20000;

    public static final int RSV_OPTION_USERMOD = 0x40000;

    public static final int RSV_OPTION_RSVNAME = 0x80000;

    public static final int RSV_OPTION_EXPIRED = 0x100000;

    public static class addRsvRequest extends Structure {
        public static class ByReference extends addRsvRequest implements Structure.ByReference {}
        public static class ByValue extends addRsvRequest implements Structure.ByValue {}
        public addRsvRequest() {}
        public addRsvRequest(Pointer p) { super(p); read(); }


        public int options;

        public String name;

        public int minNumProcs;

        public int maxNumProcs;

        //struct procRange;

        public int numAskedHosts;

        public Pointer askedHosts;

        public String resReq;

        public String timeWindow;

        public _rsvExecCmd_t.ByReference execCmd;

        public String desc;

        public String rsvName;
    }



    public static class rmRsvRequest extends Structure {
        public static class ByReference extends rmRsvRequest implements Structure.ByReference {}
        public static class ByValue extends rmRsvRequest implements Structure.ByValue {}
        public rmRsvRequest() {}
        public rmRsvRequest(Pointer p) { super(p); read(); }


        public String rsvId;
    }



    public static class modRsvRequest extends Structure {
        public static class ByReference extends modRsvRequest implements Structure.ByReference {}
        public static class ByValue extends modRsvRequest implements Structure.ByValue {}
        public modRsvRequest() {}
        public modRsvRequest(Pointer p) { super(p); read(); }


        public String rsvId;

        public addRsvRequest fieldsFromAddReq;

        public String disabledDuration;
    }



    public static class hostRsvInfoEnt extends Structure {
        public static class ByReference extends hostRsvInfoEnt implements Structure.ByReference {}
        public static class ByValue extends hostRsvInfoEnt implements Structure.ByValue {}
        public hostRsvInfoEnt() {}
        public hostRsvInfoEnt(Pointer p) { super(p); read(); }


        public String host;

        public int numCPUs;

        public int numSlots;

        public int numRsvProcs;

        public int numusedRsvProcs;

        public int numUsedProcs;
    }



    public static class rsvInfoEnt extends Structure {
        public static class ByReference extends rsvInfoEnt implements Structure.ByReference {}
        public static class ByValue extends rsvInfoEnt implements Structure.ByValue {}
        public rsvInfoEnt() {}
        public rsvInfoEnt(Pointer p) { super(p); read(); }


        public int options;

        public String rsvId;

        public String name;

        public int numRsvHosts;

        public Pointer /* hostRsvInfoEnt.ByReference */ rsvHosts;

        public String timeWindow;

        public int numRsvJobs;

        public LongByReference jobIds;

        public IntByReference jobStatus;

        public String desc;

        public Pointer disabledDurations;

        public int state;

        public String nextInstance;

        public String creator;
    }




    public static class slotInfoRequest extends Structure {
        public static class ByReference extends slotInfoRequest implements Structure.ByReference {}
        public static class ByValue extends slotInfoRequest implements Structure.ByValue {}
        public slotInfoRequest() {}
        public slotInfoRequest(Pointer p) { super(p); read(); }


        public static int SLOT_OPTION_RESREQ = 0X001;

        public int options;

        public String resReq;
    }




    public static class SRInfoEnt extends Structure {
        public static class ByReference extends SRInfoEnt implements Structure.ByReference {}
        public static class ByValue extends SRInfoEnt implements Structure.ByValue {}
        public SRInfoEnt() {}
        public SRInfoEnt(Pointer p) { super(p); read(); }


        public int numReserved;

        public NativeLong predictedStartTime;
    }



    public static class hostSRInfoEnt extends Structure {
        public static class ByReference extends hostSRInfoEnt implements Structure.ByReference {}
        public static class ByValue extends hostSRInfoEnt implements Structure.ByValue {}
        public hostSRInfoEnt() {}
        public hostSRInfoEnt(Pointer p) { super(p); read(); }

        public String host;
        public int hStatus;
        public int userJobLimit;
        public int maxJobs;
        public int numJobs;
        public int numRUN;
        public int numSSUSP;
        public int numUSUSP;
        public int numRESERVE;
        public int numSR;
        public Pointer /* SRInfoEnt.ByReference */ SRInfo;
    }



    public static class slotInfoReply extends Structure {
        public static class ByReference extends slotInfoReply implements Structure.ByReference {}
        public static class ByValue extends slotInfoReply implements Structure.ByValue {}
        public slotInfoReply() {}
        public slotInfoReply(Pointer p) { super(p); read(); }


        public NativeLong masterTime;
        public int numHosts;
        public Pointer /* hostSRInfoEnt.ByReference */ hostInfo;
        public int numAR;
        public Pointer /* rsvInfoEnt.ByReference */ ARInfo;
    }






    public static final int LSB_RSRC_LIMIT_TYPE_SLOTS = 0;
    public static final int LSB_RSRC_LIMIT_TYPE_SLOT_PERPSR = 1;
    public static final int LSB_RSRC_LIMIT_TYPE_MEM = 2;
    public static final int LSB_RSRC_LIMIT_TYPE_MEM_PERCENT = 3;
    public static final int LSB_RSRC_LIMIT_TYPE_SWP = 4;
    public static final int LSB_RSRC_LIMIT_TYPE_SWP_PERCENT = 5;
    public static final int LSB_RSRC_LIMIT_TYPE_TMP = 6;
    public static final int LSB_RSRC_LIMIT_TYPE_TMP_PERCENT = 7;
    public static final int LSB_RSRC_LIMIT_TYPE_JOBS = 8;

    public static final int LSB_RSRC_LIMIT_TYPE_EXT_RSRC = 9;

    public static interface consumerType {
        public static final int LIMIT_QUEUES = 1;

        public static final int LIMIT_PER_QUEUE = 2;

        public static final int LIMIT_USERS = 3;

        public static final int LIMIT_PER_USER = 4;

        public static final int LIMIT_HOSTS = 5;

        public static final int LIMIT_PER_HOST = 6;

        public static final int LIMIT_PROJECTS = 7;

        public static final int LIMIT_PER_PROJECT = 8;
    }



    public static class _limitConsumer extends Structure {
        public static class ByReference extends _limitConsumer implements Structure.ByReference {}
        public static class ByValue extends _limitConsumer implements Structure.ByValue {}
        public _limitConsumer() {}
        public _limitConsumer(Pointer p) { super(p); read(); }


        public int type;

        public String name;
    }



    public static class _limitResource extends Structure {
        public static class ByReference extends _limitResource implements Structure.ByReference {}
        public static class ByValue extends _limitResource implements Structure.ByValue {}
        public _limitResource() {}
        public _limitResource(Pointer p) { super(p); read(); }


        public String name;

        public int type;

        public float val;
    }



    public static class _limitInfoReq extends Structure {
        public static class ByReference extends _limitInfoReq implements Structure.ByReference {}
        public static class ByValue extends _limitInfoReq implements Structure.ByValue {}
        public _limitInfoReq() {}
        public _limitInfoReq(Pointer p) { super(p); read(); }


        public String name;

        public int consumerC;

        public Pointer /* _limitConsumer.ByReference */ consumerV;
    }



    public static class _limitItem extends Structure {
        public static class ByReference extends _limitItem implements Structure.ByReference {}
        public static class ByValue extends _limitItem implements Structure.ByValue {}
        public _limitItem() {}
        public _limitItem(Pointer p) { super(p); read(); }


        public int consumerC;

        public Pointer /* _limitConsumer.ByReference */ consumerV;

        public int resourceC;

        public Pointer /* _limitResource.ByReference */ resourceV;
    }



    public static class _limitInfoEnt extends Structure {
        public static class ByReference extends _limitInfoEnt implements Structure.ByReference {}
        public static class ByValue extends _limitInfoEnt implements Structure.ByValue {}
        public _limitInfoEnt() {}
        public _limitInfoEnt(Pointer p) { super(p); read(); }


        public String name;

        public _limitItem confInfo;

        public int usageC;

        public Pointer /* _limitItem.ByReference */ usageInfo;

    }




    public static final int ADD_THRESHOLD = 1;
    public static final int GET_THRESHOLD = 2;
    public static final int DEL_THRESHOLD = 3;


    public static class thresholdEntry extends Structure {
        public static class ByReference extends thresholdEntry implements Structure.ByReference {}
        public static class ByValue extends thresholdEntry implements Structure.ByValue {}
        public thresholdEntry() {}
        public thresholdEntry(Pointer p) { super(p); read(); }


        public String attr;

        public hostInfoEnt.ByReference hostEntryPtr;
    }



    public static native int lsb_limitInfo(_limitInfoReq req, Pointer limitItemRef, IntByReference size, LibLsf.lsInfo lsInfo);

    public static native void lsb_freeLimitInfoEnt(_limitInfoEnt ent, int size);


    public static final int LSB_RESIZE_REL_NONE = 0x0;

    public static final int LSB_RESIZE_REL_ALL = 0x01;

    public static final int LSB_RESIZE_REL_CANCEL = 0x02;

    public static final int LSB_RESIZE_REL_NO_NOTIFY = 0x04;

    public static class job_resize_release extends Structure {
        public static class ByReference extends job_resize_release implements Structure.ByReference {}
        public static class ByValue extends job_resize_release implements Structure.ByValue {}
        public job_resize_release() {}
        public job_resize_release(Pointer p) { super(p); read(); }


        public long jobId;

        public int options;

        public int nHosts;

        public Pointer hosts;

        public IntByReference slots;

        public String notifyCmd;
    }



    public static class job_resize_request extends Structure {
        public static class ByReference extends job_resize_request implements Structure.ByReference {}
        public static class ByValue extends job_resize_request implements Structure.ByValue {}
        public job_resize_request() {}
        public job_resize_request(Pointer p) { super(p); read(); }

        public long jobId;
        public int options;

        public int nHosts;

        public Pointer hosts;

        public String notifyCmd;
    }







    public static final int QUERY_DEPEND_RECURSIVELY = 0x1;

    public static final int QUERY_DEPEND_DETAIL = 0x2;

    public static final int QUERY_DEPEND_UNSATISFIED = 0x4;

    public static final int QUERY_DEPEND_CHILD = 0x8;


    public static class jobDepRequest extends Structure {
        public static class ByReference extends jobDepRequest implements Structure.ByReference {}
        public static class ByValue extends jobDepRequest implements Structure.ByValue {}
        public jobDepRequest() {}
        public jobDepRequest(Pointer p) { super(p); read(); }

        public long jobId;

        public int options;

        public int level;
    }




    public static class queriedJobs extends Structure {
        public static class ByReference extends queriedJobs implements Structure.ByReference {}
        public static class ByValue extends queriedJobs implements Structure.ByValue {}
        public queriedJobs() {}
        public queriedJobs(Pointer p) { super(p); read(); }


        public long jobId;

        public String dependcondition;

        public int satisfied;
    }




    public static final int JOB_HAS_DEPENDENCY = 0x1;

    public static final int JOB_HAS_INDIVIDUAL_CONDITION = 0x2;


    public static class dependJobs extends Structure {
        public static class ByReference extends dependJobs implements Structure.ByReference {}
        public static class ByValue extends dependJobs implements Structure.ByValue {}
        public dependJobs() {}
        public dependJobs(Pointer p) { super(p); read(); }

        public long jobId;

        public String jobname;

        public int level;

        public int jobstatus;

        public int hasDependency;

        public String condition;

        public int satisfied;

        public long depJobId;
    }




    public static class jobDependInfo extends Structure {
        public static class ByReference extends jobDependInfo implements Structure.ByReference {}
        public static class ByValue extends jobDependInfo implements Structure.ByValue {}
        public jobDependInfo() {}
        public jobDependInfo(Pointer p) { super(p); read(); }


        public int options;

        public int numQueriedJobs;

        public Pointer /* queriedJobs.ByReference */ queriedJobs;

        public int level;

        public int numJobs;

        public Pointer /* dependJobs.ByReference */ depJobs;
    }







    public static boolean IS_PEND(int s) {
        return (JNAUtils.toBoolean((s) & JOB_STAT_PEND) || JNAUtils.toBoolean((s) & JOB_STAT_PSUSP));
    }


    public static boolean IS_START(int s) {
        return (JNAUtils.toBoolean((s) & JOB_STAT_RUN) || JNAUtils.toBoolean((s) & JOB_STAT_SSUSP) || JNAUtils.toBoolean((s) & JOB_STAT_USUSP));
    }

    public static boolean IS_FINISH(int s) {
        return (JNAUtils.toBoolean((s) & JOB_STAT_DONE) || JNAUtils.toBoolean((s) & JOB_STAT_EXIT));
    }

    public static boolean IS_SUSP(int s) {
        return (JNAUtils.toBoolean((s) & JOB_STAT_PSUSP) || JNAUtils.toBoolean((s) & JOB_STAT_SSUSP) || JNAUtils.toBoolean((s) & JOB_STAT_USUSP));
    }


    public static boolean IS_POST_DONE(int s) {
        return (((s) & JOB_STAT_PDONE) == JOB_STAT_PDONE);
    }

    public static boolean IS_POST_ERR(int s) {
        return (((s) & JOB_STAT_PERR) == JOB_STAT_PERR);
    }

    public static boolean IS_POST_FINISH(int s) {
        return (IS_POST_DONE(s) || IS_POST_ERR(s));
    }


    public static int lsberrno() {
        return lsb_errno().getValue();
    }




    //public int lsb_mbd_version;

    public static final int PRINT_SHORT_NAMELIST = 0x01;
    public static final int PRINT_LONG_NAMELIST = 0x02;
    public static final int PRINT_MCPU_HOSTS = 0x04;

    public static class nameList extends Structure {
        public static class ByReference extends nameList implements Structure.ByReference {}
        public static class ByValue extends nameList implements Structure.ByValue {}
        public nameList() {}
        public nameList(Pointer p) { super(p); read(); }


        public int listSize;

        public Pointer names;

        public IntByReference counter;
    }



    public static native nameList.ByReference lsb_parseShortStr(String string1, int int1);

    public static native nameList.ByReference lsb_parseLongStr(String string1);

    public static native String lsb_printNameList(nameList namelist1, int int1);

    public static native nameList.ByReference lsb_compressStrList(Pointer stringArray1, int int1);

    public static native String lsb_splitName(String string1, IntByReference int1);

    public static native IntByReference lsb_errno();



    public static native paramConf.ByReference lsb_readparam(LibLsf.lsConf lsConf1);

    public static native userConf.ByReference lsb_readuser(LibLsf.lsConf lsConf1, int int1, LibLsf.clusterConf clusterConf1);

    public static native userConf.ByReference lsb_readuser_ex(LibLsf.lsConf lsConf1, int int1, LibLsf.clusterConf clusterConf1, LibLsf.sharedConf sharedConf1);

    public static native hostConf.ByReference lsb_readhost(LibLsf.lsConf lsConf1, LibLsf.lsInfo lsInfo1, int int1, LibLsf.clusterConf clusterConf1);

    public static native queueConf.ByReference lsb_readqueue(LibLsf.lsConf lsConf1, LibLsf.lsInfo lsInfo1, int int1, LibLsf.sharedConf sharedConf1, LibLsf.clusterConf clusterConf1);

    public static native void updateClusterConf(LibLsf.clusterConf clusterConf1);


    public static native hostPartInfoEnt.ByReference lsb_hostpartinfo(Pointer stringArray1, IntByReference numHostHosts);

    public static native int lsb_init(String appName);

    public static native int sch_lsb_init();

    public static native int lsb_openjobinfo(long jobId, String jobName, String userName, String queueName, String hostName, int options);

    public static native jobInfoHead.ByReference lsb_openjobinfo_a(long jobId, String jobName, String userName, String queueName, String hostName, int options);

    public static native jobInfoHeadExt.ByReference lsb_openjobinfo_a_ext(long jobId, String jobName, String userName, String queueName, String hostName, int options);

    public static native jobInfoHeadExt.ByReference lsb_openjobinfo_req(jobInfoReq req);

    public static native int lsb_queryjobinfo(int int1, NativeLongByReference long1, String string1);

    public static native jobInfoEnt.ByReference lsb_fetchjobinfo(IntByReference int1, int int2, NativeLongByReference long1, String string1);

    public static native jobInfoEnt.ByReference lsb_fetchjobinfo_ext(IntByReference int1, int int2, NativeLongByReference long1, String string1, jobInfoHeadExt jobInfoHeadExt);

    public static native jobInfoEnt.ByReference lsb_readjobinfo(IntByReference more);

    public static native long lsb_submit(submit jobSubReq, submitReply jobSubReply);

    public static native jobInfoEnt.ByReference lsb_readjobinfo_cond(IntByReference more, jobInfoHeadExt jInfoHExt);


    public static native void lsb_closejobinfo();

    public static native int lsb_hostcontrol(hostCtrlReq req);

    public static native int lsb_hghostcontrol(hgCtrlReq hostCtrlReq1, hgCtrlReply reply);

    public static native queueInfoEnt.ByReference lsb_queueinfo(Pointer queues, IntByReference numQueues, String hosts, String user, int options);

    public static native int lsb_reconfig(mbdCtrlReq req);

    public static native int lsb_signaljob(long jobId, int sigValue);

    public static native int lsb_killbulkjobs(signalBulkJobs s);

    public static native int lsb_msgjob(long long1, String s);

    public static native int lsb_chkpntjob(long jobId, NativeLong period, int options);

    public static native int lsb_deletejob(long jobId, int times, int options);

    public static native int lsb_forcekilljob(long jobId);

    public static native int lsb_submitframe(submit jobSubReq, String frameExp, submitReply jobSubReply);

    public static native int lsb_requeuejob(jobrequeue reqPtr);

    public static native String lsb_sysmsg();

    public static native void lsb_perror(String usrMsg);

    public static native void lsb_errorByCmd(String string1, String string2, int int1);

    public static native String lsb_sperror(String string1);

    public static native String lsb_peekjob(long jobId);

    public static native int lsb_mig(submig mig, IntByReference badHostIdx);

    public static native clusterInfoEnt.ByReference lsb_clusterinfo(IntByReference int1, Pointer stringArray1, int int2);

    public static native clusterInfoEntEx.ByReference lsb_clusterinfoEx(IntByReference int1, Pointer stringArray1, int int2);

    public static native hostInfoEnt.ByReference lsb_hostinfo(Pointer hosts, IntByReference numHosts);


    public static native hostInfoEnt.ByReference lsb_hostinfo_ex(Pointer resReq, IntByReference numHosts, String string1, int options);

    public static native condHostInfoEnt.ByReference lsb_hostinfo_cond(Pointer hosts, IntByReference numHosts, String resReq, int options);


    public static native int lsb_movejob(long jobId, IntByReference opCode, int position);

    public static native int lsb_switchjob(long jobId, String queue);

    public static native int lsb_queuecontrol(queueCtrlReq req);

    public static native userInfoEnt.ByReference lsb_userinfo(Pointer users, IntByReference numUsers);

    public static native groupInfoEnt.ByReference lsb_hostgrpinfo(Pointer groups, IntByReference numGroups, int options);

    public static native groupInfoEnt.ByReference lsb_usergrpinfo(Pointer groups, IntByReference numGroups, int options);

    public static native parameterInfo.ByReference lsb_parameterinfo(Pointer names, IntByReference numUsers, int options);

    public static native long lsb_modify(submit jobSubReq, submitReply jobSubReply, long jobId);

    public static native FloatByReference getCpuFactor(String string1, int int1);

    public static native String lsb_suspreason(int reasons, int subreasons, loadIndexLog ld);

    public static native String lsb_pendreason(int numReasons, IntByReference rsTb, jobInfoHead jInfoH, loadIndexLog ld, int clusterId);

    public static native calendarInfoEnt.ByReference lsb_calendarinfo(Pointer calendars, IntByReference numCalendars, String user);

    public static native int lsb_calExprOccs(String string1, int int1, int int2, String string2, PointerByReference int3);

    public static native int lsb_calendarop(int oper, int numNames, Pointer names, String desc, String calExpr, int options, String badStr);

    public static native int lsb_puteventrec(Pointer logPtr, eventRec log_fp);

    public static native int lsb_puteventrecRaw(Pointer pointer1, eventRec eventRec1, String string1);

    public static native eventRec.ByReference lsb_geteventrec(Pointer log_fp, IntByReference lineNum);

    public static native eventRec.ByReference lsb_geteventrec_decrypt(Pointer pointer1, IntByReference int1);

    public static native eventRec.ByReference lsb_geteventrecord(Pointer pointer1, IntByReference int1);

    public static native eventRec.ByReference lsb_geteventrecordEx(Pointer pointer1, IntByReference int1, Pointer stringArray1);

    public static native eventRec.ByReference lsb_getnewjob_from_string(String string1);

    public static native eventInfoEnt.ByReference lsb_eventinfo(Pointer stringArray1, IntByReference int1, String string1);

    public static native Pointer lsb_sharedresourceinfo(Pointer resources, IntByReference numResources, String hostName, int options);


    public static native int lsb_geteventrecbyline(String line, eventRec logRec);

    public static int lsb_connect(int a) {
        return lsb_rcvconnect();
    }

    public static native int lsb_rcvconnect();

    public static native int lsb_sndmsg(lsbMsgHdr lsbMsgHdr1, String string1, int int1);

    public static native int lsb_rcvmsg(lsbMsgHdr lsbMsgHdr1, Pointer stringArray1, int int1);

    public static native int lsb_runjob(runJobRequest runJobRequest);


    public static native int lsb_addjgrp(jgrpAdd jgrpAdd1, Pointer jgrpReply1);

    public static native int lsb_modjgrp(jgrpMod jgrpMod1, Pointer jgrpReply1);

    public static native int lsb_holdjgrp(String string1, int int1, Pointer jgrpReply1);

    public static native int lsb_reljgrp(String string1, int int1, Pointer jgrpReply1);

    public static native int lsb_deljgrp(String string1, int int1, Pointer jgrpReply1);

    public static native int lsb_deljgrp_ext(jgrpCtrl jgrpCtrl1, Pointer jgrpReply1);

    public static native jgrp.ByReference lsb_listjgrp(IntByReference int1);

    public static native serviceClass.ByReference lsb_serviceClassInfo(IntByReference int1);


    public static native appInfoEnt.ByReference lsb_appInfo(IntByReference int1);

    public static native void lsb_freeAppInfoEnts(int int1, appInfoEnt appInfoEnt1);


    public static native String lsb_jobid2str(long long1);

    public static native String lsb_jobid2str_r(long long1, byte[] byte1);

    public static native String lsb_jobidinstr(long long1);

    public static native void jobId32To64(LongByReference long1, int int1, int int2);

    public static native void jobId64To32(long long1, IntByReference int1, IntByReference int2);

    public static native int lsb_setjobattr(int int1, jobAttrInfoEnt jobAttrInfoEnt1);


    public static native long lsb_rexecv(int int1, Pointer stringArray1, Pointer stringArray2, IntByReference int2, int int3);


    public static interface lsb_catchCallback extends Callback {
        int invoke(Pointer pointer);
    }

    public static native int lsb_catch(String string1, lsb_catchCallback callback);

    public static native void lsb_throw(String string1, Pointer pointer1);



    public static native int lsb_postjobmsg(jobExternalMsgReq jobExternalMsg, String filename);

    public static native int lsb_readjobmsg(jobExternalMsgReq jobExternalMsg, jobExternalMsgReply jobExternalMsgReply);


    public static native int lsb_bulkJobInfoUpdate(symJobStatusUpdateReqArray symJobStatusUpdateReqArray1, symJobStatusUpdateReplyArray symJobStatusUpdateReplyArray1);


    public static native int lsb_addreservation(addRsvRequest request, String rsvId);

    public static native int lsb_removereservation(String rsvId);


    public static native rsvInfoEnt.ByReference lsb_reservationinfo(String rsvId, IntByReference numEnts, int options);

    public static native int lsb_freeRsvExecCmd(Pointer _rsvExecCmd_tArray1);

    public static native _rsvExecCmd_t.ByReference lsb_dupRsvExecCmd(_rsvExecCmd_t _rsvExecCmd_t1);

    public static native int lsb_parseRsvExecOption(String string1, Pointer _rsvExecCmd_tArray1);


    public static native int lsb_modreservation(modRsvRequest request);




    public static class jobExtschInfoReq extends Structure {
        public static class ByReference extends jobExtschInfoReq implements Structure.ByReference {}
        public static class ByValue extends jobExtschInfoReq implements Structure.ByValue {}
        public jobExtschInfoReq() {}
        public jobExtschInfoReq(Pointer p) { super(p); read(); }

        public int qCnt;
        public Pointer queues;
    }



    public static class jobExtschInfo extends Structure {
        public static class ByReference extends jobExtschInfo implements Structure.ByReference {}
        public static class ByValue extends jobExtschInfo implements Structure.ByValue {}
        public jobExtschInfo() {}
        public jobExtschInfo(Pointer p) { super(p); read(); }

        public long jobId;
        public int status;
        public NativeLong jRusageUpdateTime;
        public LibLsf.jRusage runRusage;
    }



    public static class jobExtschInfoReply extends Structure {
        public static class ByReference extends jobExtschInfoReply implements Structure.ByReference {}
        public static class ByValue extends jobExtschInfoReply implements Structure.ByValue {}
        public jobExtschInfoReply() {}
        public jobExtschInfoReply(Pointer p) { super(p); read(); }

        public int jobCnt;
        public PointerByReference jobs;
    }



    public static native int getjobinfo4queues(jobExtschInfoReq jobExtschInfoReq1, jobExtschInfoReply jobExtschInfoReply1);

    public static native void free_jobExtschInfoReply(jobExtschInfoReply jobExtschInfoReply1);

    public static native void free_jobExtschInfoReq(jobExtschInfoReq jobExtschInfoReq1);


    public static native String longer_strcpy(String dest, String src);


    public static class diagnoseJobReq extends Structure {
        public static class ByReference extends diagnoseJobReq implements Structure.ByReference {}
        public static class ByValue extends diagnoseJobReq implements Structure.ByValue {}
        public diagnoseJobReq() {}
        public diagnoseJobReq(Pointer p) { super(p); read(); }

        public int jobCnt;
        public LongByReference jobId;
    }



    public static native int lsb_diagnosejob(diagnoseJobReq diagnoseJobReq1);

    public static final int SIM_STATUS_RUN = 0x01;
    public static final int SIM_STATUS_SUSPEND = 0x02;


    public static class simStatusReply extends Structure {
        public static class ByReference extends simStatusReply implements Structure.ByReference {}
        public static class ByValue extends simStatusReply implements Structure.ByValue {}
        public simStatusReply() {}
        public simStatusReply(Pointer p) { super(p); read(); }

        public int simStatus;
        public NativeLong curTime;
    }



    public static native simStatusReply.ByReference lsb_simstatus();

    public static native void free_simStatusReply(simStatusReply simStatusReply1);

    public static final int LSB_HOST_OPTION_EXPORT = 0x1;
    public static final int LSB_HOST_OPTION_EXCEPT = 0x2;
    public static final int LSB_HOST_OPTION_BATCH = 0x4;


    public static final int LSB_HOST_OPTION_CONDENSED = 0x08;


    public static final int RMS_NON_RMS_OPTIONS_ERR = (-1);

    public static final int RMS_NODE_PTILE_ERR = (-2);

    public static final int RMS_RAIL_RAILMASK_ERR = (-3);

    public static final int RMS_NODES_OUT_BOUND_ERR = (-4);

    public static final int RMS_PTILE_OUT_BOUND_ERR = (-5);

    public static final int RMS_RAIL_OUT_BOUND_ERR = (-6);

    public static final int RMS_RAILMASK_OUT_BOUND_ERR = (-7);

    public static final int RMS_NODES_SYNTAX_ERR = (-8);

    public static final int RMS_PTILE_SYNTAX_ERR = (-9);

    public static final int RMS_RAIL_SYNTAX_ERR = (-10);

    public static final int RMS_RAILMASK_SYNTAX_ERR = (-11);

    public static final int RMS_BASE_SYNTAX_ERR = (-12);

    public static final int RMS_BASE_TOO_LONG = (-13);

    public static final int RMS_TOO_MANY_ALLOCTYPE_ERR = (-14);

    public static final int RMS_NO_LSF_EXTSCHED_Y_ERR = (-15);

    public static final int RMS_READ_ENV_ERR = (-20);

    public static final int RMS_MEM_ALLOC_ERR = (-21);

    public static final int RMS_BRACKETS_MISMATCH_ERR = (-22);

    public static interface rmsAllocType_t {
          public static final int RMS_ALLOC_TYPE_UNKNOWN = 0;
          public static final int RMS_ALLOC_TYPE_SLOAD = 1;
          public static final int RMS_ALLOC_TYPE_SNODE = 2;
          public static final int RMS_ALLOC_TYPE_MCONT = 3;
    }



    public static interface rmsTopology_t {
          public static final int RMS_TOPOLOGY_UNKNOWN = 0;
          public static final int RMS_TOPOLOGY_PTILE = 1;
          public static final int RMS_TOPOLOGY_NODES = 2;
    }



    public static interface rmsFlags_t {
          public static final int RMS_FLAGS_UNKNOWN = 0;
          public static final int RMS_FLAGS_RAILS = 1;
          public static final int RMS_FLAGS_RAILMASK = 2;
    }



    public static class rmsextschedoption extends Structure {
        public static class ByReference extends rmsextschedoption implements Structure.ByReference {}
        public static class ByValue extends rmsextschedoption implements Structure.ByValue {}
        public rmsextschedoption() {}
        public rmsextschedoption(Pointer p) { super(p); read(); }

        public /*rmsAllocType_t*/ int alloc_type;
        public /*rmsTopology_t*/ int topology;
        public int topology_value;
        public int set_base;
        public byte[] base = new byte[LibLsf.MAXHOSTNAMELEN];
        public /*rmsFlags_t*/ int flags;
        public int flags_value;
    }



    public static native int parseRmsOptions(String string1, rmsextschedoption rmsextschedoption1, LibLsf.config_param config_param1);

    public static final int MBD_DEF_STREAM_SIZE = (1024 * 1024 * 1024);

    public static final int DEF_MAX_STREAM_FILE_NUMBER = 10;

    public static class lsbStream extends Structure {
        public static class ByReference extends lsbStream implements Structure.ByReference {}
        public static class ByValue extends lsbStream implements Structure.ByValue {}
        public lsbStream() {}
        public lsbStream(Pointer p) { super(p); read(); }

        public static interface trsFunc extends Callback {
            int invoke(String string1);
        }

        public String streamFile;

        public int maxStreamSize;

        public int maxStreamFileNum;

        public int trace;

        public trsFunc trs;
    }



    // NOTE: Not in libbat
    //public static native int lsb_openstream(lsbStream params);

    // NOTE: Not in libbat
    //public static native int lsb_closestream(String config);

    // NOTE: Not in libbat
    //public static native String lsb_streamversion();

    // NOTE: Not in libbat
    //public static native int lsb_writestream(eventRec logPtr);

    // NOTE: Not in libbat
    //public static native eventRec.ByReference lsb_readstream(IntByReference nline);

    // NOTE: Not in libbat
    //public static native eventRec.ByReference lsb_readstreamline(String line);

    public static final int NUM_EXITRATE_TYPES = 4;



    public static final int JOB_EXIT = 0x01;

    public static final int JOB_INIT = 0x02;

    public static final int HPC_INIT = 0x04;

    public static final int JOB_EXIT_NONLSF = 0x08;

    public static class apsFactorInfo extends Structure {
        public static class ByReference extends apsFactorInfo implements Structure.ByReference {}
        public static class ByValue extends apsFactorInfo implements Structure.ByValue {}
        public apsFactorInfo() {}
        public apsFactorInfo(Pointer p) { super(p); read(); }


        public String name;

        public float weight;

        public float limit;

        public int gracePeriod;
    }




    public static final int JGRP_DEL_USER_GROUPS = 0x01;

    public static final int JGRP_DEL_CHILD_GROUPS = 0x02;

    public static final int JGRP_DEL_ALL = 0x04;

    public static native int lsb_getallocFromHostfile(Pointer hostlist, String path);



    public static final int LSF_DJOB_DISABLE_STDIN = 0x01;

    public static final int LSF_DJOB_REPLACE_ENV = 0x02;

    public static final int LSF_DJOB_NOWAIT = 0x04;

    public static final int LSF_DJOB_STDERR_WITH_HOSTNAME = 0x08;

    public static final int LSF_DJOB_STDOUT_WITH_HOSTNAME = 0x10;

    public static final int LSF_DJOB_USE_LOGIN_SHELL = 0x20;

    public static final int LSF_DJOB_USE_BOURNE_SHELL = 0x40;

    public static final int LSF_DJOB_STDERR = 0x80;


    public static native int lsb_launch(Pointer where, Pointer argv, int userOptions, Pointer envp);


    public static native int lsb_getalloc(Pointer hostlist);


    public static native int lsb_resize_cancel(long jobId);

    public static native int lsb_resize_release(job_resize_release req);

    public static native int lsb_resize_request(job_resize_request job_resize_request1);

    public static native jobDependInfo.ByReference lsb_getjobdepinfo(jobDepRequest jobdepReq);



}
