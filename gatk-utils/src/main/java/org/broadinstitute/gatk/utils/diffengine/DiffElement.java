/*
* Copyright 2012-2016 Broad Institute, Inc.
* 
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use,
* copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following
* conditions:
* 
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
* THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package org.broadinstitute.gatk.utils.diffengine;

import org.broadinstitute.gatk.utils.Utils;
import org.broadinstitute.gatk.utils.exceptions.ReviewedGATKException;

/**
 * Created by IntelliJ IDEA.
 * User: depristo
 * Date: 7/4/11
 * Time: 12:55 PM
 *
 * An interface that must be implemented to allow us to calculate differences
 * between structured objects
 */
public class DiffElement {
    public final static DiffElement ROOT = new DiffElement();

    final private String name;
    final private DiffElement parent;
    final private DiffValue value;

    /**
     * For ROOT only
     */
    private DiffElement() {
        this.name = "ROOT";
        this.parent = null;
        this.value = new DiffValue(this, "ROOT");
    }

    public DiffElement(String name, DiffElement parent, DiffValue value) {
        if ( name.equals("ROOT") ) throw new IllegalArgumentException("Cannot use reserved name ROOT");
        this.name = name;
        this.parent = parent;
        this.value = value;
        this.value.setBinding(this);
    }

    public String getName() {
        return name;
    }

    public DiffElement getParent() {
        return parent;
    }

    public DiffValue getValue() {
        return value;
    }

    public boolean isRoot() { return this == ROOT; }

    @Override
    public String toString() {
        return getName() + "=" + getValue().toString();
    }

    public String toString(int offset) {
        return (offset > 0 ? Utils.dupString(' ', offset) : 0) + getName() + "=" + getValue().toString(offset);
    }

    public final String fullyQualifiedName() {
        if ( isRoot() )
            return "";
        else if ( parent.isRoot() )
            return name;
        else
            return parent.fullyQualifiedName() + "." + name;
    }

    public String toOneLineString() {
        return getName() + "=" + getValue().toOneLineString();
    }

    public DiffNode getValueAsNode() {
        if ( getValue().isCompound() )
            return (DiffNode)getValue();
        else
            throw new ReviewedGATKException("Illegal request conversion of a DiffValue into a DiffNode: " + this);
    }

    public int size() {
        return 1 + getValue().size();
    }
}
