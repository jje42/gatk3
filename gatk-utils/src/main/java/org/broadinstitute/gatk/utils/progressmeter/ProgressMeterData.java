/*
* Copyright 2012-2016 Broad Institute, Inc.
* 
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use,
* copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following
* conditions:
* 
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
* THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package org.broadinstitute.gatk.utils.progressmeter;


/**
 * a snapshot of our performance, suitable for storage and later analysis
 */
class ProgressMeterData {
    private final double elapsedSeconds;
    private final long unitsProcessed;
    private final long bpProcessed;

    public ProgressMeterData(double elapsedSeconds, long unitsProcessed, long bpProcessed) {
        this.elapsedSeconds = elapsedSeconds;
        this.unitsProcessed = unitsProcessed;
        this.bpProcessed = bpProcessed;
    }

    public double getElapsedSeconds() {
        return elapsedSeconds;
    }

    public long getUnitsProcessed() {
        return unitsProcessed;
    }

    public long getBpProcessed() {
        return bpProcessed;
    }

    /** How long in seconds to process 1M traversal units? */
    public double secondsPerMillionElements() {
        return (elapsedSeconds * 1000000.0) / Math.max(unitsProcessed, 1);
    }

    /** How long in seconds to process 1M bp on the genome? */
    public double secondsPerMillionBP() {
        return (elapsedSeconds * 1000000.0) / Math.max(bpProcessed, 1);
    }

    /** What fraction of the target intervals have we covered? */
    public double calculateFractionGenomeTargetCompleted(final long targetSize) {
        return (1.0*bpProcessed) / Math.max(targetSize, 1);
    }
}
